#version 100

precision mediump float;

uniform vec3 lightColour;

void main() {
	gl_FragColor = vec4(lightColour, 1.0);
}
