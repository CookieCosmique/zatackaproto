#version 300 es

in vec2 position;
in vec2 direction;
in float hole;

uniform mat4 projectionMatrix;
uniform mat4 viewMatrix;
uniform mat4 viewMatrix_inverse;

out float alpha;

const float size = 0.175;

#if 1
void main()
{
	alpha = hole == 0.0 ? 1.0 : 0.25;
	vec4 positionRelativeToCam = viewMatrix * vec4(position.x, 0.0, position.y, 1.0);
	gl_Position = projectionMatrix * positionRelativeToCam;
}
#else
void EmitPoint(vec2 point, vec3 norm)
{
	vec3 fWorldPosition = vec3(point.x, 0.0, point.y);
	gl_Position = projectionMatrix * viewMatrix * vec4(fWorldPosition, 1.0);
	EmitVertex();
}

void Emit()
{
	vec2 normdir = normalize(vec2(-direction.y, direction.x));
	EmitPoint(position + size * normdir, vec3(normdir.x, 0.6, normdir.y));
	EmitPoint(position - size * normdir, vec3(-normdir.x, 0.6, -normdir.y));
}

void main()
{
	alpha = hole == 0.0 ? 1.0 : 0.25;
	vec4 positionRelativeToCam = viewMatrix * vec4(position.x, 0.0, position.y, 1.0);
	gl_Position = projectionMatrix * positionRelativeToCam;
	Emit();
	EndPrimitive();
}
#endif