#version 330

layout(lines) in;
layout(line_strip, max_vertices = 5) out;

in vec2 dir[];
in float gHole[];

out vec3 fWorldPosition;
out vec3 fNormal;
out vec3 fCameraVector;
out float alpha;

uniform mat4 projectionMatrix;
uniform mat4 viewMatrix;

const float size = .175f;

void EmitPoint(vec2 point, vec3 norm)
{
	fWorldPosition = vec3(point.x, 0.f, point.y);
	gl_Position = projectionMatrix * viewMatrix * vec4(fWorldPosition, 1.f);
	fCameraVector = (inverse(viewMatrix) * vec4(0.f, 0.f, 0.f, 1.f)).xyz - fWorldPosition;
	fNormal = norm;
	EmitVertex();
}

void EmitRectangle()
{
	vec2 normdir = normalize(vec2(-dir[0].y, dir[0].x));
	vec2 normdir2 = normalize(vec2(-dir[1].y, dir[1].x));

	EmitPoint(gl_in[0].gl_Position.xy + size * normdir, vec3(normdir.x, .5f, normdir.y));
	EmitPoint(gl_in[0].gl_Position.xy - size * normdir, vec3(-normdir.x, .5f, -normdir.y));

	EmitPoint(gl_in[1].gl_Position.xy - size * normdir2, vec3(-normdir2.x, .5f, -normdir2.y));
	EmitPoint(gl_in[1].gl_Position.xy + size * normdir2, vec3(normdir2.x, .5f, normdir2.y));

	EmitPoint(gl_in[0].gl_Position.xy + size * normdir, vec3(normdir.x, .5f, normdir.y));
}

void main()
{
	alpha = min(gHole[0], gHole[1]) == 0.f ? 1.f : .25f;
	EmitRectangle();
	EndPrimitive();
}
