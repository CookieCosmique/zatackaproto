#version 300 es

precision mediump float;

uniform vec3 color;

in float alpha;

out vec4 out_Color;

void main() {
	out_Color = vec4(color, alpha);
}
