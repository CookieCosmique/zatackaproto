# -----------------------------------------------------------------------------
# ------------------------ PROJECT SPECIFIC DEFINITIONS -----------------------
# -----------------------------------------------------------------------------

# Command args:
#   - COMPIL					(debug/release) (default: release)
#   - DBG						(gdb/lldb/...) (default: none)
#   - VERBOSE					(true/false) (default: false)

# Constants:
#   - PLATFORM					Platform name
#   - COMPIL					debug/release, is set with COMPIL option parameter (default: release)
#   - PRINT						Command to display debug messages

# Optional vars you can set:
#   - CPP_COMPILER				(default: clang++)
#   - C_COMPILER				(default: clang)
#   - CFLAGS					(default: -Wall -Wextra -Werror -fno-exceptions $(OPTIFLAGS))
#   - DEFINES					(no default value): List of C macro defined in all source files
#   - BUILD_DIR					(default: ./build)
#   - VERBOSE					(true/false) (default: false)

# Required vars:
#   For every modes:
#     - PROJECT_NAME
#     - TARGET_TYPE				(exe/lib)
#     - TARGET_NAME
#     - SRC_FILES				List of c/cpp files
#     - INCLUDE_DIRS
#   Only for exe mode:
#     - LINK_COMPILER			($(C_COMPILER)/$(CPP_COMPILER))
#     - LIBS_TO_BUILD			Libs that need a build (You need to create a rule for each lib in this list)
#     - ADDITIONAL_LIB_DIRS		List of directories to search for libraries
#     - LIBS_TO_LINK			Libs that will be linked

# -----------------------------------------------------------------------------

# *Project name
PROJECT_NAME	= ZatackaProto

# *Target type (exe/lib)
TARGET_TYPE		= exe

# *Target name (without extension)
TARGET_NAME		= $(PROJECT_NAME)_$(PLATFORM)_$(COMPIL)

# *Link compiler (Only for exe mode): can be $(C_COMPILER) or $(CPP_COMPILER)
LINK_COMPILER	= $(CPP_COMPILER)
CFLAGS			= -Wall -Wextra -Werror -fno-exceptions -std=c++11 $(OPTIFLAGS)

BUILD_DIR		= ./build

# -----------------------------------------------------------------------------

# *Source files
SRC_FILES		= src/Assert.cpp						\
				  src/DisplayManager.cpp				\
				  src/EntityRenderer.cpp				\
				  src/EntityShader.cpp					\
				  src/GLineRenderer.cpp					\
				  src/GLineShader.cpp					\
				  src/ImageParser.cpp					\
				  src/IsInDebugger.cpp					\
				  src/LineRenderer.cpp					\
				  src/LineShader.cpp					\
				  src/Loader.cpp						\
				  src/main.cpp							\
				  src/MasterRenderer.cpp				\
				  src/Maths.cpp							\
				  src/OBJLoader.cpp						\
				  src/ShaderProgram.cpp					\
				  src/TerrainRenderer.cpp				\
				  src/TerrainShader.cpp					\
				  src/PNGParser/buffer.cpp				\
				  src/PNGParser/handle_idat.cpp			\
				  src/PNGParser/image_data.cpp			\
				  src/PNGParser/png_parser.cpp			\
				  src/PNGParser/unzib_stream.cpp

# *Include directories
INCLUDE_DIRS	= ./externals/glfw/include/				\
				  ./externals/glm32/include/			\
				  ./externals/glew/include/

# ---------------------------- ONLY FOR EXE MODE ------------------------------

# Only in exe mode:
# Libraries to link (Project specific var names)
GLEW			= externals/glew/lib/Release/$(PLATFORM)/libglew32.a
GLFW			= externals/glfw/lib/Release/$(PLATFORM)/libglfw3.a
GLM				= externals/glm32/lib/Release/$(PLATFORM)/libglm_static.a

ifneq (,$(findstring CYGWIN,$(PLATFORM)))
OPENGL			= -lgdi32 -lopengl32
else ifneq (,$(findstring Linux,$(PLATFORM)))
OPENGL			= -lGL -lX11 -lpthread -ldl
endif

# Libs that will be linked
LIBS_TO_LINK		= $(GLEW) $(GLFW) $(GLM) $(OPENGL)

# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------
