#version 330 core

in vec2 pass_textureCoords;
in vec3 surfaceNormal;
in vec3 toLightVector;
in vec3 toCameraVector;

out vec4 out_Color;

uniform sampler2D textureSampler;

uniform vec3 lightColour;

const float shineDamper = 15.f;
const float reflectivity = .5f;

vec3 computeDiffuseColor(vec3 normal, vec3 toLight, vec3 colour)
{
	float nDotl = dot(normal, toLight);
	float brightness = max(nDotl, 0.f);

	return max(brightness * colour, .2f);
}

vec3 computeSpecularColor(vec3 normal, vec3 toLight, vec3 toCamera, vec3 colour)
{
	vec3 reflectedLightDirection = reflect(-toLight, normal);

	float specularFactor = dot(reflectedLightDirection, toCamera);
	specularFactor = max(specularFactor, 0.f);
	float dampedFactor = pow(specularFactor, shineDamper);

	return dampedFactor * reflectivity * colour;
}

void main()
{
	vec3 unitNormal = normalize(surfaceNormal);
	vec3 unitLightVector = normalize(toLightVector);
	vec3 unitVectorToCamera = normalize(toCameraVector);

	vec3 diffuse = computeDiffuseColor(unitNormal, unitLightVector, lightColour);
	vec3 specular = computeSpecularColor(unitNormal, unitLightVector, unitVectorToCamera, lightColour);

	vec4 textureColour = texture(textureSampler, pass_textureCoords);

	out_Color = vec4(diffuse, 1.f) * textureColour + vec4(specular, 1.f);
}
