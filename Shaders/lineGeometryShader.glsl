#version 330 core

layout(triangles) in;
layout(triangle_strip, max_vertices = 3) out;

in vec3 gWorldPosition[];
in vec3 gNormal[];
in vec3 gCameraVector[];

out vec3 fWorldPosition;
out vec3 fNormal;
out vec3 fCameraVector;

void EmitPoint(int index)
{
	gl_Position = gl_in[index].gl_Position;
	fWorldPosition = gWorldPosition[index];
	fNormal = gNormal[index];
	fCameraVector = gCameraVector[index];
	EmitVertex();
}

void main()
{
	EmitPoint(2);
	EmitPoint(0);
	EmitPoint(1);
	EndPrimitive();
}
