#version 330 core

out vec4 out_Color;

in vec3 fWorldPosition;
in vec3 fNormal;
in vec3 fCameraVector;

uniform vec3 lightPosition;
uniform vec3 lightColour;

const float shineDamper = 15.f;
const float reflectivity = .5f;

vec3 computeDiffuseColor(vec3 normal, vec3 toLight, vec3 colour)
{
	float nDotl = dot(normal, toLight);
	float brightness = max(nDotl, 0.f);

	return max(brightness * colour, 0.f);
}

vec3 computeSpecularColor(vec3 normal, vec3 toLight, vec3 toCamera, vec3 colour)
{
	vec3 reflectedLightDirection = reflect(-toLight, normal);

	float specularFactor = dot(reflectedLightDirection, toCamera);
	specularFactor = max(specularFactor, 0.f);
	float dampedFactor = pow(specularFactor, shineDamper);

	return dampedFactor * reflectivity * colour;
}

void main()
{
	vec3 diffuse = computeDiffuseColor(normalize(fNormal), normalize(lightPosition - fWorldPosition), lightColour);
	vec3 specular = computeSpecularColor(normalize(fNormal), normalize(lightPosition - fWorldPosition), normalize(fCameraVector), lightColour);
	out_Color = vec4(diffuse, 1.f) * vec4(1.f, 0.f, 0.f, 1.f) + vec4(specular, 1.f);
}
