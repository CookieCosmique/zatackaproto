#version 330 core

in vec2 pass_textureCoords;
in vec3 surfaceNormal;
in vec3 toLightVector;
in vec3 toCameraVector;
in float brightness;

out vec4 out_Color;

uniform sampler2D textureSampler;

uniform vec3 lightColour;
uniform int lightDiffuse;
uniform int lightSpecular;
uniform vec3 color;

const float shineDamper = 15.f;
const float reflectivity = .5f;
/*
vec3 computeDiffuseColor(vec3 normal, vec3 toLight, vec3 colour)
{
	float nDotl = dot(normal, toLight);
	float brightness = max(nDotl, 0.f);

	return max(brightness * colour, 0.f);
}
*/
vec3 computeSpecularColor(vec3 normal, vec3 toLight, vec3 toCamera, vec3 colour)
{
	vec3 reflectedLightDirection = reflect(-toLight, normal);

	float specularFactor = dot(reflectedLightDirection, toCamera);
	specularFactor = max(specularFactor, 0.f);
	float dampedFactor = pow(specularFactor, shineDamper);

	return dampedFactor * reflectivity * colour;
}

void main()
{
	vec4 textureColour = texture(textureSampler, pass_textureCoords);
	out_Color = vec4(color, 1);

	if (lightDiffuse == 1)
	{
		vec3 diffuse = max(brightness * lightColour, 0.f);
		out_Color *= vec4(diffuse, 1.f);
	}

	if (lightSpecular == 1)
	{
		vec3 unitNormal = normalize(surfaceNormal);
		vec3 unitLightVector = normalize(toLightVector);
		vec3 unitVectorToCamera = normalize(toCameraVector);

		vec3 specular = computeSpecularColor(unitNormal, unitLightVector, unitVectorToCamera, lightColour);
		out_Color += vec4(specular, 1.f);
	}
}
