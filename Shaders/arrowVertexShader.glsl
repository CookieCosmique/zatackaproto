#version 330 core

layout(location = 0) in vec3 position;
layout(location = 1) in vec2 textureCoords;
layout(location = 2) in vec3 normal;

out vec2 gpass_textureCoords;
out vec3 gtoLightVector;
out vec3 gtoCameraVector;
out vec3 gworldPosition;
out vec3 gsurfaceNormal;

uniform mat4 projectionMatrix;
uniform mat4 viewMatrix;
uniform mat4 transformationMatrix;

uniform vec3 lightPosition;

void main()
{
	vec4 worldPosition = transformationMatrix * vec4(position, 1.0);
	vec4 positionRelativeToCam = viewMatrix * worldPosition;
	gl_Position = projectionMatrix * positionRelativeToCam;

	gpass_textureCoords = textureCoords;

	gsurfaceNormal = (transformationMatrix * vec4(normal, 0.f)).xyz;

	gworldPosition = worldPosition.xyz;
	gtoLightVector = lightPosition - worldPosition.xyz;
	gtoCameraVector = (inverse(viewMatrix) * vec4(0.f, 0.f, 0.f, 1.f)).xyz - worldPosition.xyz;
}
