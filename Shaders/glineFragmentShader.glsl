#version 330 core

out vec4 out_Color;

in vec3 fWorldPosition;
in vec3 fNormal;
in vec3 fCameraVector;
in float alpha;

uniform vec3 lightPosition;
uniform vec3 lightColour;

uniform int lightDiffuse;
uniform int lightSpecular;

uniform vec3 color;

const float shineDamper = 15.f;
const float reflectivity = .5f;

vec3 computeDiffuseColor(vec3 normal, vec3 toLight, vec3 colour)
{
	float nDotl = dot(normal, toLight);
	float brightness = max(nDotl, 0.f);

	return max(brightness * colour, 0.2f);
}

vec3 computeSpecularColor(vec3 normal, vec3 toLight, vec3 toCamera, vec3 colour)
{
	vec3 reflectedLightDirection = reflect(-toLight, normal);

	float specularFactor = dot(reflectedLightDirection, toCamera);
	specularFactor = max(specularFactor, 0.f);
	float dampedFactor = pow(specularFactor, shineDamper);

	return dampedFactor * reflectivity * colour;
}

void main()
{
	out_Color = vec4(color, 1.f);

//	vec3 testLightPosition = fWorldPosition;
//	testLightPosition.y += 50.f;
//	testLightPosition.x += 100.f;

	if (lightDiffuse >= .5f)
	{
		vec3 diffuse = computeDiffuseColor(normalize(fNormal), normalize(lightPosition - fWorldPosition), lightColour);
		out_Color *= vec4(diffuse, 1.f);
	}

	if (lightSpecular >= .5f)
	{
		vec3 specular = computeSpecularColor(normalize(fNormal), normalize(lightPosition - fWorldPosition), normalize(fCameraVector), lightColour);
		out_Color += vec4(specular, 1.f);
	}

	out_Color.a = alpha;
}
