#version 330 core

layout(location = 0) in vec2 position;
layout(location = 1) in vec3 normal;

out vec3 gWorldPosition;
out vec3 gNormal;
out vec3 gCameraVector;

uniform mat4 projectionMatrix;
uniform mat4 viewMatrix;

void main()
{
	gl_Position = projectionMatrix * viewMatrix * vec4(position.x, 0, position.y, 1.f);
	gWorldPosition = vec3(position.x, 0, position.y);
	gNormal = normal;
	gCameraVector = (inverse(viewMatrix) * vec4(0.f, 0.f, 0.f, 1.f)).xyz - gWorldPosition;
}
