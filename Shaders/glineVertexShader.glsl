#version 330 core

layout(location = 0) in vec2 position;
layout(location = 1) in vec2 direction;
layout(location = 2) in float hole;

out vec2 dir;
out float gHole;

void main()
{
	dir = direction;
	gHole = hole;
	gl_Position = vec4(position, 0.f, 1.f);
}
