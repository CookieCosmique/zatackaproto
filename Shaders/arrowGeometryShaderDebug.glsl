#version 330 core

layout(triangles) in;
layout(line_strip, max_vertices = 4) out;

in vec2 gpass_textureCoords[];
in vec3 gtoLightVector[];
in vec3 gtoCameraVector[];
in vec3 gworldPosition[];
in vec3 gsurfaceNormal[];

out vec2 pass_textureCoords;
out vec3 surfaceNormal;
out vec3 toLightVector;
out vec3 toCameraVector;
out float brightness;

float computeDiffuseRatio(vec3 normal, vec3 toLight)
{
	float nDotl = dot(normal, toLight);
	float ratio = max(nDotl, 0.f);

	return ratio;
}

void main()
{
	vec3 a = normalize(gworldPosition[1] - gworldPosition[0]);
	vec3 b = normalize(gworldPosition[2] - gworldPosition[0]);
	vec3 normal = normalize(cross(a, b));

	for (int i = 0; i < 4; i++)
	{
		int id = i % 3;

		gl_Position = gl_in[id].gl_Position;
		pass_textureCoords = gpass_textureCoords[id];
		surfaceNormal = normal;
		brightness = computeDiffuseRatio(normal, normalize(gtoLightVector[id]));
		toLightVector = gtoLightVector[id];
		toCameraVector = gtoCameraVector[id];
		EmitVertex();
	}

	EndPrimitive();
}

