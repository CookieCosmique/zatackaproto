#include "ProjectDefines.h"

#include "GLineShader.h"

#include "Maths.h"

#include <string>

#ifdef __EMSCRIPTEN__
GLineShader::GLineShader(const char* vertexFile, const char* fragmentFile) :
	ShaderProgram(vertexFile, fragmentFile)
#else
GLineShader::GLineShader(const char* vertexFile, const char* geometryFile, const char* fragmentFile) :
	ShaderProgram(vertexFile, geometryFile, fragmentFile)
#endif
{
	init();
}

GLineShader::~GLineShader()
{

}

void GLineShader::loadProjectionMatrix(const glm::mat4& matrix) const
{
	loadMatrix(location_projectionMatrix, matrix);
}

void GLineShader::loadViewMatrix(const Camera& camera) const
{
	const glm::mat4 ViewMatrix = Maths::createViewMatrix(camera);
	loadMatrix(location_viewMatrix, ViewMatrix);
	loadMatrix(location_viewMatrix_inverse, glm::inverse(ViewMatrix));
}

void GLineShader::loadLight(const Light& light) const
{
	loadVector(location_lightPosition, light.position);
	loadVector(location_lightColour, light.colour);
}

void GLineShader::loadLighting(bool diffuse, bool specular) const
{
	loadBool(location_lightDiffuse, diffuse);
	loadBool(location_lightSpecular, specular);
}

void GLineShader::loadColor(const glm::vec3& color) const
{
	loadVector(location_color, color);
}

void GLineShader::bindAttributes() const
{
	bindAttribute(0, "position");
	bindAttribute(1, "direction");
	bindAttribute(2, "hole");
}

void GLineShader::getAllUniformLocations()
{
	location_projectionMatrix = getUniformLocation("projectionMatrix");
	location_viewMatrix = getUniformLocation("viewMatrix");
	location_viewMatrix_inverse = getUniformLocation("viewMatrix_inverse");

	location_lightPosition = getUniformLocation("lightPosition");
	location_lightColour = getUniformLocation("lightColour");

	location_lightDiffuse = getUniformLocation("lightDiffuse");
	location_lightSpecular = getUniformLocation("lightSpecular");

	location_color = getUniformLocation("color");
}
