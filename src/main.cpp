#include "ProjectDefines.h"

#ifdef __EMSCRIPTEN__
#include <emscripten.h>
#include <emscripten/html5.h>
#endif

#include "Maths.h"

#include "DisplayManager.h"
#include "GLineRenderer.h"
#include "MasterRenderer.h"
#include "Loader.h"
#include "OBJLoader.h"
#include "Options.h"

#include <string>
#include <iostream>
#include "CheckOpenGLError.h"

namespace
{
	TexturedModel loadModel(Loader* loader, const std::string& obj)
	{
		TexturedModel model;
		model.model = OBJLoader::loadObjModel(obj, loader);
		model.texture = 0;
		return model;
	}

#if 0
	TexturedModel loadModel(Loader* loader, const std::string& obj, const std::string& texture)
	{
		TexturedModel model;
		model.model = OBJLoader::loadObjModel(obj, loader);
		model.texture = loader->loadTexture(texture);
		return model;
	}

	void mainGameLoop()
	{
		DisplayManager displayManager;
		displayManager.createDisplay();

		Light light{ glm::vec3(0.f, 0.f, 0.f), glm::vec3(1.f, 1.f, 1.f) };
//		Camera camera{ glm::vec3(0.f, 0.f, -10.f), 0.f, 180.f, 0.f };
		Camera camera{ glm::vec3(0.f, 7.f, -10.f), -45.f, 180.f, 0.f };

		Loader loader;

		GLuint defaultTexture = loader.loadTexture("Resources/default.bmp");

		TexturedModel cubeModel = loadModel(&loader, "Resources/cuberouge.obj", "Resources/cuberouge.bmp");
		Entity cubeEntity { glm::vec3(0.f, 0.f, 5.f), glm::vec3(0.f, 0.f, 0.f), glm::vec3(1.f, 1.f, 1.f) };
		Entity cubeEntity2 { glm::vec3(-2.f, -1.f, 5.f), glm::vec3(0.f, 0.f, 0.f), glm::vec3(1.f, 1.f, 1.f) };
		Entity cubeEntity3 { glm::vec3(2.f, 1.f, 5.f), glm::vec3(0.f, 0.f, 0.f), glm::vec3(1.f, 1.f, 1.f) };

		TexturedModel dragonModel = loadModel(&loader, "Resources/dragon.obj");
		Entity dragon { glm::vec3(0.f, -2.f, 0.f), glm::vec3(0.f, 0.f, 0.f), glm::vec3(.5f, .5f, .5f) };

		TexturedModel arrowModel = loadModel(&loader, "Resources/arrow.obj");
		Entity arrow{ glm::vec3(0.f, 0.f, -5.f), glm::vec3(0.f, 0.f, 0.f), glm::vec3(1.f, 1.f, 1.f) };

		Entity lightEntity{ glm::vec3(0.f, 0.f, 0.f), glm::vec3(0.f, 0.f, 0.f), glm::vec3(.2f, .2f, .2f) };

		MasterRenderer renderer(defaultTexture, &loader);

		renderer.addEntity(&cubeModel, &cubeEntity);
		renderer.addEntity(&cubeModel, &cubeEntity2);
		renderer.addEntity(&cubeModel, &cubeEntity3);
		renderer.addEntity(&dragonModel, &dragon);
		renderer.addEntity(&arrowModel, &arrow);
		renderer.addEntity(&cubeModel, &lightEntity);

		GLFWwindow* window = displayManager.getWindow();

		while (!displayManager.isCloseRequested())
		{
			cubeEntity.rotation.y += .5f;
			cubeEntity3.rotation.x += .5f;
			dragon.rotation.y += .5f;
			arrow.rotation.y += .5f;

			if (glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS)
			{
				light.position.x -= .05f;
				lightEntity.position.x -= .05f;
			}
			if (glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS)
			{
				light.position.x += .05f;
				lightEntity.position.x += .05f;
			}
			if (glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS)
			{
				light.position.y += .05f;
				lightEntity.position.y += .05f;
			}
			if (glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS)
			{
				light.position.y -= .05f;
				lightEntity.position.y -= .05f;
			}
			if (glfwGetKey(window, GLFW_KEY_F) == GLFW_PRESS)
			{
				light.position.z += .05f;
				lightEntity.position.z += .05f;
			}
			if (glfwGetKey(window, GLFW_KEY_V) == GLFW_PRESS)
			{
				light.position.z -= .05f;
				lightEntity.position.z -= .05f;
			}

			renderer.render(camera, light, 0, 0, Options());
			displayManager.updateDisplay();
		}

		renderer.cleanUp();

		loader.cleanUp();

		displayManager.closeDisplay();
	}

#endif

	class Line
	{
	public:
		Line() :
			elements(0),
			loaded(0)
		{
			glGenVertexArrays(1, &vao); CGLE();
			glBindVertexArray(vao); CGLE();

			glGenBuffers(1, &vbo); CGLE();
			{
				glBindBuffer(GL_ARRAY_BUFFER, vbo); CGLE();
				glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, nullptr); CGLE();
				GLfloat vertices[2 * 4 * maxData];
				glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * 2 * 4 * maxData, vertices, GL_DYNAMIC_DRAW); CGLE();
			}
			glBindBuffer(GL_ARRAY_BUFFER, 0); CGLE();

			glGenBuffers(1, &vbo_normals); CGLE();
			{
				glBindBuffer(GL_ARRAY_BUFFER, vbo_normals); CGLE();
				glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, nullptr); CGLE();
				GLfloat normals[2 * 6 * maxData];
				glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * 2 * 6 * maxData, normals, GL_DYNAMIC_DRAW); CGLE();
			}
			glBindBuffer(GL_ARRAY_BUFFER, 0); CGLE();

			glGenBuffers(1, &ebo); CGLE();
			{
				glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo); CGLE();
				GLuint indices[12 * maxData];
				glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLuint) * 12 * maxData, indices, GL_DYNAMIC_DRAW); CGLE();
			}

			glBindVertexArray(0); CGLE();
		}

		void initPoint(const glm::vec2& point, const glm::vec2& dir)
		{
			if (loaded / 2 >= maxData)
				return;

			const glm::vec2 norm = glm::normalize(dir);
			const glm::vec2 normal = glm::vec2(norm.y, norm.x);

			GLfloat vertices[8] = {
				point.x + size / 2.f * normal.x, point.y + size / 2.f * normal.y,
				point.x - size / 2.f * normal.x, point.y - size / 2.f * normal.y,
				point.x + size / 2.f * normal.x, point.y + size / 2.f * normal.y,
				point.x - size / 2.f * normal.x, point.y - size / 2.f * normal.y
			};

			GLfloat normals[12] = {
				normal.x, 0.5f, normal.y,
				-normal.x, 0.25f, -normal.y,
				normal.x, 0.5f, normal.y,
				-normal.x, 0.25f, -normal.y,
			};

			GLuint indices[6] = {
				loaded + 0, loaded + 1, loaded + 2,
				loaded + 1, loaded + 2, loaded + 3
			};

			glBindVertexArray(vao); CGLE();

			glBindBuffer(GL_ARRAY_BUFFER, vbo); CGLE();
			glBufferSubData(GL_ARRAY_BUFFER, sizeof(GLfloat) * 2 * loaded, sizeof(GLfloat) * 8, vertices); CGLE();

			glBindBuffer(GL_ARRAY_BUFFER, vbo_normals); CGLE();
			glBufferSubData(GL_ARRAY_BUFFER, sizeof(GLfloat) * 3 * loaded, sizeof(GLfloat) * 12, normals); CGLE();
			glBindBuffer(GL_ARRAY_BUFFER, 0); CGLE();

			glBufferSubData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLuint) * elements, sizeof(GLuint) * 6, indices); CGLE();

			loaded += 4;
			elements += 6;

			std::cout << "Points: " << loaded / 2 << std::endl;

			glBindVertexArray(0); CGLE();

			lastPos = point;
			lastDir = dir;
		}

		void addPoint(const glm::vec2& point, const glm::vec2& dir)
		{
			if (loaded / 2 >= maxData)
				return;

			const glm::vec2 norm = glm::normalize(dir);
			const glm::vec2 normal = glm::vec2(norm.y, norm.x);

			const glm::vec2 lastNorm = glm::normalize(lastDir);
			const glm::vec2 lastNormal = glm::vec2(lastNorm.y, lastNorm.x);

			GLfloat vertices[16] = {
				lastPos.x + size / 2.f * lastNormal.x, lastPos.y + size / 2.f * lastNormal.y,
				lastPos.x - size / 2.f * lastNormal.x, lastPos.y - size / 2.f * lastNormal.y,
				point.x + size / 2.f * normal.x, point.y + size / 2.f * normal.y,
				point.x - size / 2.f * normal.x, point.y - size / 2.f * normal.y,

				point.x + size / 2.f * normal.x, point.y + size / 2.f * normal.y,
				point.x - size / 2.f * normal.x, point.y - size / 2.f * normal.y,
				point.x + size / 2.f * normal.x, point.y + size / 2.f * normal.y,
				point.x - size / 2.f * normal.x, point.y - size / 2.f * normal.y
			};

			GLfloat normals[24] = {
				lastNormal.x, 0.5f, lastNormal.y,
				-lastNormal.x, 0.25f, -lastNormal.y,
				normal.x, 0.5f, normal.y,
				-normal.x, 0.25f, -normal.y,
				normal.x, 0.5f, normal.y,
				-normal.x, 0.25f, -normal.y,
				normal.x, 0.5f, normal.y,
				-normal.x, 0.25f, -normal.y,
			};

			GLuint indices[12] = {
				loaded + 0, loaded + 1, loaded + 2,
				loaded + 1, loaded + 2, loaded + 3,
				loaded + 4, loaded + 5, loaded + 6,
				loaded + 5, loaded + 6, loaded + 7
			};

			glBindVertexArray(vao); CGLE();

			glBindBuffer(GL_ARRAY_BUFFER, vbo); CGLE();
			glBufferSubData(GL_ARRAY_BUFFER, sizeof(GLfloat) * 2 * loaded, sizeof(GLfloat) * 16, vertices); CGLE();

			glBindBuffer(GL_ARRAY_BUFFER, vbo_normals); CGLE();
			glBufferSubData(GL_ARRAY_BUFFER, sizeof(GLfloat) * 3 * loaded, sizeof(GLfloat) * 24, normals); CGLE();
			glBindBuffer(GL_ARRAY_BUFFER, 0); CGLE();

			glBufferSubData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLuint) * elements, sizeof(GLuint) * 12, indices); CGLE();

			loaded += 8;
			elements += 12;

			std::cout << "Points: " << loaded / 2 << ", GPU Memory: " << loaded * 2 * sizeof(GLfloat) + elements * sizeof(GLuint) + loaded * 3 * sizeof(GLfloat) << " bytes" << std::endl;

			glBindVertexArray(0); CGLE();
		}

		void updateLast(const glm::vec2& point, const glm::vec2& dir)
		{
			if (loaded / 2 >= maxData)
				return;

			const glm::vec2 norm = glm::normalize(dir);
			const glm::vec2 normal = glm::vec2(norm.y, norm.x);

			GLfloat vertices[4] = {
				point.x + size / 2.f * normal.x, point.y + size / 2.f * normal.y,
				point.x - size / 2.f * normal.x, point.y - size / 2.f * normal.y
			};

			glBindVertexArray(vao); CGLE();
			glBindBuffer(GL_ARRAY_BUFFER, vbo); CGLE();

			glBufferSubData(GL_ARRAY_BUFFER, sizeof(GLfloat) * 2 * (loaded - 2), sizeof(GLfloat) * 2 * 2, vertices); CGLE();

			glBindBuffer(GL_ARRAY_BUFFER, 0); CGLE();
			glBindVertexArray(0); CGLE();

			lastPos = point;
			lastDir = dir;
		}

	public:
		GLuint vao;
		GLuint elements;

	private:
		static const unsigned int maxData = 1000;
		const float size = .5f;
		GLuint loaded;

		glm::vec2 lastPos;
		glm::vec2 lastDir;

		GLuint vbo;
		GLuint vbo_normals;
		GLuint ebo;
	};

	class GLine
	{
	public:
		GLine() :
			elements(0),
			loaded(0),
			elements_index(0)
		{
			glGenVertexArrays(1, &vao); CGLE();
			glBindVertexArray(vao); CGLE();

			glGenBuffers(1, &vbo); CGLE();
			{
				glBindBuffer(GL_ARRAY_BUFFER, vbo); CGLE();
//				glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 0, nullptr); CGLE();
				glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(GLfloat) * 4 + sizeof(GLfloat), (void*)(0)); CGLE();
				glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(GLfloat) * 4 + sizeof(GLfloat), (void*)(sizeof(GLfloat) * 2)); CGLE();
				glVertexAttribPointer(2, 1, GL_FLOAT, GL_FALSE, sizeof(GLfloat) * 4 + sizeof(GLfloat), (void*)(sizeof(GLfloat) * 4)); CGLE();
				GLfloat vertices[10 * maxData];
				glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * 10 * maxData, vertices, GL_STATIC_DRAW); CGLE();
			}

			glGenBuffers(1, &ebo); CGLE();
			{
				glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo); CGLE();
				GLuint indices[4 * maxData];
				glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLuint) * 4 * maxData, indices, GL_STATIC_DRAW); CGLE();
			}

			glBindVertexArray(0); CGLE();
		}

		void initPoint(const glm::vec2& point, const glm::vec2& dir)
		{
			GLfloat vertices[10] = {
				point.x, point.y,
				dir.x, dir.y,
				0.f,
				point.x, point.y,
				dir.x, dir.y,
				0.f
			};

			GLuint indices[2] = {
				0, 1
			};

			glBindVertexArray(vao); CGLE();

			glBindBuffer(GL_ARRAY_BUFFER, vbo); CGLE();
			glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(GLfloat) * 10, vertices); CGLE();
			glBufferSubData(GL_ELEMENT_ARRAY_BUFFER, 0, sizeof(GLuint) * 2, indices); CGLE();

			glBindVertexArray(0); CGLE();

			elements = 2;
			elements_index = 2;
			loaded = 10;
		}

		void linkPoint(const glm::vec2& point, const glm::vec2& dir, float hole)
		{
			if (loaded / 10 >= maxData)
				return;

			GLfloat vertices[10] = {
				point.x, point.y,
				dir.x, dir.y,
				hole,
				point.x, point.y,
				dir.x, dir.y,
				hole
			};

			GLuint indices[4] = {
				elements_index - 1, elements_index,
				elements_index, elements_index + 1
			};

			glBindVertexArray(vao); CGLE();

			glBindBuffer(GL_ARRAY_BUFFER, vbo); CGLE();
			glBufferSubData(GL_ARRAY_BUFFER, sizeof(GLfloat) * loaded, sizeof(GLfloat) * 10, vertices); CGLE();
			glBufferSubData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLuint) * elements, sizeof(GLuint) * 4, indices); CGLE();

			glBindVertexArray(0); CGLE();

			elements += 4;
			elements_index += 2;
			loaded += 10;

//			std::cout << "Elements: " << elements << ", GPU Memory: " << sizeof(GLfloat) * loaded + sizeof(GLuint) * elements << " bytes" << std::endl;
		}

		void addPoint(const glm::vec2& point, const glm::vec2& dir, float hole)
		{
			if (loaded / 10 >= maxData)
				return;

			GLfloat vertices[10] = {
				point.x, point.y,
				dir.x, dir.y,
				hole,
				point.x, point.y,
				dir.x, dir.y,
				hole
			};

			GLuint indices[2] = {
				elements_index, elements_index + 1
			};

			glBindVertexArray(vao); CGLE();

			glBindBuffer(GL_ARRAY_BUFFER, vbo); CGLE();
			glBufferSubData(GL_ARRAY_BUFFER, sizeof(GLfloat) * loaded, sizeof(GLfloat) * 10, vertices); CGLE();
			glBufferSubData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLuint) * elements, sizeof(GLuint) * 2, indices); CGLE();

			glBindVertexArray(0); CGLE();

			elements += 2;
			elements_index += 2;
			loaded += 10;

//			std::cout << "Elements: " << elements << ", GPU Memory: " << sizeof(GLfloat) * loaded + sizeof(GLuint) * elements << " bytes" << std::endl;
		}

		void updateLast(const glm::vec2& point, const glm::vec2& dir)
		{
			if (loaded / 10 >= maxData)
				return;

			GLfloat vertices[4] = {
				point.x, point.y,
				dir.x, dir.y
			};

			glBindVertexArray(vao); CGLE();

			glBindBuffer(GL_ARRAY_BUFFER, vbo); CGLE();
			glBufferSubData(GL_ARRAY_BUFFER, sizeof(GLfloat) * (loaded - 5), sizeof(GLfloat) * 4, vertices); CGLE();

			glBindVertexArray(0); CGLE();
		}

	public:
		GLuint vao;
		GLuint elements;

	private:
		static const unsigned int maxData = 16384;
		GLuint loaded;
		GLuint elements_index;

		GLuint vbo;
		GLuint ebo;
	};

	Options op = { false, true, true, glm::vec3(1.f, 0.20f, 0.15f), true };

	float cameraAngle;

	const float CameraAngleTop = 90.f;
	const float CameraAnglePreset = 72.f;

	enum CAMERA_TYPE {
		CENTERED = 0,
		BORDER,
		FRONT
	};

	CAMERA_TYPE CameraType = CAMERA_TYPE::CENTERED;

	void key_callback(GLFWwindow*, int key, int, int action, int)
	{
		if (action == GLFW_PRESS)
		{
			if (key == GLFW_KEY_T)
				op.debug_vertices = !op.debug_vertices;
			else if (key == GLFW_KEY_Y)
				op.diffuse = !op.diffuse;
			else if (key == GLFW_KEY_U)
				op.specular = !op.specular;
			else if (key == GLFW_KEY_1)
				CameraType = CAMERA_TYPE::CENTERED;
			else if (key == GLFW_KEY_2)
				CameraType = CAMERA_TYPE::BORDER;
			else if (key == GLFW_KEY_3)
				CameraType = CAMERA_TYPE::FRONT;
			else if (key == GLFW_KEY_8)
				op.color.r = op.color.r == 1.f ? 0.f : 1.f;
			else if (key == GLFW_KEY_9)
				op.color.g = op.color.g == 1.f ? 0.f : 1.f;
			else if (key == GLFW_KEY_0)
				op.color.b = op.color.b == 1.f ? 0.f : 1.f;
			else if (key == GLFW_KEY_ENTER)
			{
				if (op.diffuse)
				{
					op.diffuse = false;
					op.specular = false;
					cameraAngle = CameraAngleTop;
				}
				else
				{
					op.diffuse = true;
					op.specular = true;
					cameraAngle = CameraAnglePreset;
				}
			}
		}
	}

	struct ZATACKA_CONTEXT {
		DisplayManager displayManager;
		Light light;
		Camera camera;
		Loader loader;
		TexturedModel arrowModel;
		Entity arrow;
		GLuint defaultTexture;
		GLine* pLine;
		RenderLines renderLines;
		MasterRenderer* pRenderer;
		GLFWwindow* window;

		int animation;
		bool jumping;
		bool falling;

		float cameraDist;
		float cameraZOffset;

		glm::vec2 Camera2D;

		const float speed = 1.f / 5.f;
		const float turnSpeed = 7.f;
		float currentSpeed;

		const float turnDist = .25f;
		float currentDist;
		bool turning;

		const float distBetweenHoles = 30.f;
		const float holeSize = 2.7f;
		bool hole;
		float holeDist;

		unsigned int ticksBeforeLine;

		unsigned char button4;
		unsigned char button5;

		const int FPS = 60;

		void Init() {
			displayManager.createDisplay();
			printf("Display created\n");
//		glEnable(GL_CULL_FACE);
//		glCullFace(GL_BACK);
			glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
			glEnable(GL_BLEND);
			light = { glm::vec3(0.f, 1000.f, 0.f), glm::vec3(1.f, 1.f, 1.f) };
			camera = { glm::vec3(0.f, 30.f, -5.f), -75.f, 180.f, 0.f };
			printf("loadModel\n");
			arrowModel = loadModel(&loader, "Resources/arrow2.obj");
			printf("Model loaded: vao:%d, size:%d\n", arrowModel.model.vao, arrowModel.model.size);
			arrow = { glm::vec3(0.f, 0.f, 0.f), glm::vec3(0.f, 0.f, 0.f), glm::vec3(1.f, 1.f, .6180f) };
			printf("loadTexture\n");
			defaultTexture = loader.loadTexture("Resources/default.bmp");
			printf("Texture loaded: %d\n", defaultTexture);
			pLine = new GLine();
			renderLines.count = 1;
			pRenderer = new MasterRenderer(defaultTexture, &loader);
			pRenderer->addEntity(&arrowModel, &arrow);
			window = displayManager.getWindow();
			glfwSetKeyCallback(window, key_callback);

			animation = 0;
			jumping = false;
			falling = false;

			cameraAngle = CameraAnglePreset;
			cameraDist = 48.f;
			cameraZOffset = -2.f;

			Camera2D = arrow.position;

			currentSpeed = speed;

			currentDist = turnDist;
			turning = false;

			hole = false;
			holeDist = distBetweenHoles;

			ticksBeforeLine = 180;

			button4 = GLFW_RELEASE;
			button5 = GLFW_RELEASE;
		}

		void Update() {

			const double StartFrameTimeMs = glfwGetTime() * 1000.0;

			if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
				displayManager.requestClose();

			if (ticksBeforeLine > 0)
			{
				ticksBeforeLine--;

				if (ticksBeforeLine == 0)
				{
					pLine->initPoint(glm::vec2(arrow.position.x, arrow.position.z), glm::vec2(std::cos(toRadian(arrow.rotation.y)), -std::sin(toRadian(arrow.rotation.y))));
					op.colored_arrow = false;
				}
			}

			if (glfwJoystickPresent(GLFW_JOYSTICK_1))
			{
				int count;
				const unsigned char* buttons = glfwGetJoystickButtons(GLFW_JOYSTICK_1, &count);
#if 0
				for (int i = 0; i < count; ++i) {
					std::cout << "Button[" << i << "]: " << (unsigned int)buttons[i] << std::endl;
				}
#endif

				if (buttons[4] != button4)
				{
					button4 = buttons[4];

					if (button4 == GLFW_PRESS)
					{
						if (cameraAngle != 90.f)
							cameraAngle = 90.f;
						else
							cameraAngle = 60.f;
					}
				}

				if (buttons[5] != button5)
				{
					button5 = buttons[5];

					if (button5 == GLFW_PRESS)
					{
						op.diffuse = !op.diffuse;
						op.specular = !op.specular;
					}
				}
			}

			if (glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS)
				arrow.rotation.y -= turnSpeed;
			if (glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS)
				arrow.rotation.y += turnSpeed;

			bool currentTurning = ((glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS) ^ (glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS));

			if (glfwJoystickPresent(GLFW_JOYSTICK_1))
			{
				int count;
				const float* axes = glfwGetJoystickAxes(GLFW_JOYSTICK_1, &count);
#if 0
				if (axes[0] >= 0.2f)
				{
					arrow.rotation.y -= turnSpeed;
					currentTurning = true;
				}
				else if (axes[0] <= -0.2f)
				{
					arrow.rotation.y += turnSpeed;
					currentTurning = true;
				}
				else
				{
					currentTurning = false;
				}
#else
				arrow.rotation.y -= turnSpeed * axes[0];

				if (axes[0] != 0.f)
					currentTurning = true;
#endif

				currentSpeed = speed * (1.f + (axes[5] + 1.f) * .5f);
			}

			if (glfwGetKey(window, GLFW_KEY_H) != GLFW_PRESS)
			{
				const glm::vec2 dir = glm::normalize(glm::vec2(std::cos(toRadian(arrow.rotation.y)), -std::sin(toRadian(arrow.rotation.y))));

				arrow.position.x += dir.x * currentSpeed;
				arrow.position.z += dir.y * currentSpeed;

				if (ticksBeforeLine == 0)
				{
					currentDist += currentSpeed;

					holeDist -= currentSpeed;

					if (holeDist <= 0.f)
					{
						//					line.addPoint(glm::vec2(arrow.position.x, arrow.position.z), glm::vec2(std::cos(toRadian(arrow.rotation.y)), -std::sin(toRadian(arrow.rotation.y))));

						if (hole)
							holeDist = distBetweenHoles;
						else
							holeDist = holeSize;

						hole = !hole;

						pLine->updateLast(glm::vec2(arrow.position.x, arrow.position.z), glm::vec2(std::cos(toRadian(arrow.rotation.y)), -std::sin(toRadian(arrow.rotation.y))));
						pLine->addPoint(glm::vec2(arrow.position.x, arrow.position.z), glm::vec2(std::cos(toRadian(arrow.rotation.y)), -std::sin(toRadian(arrow.rotation.y))), hole ? 1.f : 0.f);
					}

//				if (!hole)
					else
					{
						if (turning != currentTurning || (currentTurning && currentDist >= turnDist))
						{
							pLine->updateLast(glm::vec2(arrow.position.x, arrow.position.z), glm::vec2(std::cos(toRadian(arrow.rotation.y)), -std::sin(toRadian(arrow.rotation.y))));
							pLine->addPoint(glm::vec2(arrow.position.x, arrow.position.z), glm::vec2(std::cos(toRadian(arrow.rotation.y)), -std::sin(toRadian(arrow.rotation.y))), hole ? 1.f : 0.f);
//						pLine->linkPoint(glm::vec2(arrow.position.x, arrow.position.z), glm::vec2(std::cos(toRadian(arrow.rotation.y)), -std::sin(toRadian(arrow.rotation.y))), hole ? 1.f : 0.f);
							currentDist = 0.f;
						}
						else
							pLine->updateLast(glm::vec2(arrow.position.x, arrow.position.z), glm::vec2(std::cos(toRadian(arrow.rotation.y)), -std::sin(toRadian(arrow.rotation.y))));
					}

					turning = currentTurning;
				}
//				std::cout << "Position: x=" << arrow.position.x << ", z=" << arrow.position.z << std::endl;
			}

			if (!jumping && !falling && glfwGetKey(window, GLFW_KEY_SPACE) == GLFW_PRESS)
				jumping = true;

			if (animation == 0 && glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS)
			{
				animation = 360;
				currentSpeed = speed * 2;
			}

			if (animation != 0)
			{
				animation -= 12;
				arrow.rotation.x = (float)animation;

				if (animation == 0)
					currentSpeed = speed;
			}

			if (falling)
			{
				arrow.position.y -= .1f;

				if (arrow.position.y <= 0.1f)
					falling = false;
			}

			if (jumping)
			{
				arrow.position.y += .1f;

				if (arrow.position.y >= 5.f)
				{
					jumping = false;
					falling = true;
				}
			}

			// Camera

			if (glfwGetKey(window, GLFW_KEY_KP_ADD) == GLFW_PRESS)
			{
				cameraDist -= .5f;
				std::cout << "dist: " << cameraDist << std::endl;
			}

			if (glfwGetKey(window, GLFW_KEY_KP_SUBTRACT) == GLFW_PRESS)
			{
				cameraDist += .5f;
				std::cout << "dist: " << cameraDist << std::endl;
			}

			if (glfwGetKey(window, GLFW_KEY_KP_8) == GLFW_PRESS)
			{
				cameraAngle++;

				if (cameraAngle > 90.f)
					cameraAngle = 90.f;
				std::cout << "angle: " << cameraAngle << std::endl;
			}

			if (glfwGetKey(window, GLFW_KEY_KP_2) == GLFW_PRESS)
			{
				cameraAngle--;

				if (cameraAngle < 0.f)
					cameraAngle = 0.f;
				std::cout << "angle: " << cameraAngle << std::endl;
			}

			if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
			{
				cameraZOffset += .1f;
				std::cout << "Camera Z Offset: " << cameraZOffset << std::endl;
			}

			if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
			{
				cameraZOffset -= .1f;
				std::cout << "Camera Z Offset: " << cameraZOffset << std::endl;
			}

			const float CameraSizeX = 6.f * 5.f;
			const float CameraSizeY = 9.f / 16.f * CameraSizeX;

			switch (CameraType) {
			case CENTERED:
				Camera2D.x = arrow.position.x;
				Camera2D.y = arrow.position.z;
				break;
			case BORDER:
				if (arrow.position.x > Camera2D.x + CameraSizeX)
					Camera2D.x = arrow.position.x - CameraSizeX;
				else if (arrow.position.x < Camera2D.x - CameraSizeX)
					Camera2D.x = arrow.position.x + CameraSizeX;
				if (arrow.position.z > Camera2D.y + CameraSizeY)
					Camera2D.y = arrow.position.z - CameraSizeY;
				else if (arrow.position.z < Camera2D.y - CameraSizeY)
					Camera2D.y = arrow.position.z + CameraSizeY;
				break;
			case FRONT: {
				const glm::vec2 dir = glm::normalize(glm::vec2(std::cos(toRadian(arrow.rotation.y)), -std::sin(toRadian(arrow.rotation.y))));
				const glm::vec2 TargetPos(arrow.position.x + dir.x * (CameraSizeX * currentSpeed * 5.f), arrow.position.z + dir.y * (CameraSizeY * currentSpeed * 5.f));
				Camera2D.x += (TargetPos.x - Camera2D.x) / 64.f;
				Camera2D.y += (TargetPos.y - Camera2D.y) / 64.f;
				break;
			}
			default:
				breakpoint();
				break;
			}

			camera.position.x = Camera2D.x;
			camera.position.y = 0.f;
			camera.position.z = Camera2D.y;
			camera.pitch = -cameraAngle;

			if (glfwJoystickPresent(GLFW_JOYSTICK_1))
			{
				int count;
				const float* axes = glfwGetJoystickAxes(GLFW_JOYSTICK_1, &count);

				camera.position.x -= 6.f * 5.f * axes[2] * .5f;
				camera.position.z += 6.f * 5.f * axes[3] * .25f;
#if 0
				for (int i = 0; i < count; ++i)
					std::cout << "Axe[" << i << "]: " << axes[i] << std::endl;
#endif
			}

			camera.position.y = cameraDist * std::sin(toRadian(cameraAngle));
			camera.position.z = camera.position.z - cameraDist * std::cos(toRadian(cameraAngle)) + cameraZOffset;

			renderLines.renderLines[0].vao = pLine->vao;
			renderLines.renderLines[0].size = pLine->elements;
			renderLines.renderLines[0].options.color = op.color;

			pRenderer->render(camera, light, renderLines, op);
			displayManager.updateDisplay();

			double TimeDiffMs;

			do {
				const double EndFrameTimeMs = glfwGetTime() * 1000.f;
				TimeDiffMs = EndFrameTimeMs - StartFrameTimeMs;
			} while (TimeDiffMs < 1000.f / (double)FPS);
		}

		void Shutdown() {
			pRenderer->cleanUp();
			delete pRenderer;
			loader.cleanUp();
			delete pLine;
			displayManager.closeDisplay();
		}
	};

#ifdef __EMSCRIPTEN__
	void MainLoop(void* pData) {
		ZATACKA_CONTEXT& Context = *(ZATACKA_CONTEXT*)pData;
		Context.Update();
	}

	void RunMainLoop(ZATACKA_CONTEXT& Context) {
		printf("emscripten_set_main_loop_arg\n");
		emscripten_set_main_loop_arg(MainLoop, &Context, Context.FPS, 1);
	}
#else
	void RunMainLoop(ZATACKA_CONTEXT& Context) {
		printf("desktop main loop\n");
		while (!Context.displayManager.isCloseRequested())
			Context.Update();
	}
#endif

	void zatackaProto()
	{
		printf("zatackaProto\n");
		ZATACKA_CONTEXT Context;
		printf("Context.Init\n");
		Context.Init();
		RunMainLoop(Context);
		Context.Shutdown();
	}
}

int main()
{
	//mainGameLoop();
	zatackaProto();
	return 0;
}