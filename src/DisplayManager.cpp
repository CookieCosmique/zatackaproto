#include "ProjectDefines.h"

#include "Assert.h"
#include "CheckOpenGLError.h"

#include "DisplayManager.h"

namespace
{
	int InitGlew(void)
	{
#if defined(_WIN32) || defined(_WIN64) || defined(__CYGWIN__) || defined(__linux__)
		glewExperimental = GL_TRUE;
		GLenum error = glewInit();
		AssertErrorMsg(error == GLEW_OK, "Error while initializing glew: ", glewGetErrorString(error));
		return error;
#endif
		return 0;
	}
	void ErrorCallback(int id, const char* description)
	{
		AssertErrorMsg(false, "OpenglError: ", id, " - ", description);
#ifndef DEBUG
		(void)id;
		(void)description;
#endif
	}
}

const int DisplayManager::WIDTH = 1280;
const int DisplayManager::HEIGHT = 720;

DisplayManager::DisplayManager() :
	window(nullptr)
{

}

void DisplayManager::createDisplay()
{
	glfwSetErrorCallback(&ErrorCallback);

	if (glfwInit() == 0)
		AssertErrorMsg(false, "Error: glfwInit");

#if defined(_WIN32) || defined(__CYGWIN__)
	const int OpenGLVersionMajor = 4;
	const int OpenGLVersionMinor = 1;
#elif defined(__linux__)
	const int OpenGLVersionMajor = 3;
	const int OpenGLVersionMinor = 3;
#else
	const int OpenGLVersionMajor = 3;
	const int OpenGLVersionMinor = 2;
#endif

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, OpenGLVersionMajor); CGLE();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, OpenGLVersionMinor); CGLE();

	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE); CGLE();
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); CGLE();
	glfwWindowHint(GLFW_RESIZABLE, GL_FALSE); CGLE();
	glfwWindowHint(GLFW_FOCUSED, GL_TRUE); CGLE();
	glfwWindowHint(GLFW_VISIBLE, GL_TRUE); CGLE();
	//glfwWindowHint(GLFW_REFRESH_RATE, 60); CGLE();
	glfwWindowHint(GLFW_DECORATED, GL_TRUE); CGLE();
#if defined(_WIN32) || defined(__CYGWIN__)
	glfwWindowHint(GLFW_SAMPLES, 16); CGLE();
#endif

	glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GL_TRUE); CGLE();

	this->window = glfwCreateWindow(WIDTH, HEIGHT, "Proto", nullptr, nullptr);

	const GLFWvidmode* mode = glfwGetVideoMode(glfwGetPrimaryMonitor());

	glfwSetWindowPos(this->window, (mode->width - WIDTH) / 2, (mode->height - HEIGHT) / 2); CGLE();

	glfwMakeContextCurrent(this->window); CGLE();

#ifndef __EMSCRIPTEN__
	glfwSwapInterval(0); CGLE();
#endif

	InitGlew();

	AssertFatal(glGenVertexArrays != NULL);

	lastFrameTime = getCurrentTime();
}

void DisplayManager::updateDisplay()
{
	AssertFatal(this->window != nullptr);
	glfwSwapBuffers(this->window); CGLE();
	glfwPollEvents(); CGLE();

	long currentFrameTime = getCurrentTime();
	delta = (currentFrameTime - lastFrameTime) / 1000.f;
	lastFrameTime = currentFrameTime;
}

void DisplayManager::closeDisplay()
{
	glfwDestroyWindow(this->window); CGLE();
	this->window = nullptr;
}

void DisplayManager::requestClose()
{
	glfwSetWindowShouldClose(this->window, 1);
}

bool DisplayManager::isCloseRequested()
{
	return glfwWindowShouldClose(this->window) != 0;
}

long DisplayManager::getCurrentTime()
{
	return (long)(glfwGetTime() * 1000);
}
