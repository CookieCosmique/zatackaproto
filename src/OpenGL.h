#pragma once

#ifdef __APPLE__
# define GLFW_INCLUDE_GLCOREARB
#elif defined(_WIN32) || defined(_WIN64) || defined(__CYGWIN__)
# define GLFW_EXPOSE_NATIVE_WIN32
# include <windows.h>
# include <windef.h>
# define GLEW_STATIC
# include "GL/glew.h"
#elif defined(__linux__)
# define GLEW_STATIC
# include <stdlib.h>
# include "GL/glew.h"
#elif defined(__EMSCRIPTEN__)
# define GL_GLEXT_PROTOTYPES 1
# include <GLES3/gl3.h>
# include <GLES3/gl2ext.h>
# include <emscripten.h>
# include <GL/gl.h>
# include <GL/glext.h>
# define GLFW_INCLUDE_ES3
#endif

#include <GLFW/glfw3.h>
#ifndef __EMSCRIPTEN__
# include <GLFW/glfw3native.h>
#endif
