#include "../ProjectDefines.h"

#include "image_data.h"

#include "Types.h"

//#include <iostream>
#include <string.h>

namespace PNG
{
	char(*blitFuncArray[8]) (t_image_recovering *, singlet *, const u16) = { &blitPASS0, &blitPASS1, &blitPASS2, &blitPASS3, &blitPASS4, &blitPASS5, &blitPASS6, &blitPASS7};

	char		createImageData(t_image_data *img, t_IHDR_chunk *ihdr, PNG_REVERSE v_reverse)
	{
		img->width = ihdr->width;
		img->height = ihdr->height;
		img->bit_depth = ihdr->bit_depth;
		img->colour_type = ihdr->colour_type;
		img->v_reverse = v_reverse;

		if (ihdr->colour_type == 0)			//greyscale
			img->channel = 1;
		else if (ihdr->colour_type == 2)	//rgb
			img->channel = 3;
		else if (ihdr->colour_type == 3)	//prefer not to talk about it
		{
			//std::cout << "palette type color not supported" << std::endl;
			return 0;
		}
		else if (ihdr->colour_type == 4)
			img->channel = 2;				//grayscale + alpha
		else if (ihdr->colour_type == 6)
			img->channel = 4;				//rgba

		img->pix_size = img->channel * img->bit_depth / 8;

		img->data_size = (img->width * img->height * img->pix_size);
		img->data = MY_NEW singlet[static_cast<unsigned int>(img->data_size)];

		return 1;
	}

	char		copySomeScanLine(t_image_data *, t_unzib_stream *)
	{
		return 1;
	}

	char		uncoverImage(t_image_data *img, t_IHDR_chunk *ihdr, t_unzib_stream *unz)
	{
		t_image_recovering	recover;	//i have to be careful, recover filter is actualy 0 normaly, define that
										//the png will use for EACH SCANLINE one of the 5 defined filter type

		singlet	*scanline_a;	//the first scanline buffer
		singlet	*scanline_b;	//the second scanline buffer

		bool	three_lines_fuck;

		recover = {};
		recover.img = img;

		recover.filter = ihdr->filter;
		if (ihdr->interlace == 0)
			recover.current_pass = 0;
		else
			recover.current_pass = 1;
		//use 7777--- line to stock the data, scanline by scanline ?, then copy it where needed, use the two 7 line as scanline

		three_lines_fuck = (img->height == 3);

		scanline_a = scanlinePointer(&recover, 1);
		scanline_b = scanlinePointer(&recover, 3); //if there is exactly 3lines, im in trouble
		if (three_lines_fuck)
			scanline_b = MY_NEW singlet[img->width * img->pix_size / 2];

		if (recover.current_pass == 0)
		{
			recover.current_pass_height = recover.img->height;
			recover.current_pass_width = recover.img->width;

			recover.current_scanline = 0;
			while (recover.current_scanline < recover.current_pass_height)
			{
				if (recover.current_scanline == 0)
					getUnzibScanline(scanlinePointer(&recover, 0), nullptr, img->pix_size, recover.current_pass_width, unz);
				else
					getUnzibScanline(scanlinePointer(&recover, recover.current_scanline),
						scanlinePointer(&recover, recover.current_scanline - 1),
						img->pix_size, recover.current_pass_width, unz);

				recover.current_scanline++;
			}
		}
		else
		{
			while (recover.current_pass < 7)	//pass 1 through 6 or, 7 is handled differently
			{
				recover.current_pass_height = PASS_N_HEIGHT(recover.img->height, recover.current_pass);
				recover.current_pass_width = PASS_N_WIDTH(recover.img->width, recover.current_pass);

				recover.current_scanline = 0;
				if (recover.current_pass_width > 0)	//so i dont try to get filter byte on empty pass
				{
					while (recover.current_scanline < recover.current_pass_height)
					{
						if (recover.current_scanline == 0)
							getUnzibScanline(scanline_a, nullptr, img->pix_size, recover.current_pass_width, unz);
						else
							getUnzibScanline(scanline_a, scanline_b, img->pix_size, recover.current_pass_width, unz);
						blitScanlineIMG(&recover, scanline_a, img->pix_size);
						recover.current_scanline++;
						if (recover.current_scanline < recover.current_pass_height)
						{
							getUnzibScanline(scanline_b, scanline_a, img->pix_size, recover.current_pass_width, unz);
							blitScanlineIMG(&recover, scanline_b, img->pix_size);
							recover.current_scanline++;
						}
					}
				}
				recover.current_pass++;
			}

			recover.current_pass_height = PASS_N_HEIGHT(recover.img->height, recover.current_pass);
			recover.current_pass_width = recover.img->width;

			recover.current_scanline = 0;
			while (recover.current_scanline < recover.current_pass_height)
			{
				if (recover.current_scanline == 0)
					getUnzibScanline(scanlinePointer(&recover, 1),
						nullptr, img->pix_size, recover.current_pass_width, unz);
				else
					getUnzibScanline(scanlinePointer(&recover, (recover.current_scanline * 2) + 1),
						scanlinePointer(&recover, (recover.current_scanline * 2) - 1),
						img->pix_size, recover.current_pass_width, unz);

				recover.current_scanline++;
			}
		}


		if (three_lines_fuck)
			MY_DELETE_ARRAY(scanline_b);

		return 1;
	}

	inline singlet	*scanlinePointer(t_image_recovering *rec, u64 scanline_id)
	{
		if (rec->img->v_reverse == NO_REVERSE)
			return (rec->img->data + rec->img->width * rec->img->pix_size * scanline_id);
		else if (rec->img->v_reverse == V_REVERSE)
			return (rec->img->data + rec->img->width * rec->img->pix_size * (rec->img->height - scanline_id - 1));
		else
			return (0);
	}

	inline singlet	*offScanPointer(t_image_recovering *rec, u16 freq, u16 off)
	{
		return scanlinePointer(rec, rec->current_scanline * freq + off);
	}

	char		blitScanlineIMG(t_image_recovering *rec, singlet *scanline, const u16 pix_size)
	{
		return ((blitFuncArray[rec->current_pass])(rec, scanline, pix_size));
	}

	//		  Interlacing Adam 7
	//	    1  2  3  4  5  6  7  8
	//	   /////////////////////////
	// 1 // 1  6  4  6  2  6  4  6
	// 2 // 7  7  7  7  7  7  7  7
	// 3 // 5  6  5  6  5  6  5  6
	// 4 // 7  7  7  7  7  7  7  7
	// 5 // 3  6  4  6  3  6  4  6
	// 6 // 7  7  7  7  7  7  7  7
	// 7 // 5  6  5  6  5  6  5  6
	// 8 // 7  7  7  7  7  7  7  7

		//blitPASS__N	-> __freq, how much space to skip (8 ? 4 ? 2 ?) (define pixel frequency, 1/8, 1/4 ..)
		//				-> __offset, the offset in pixels for this pass
/*#	define		BLIT_PASS_N(__n, __w_freq, __w_offset, __h_freq, __h_offset)			\
	char		blitPASS##__n(t_image_recovering *rec, singlet *scanline, u16 pix_size)	\
	{																					\
		const	u64	max_len = pix_size * rec->current_pass_width;						\
		const	u64	pix_offset = __w_offset * pix_size;									\
		for (int i = 0; i < max_len; i += pix_size)										\
			memcpy(offScanPointer(rec, __h_freq, __h_offset) +							\
				(i * __w_freq) + pix_offset, scanline + i, pix_size);					\
																						\
		return 1;																		\
	}
	*/

#	define		BLIT_PASS_N(__n, __w_freq, __w_offset, __h_freq, __h_offset)					\
	char		blitPASS##__n(t_image_recovering *rec, singlet *scanline, const u16 pix_size)	\
	{																							\
		const	u64	max_len = pix_size * rec->current_pass_width;								\
		const	u64	pix_offset = __w_offset * pix_size;											\
		const	u32	freq_pix = __w_freq * pix_size;												\
																								\
		singlet	*const tmp_scan = offScanPointer(rec, __h_freq, __h_offset) +	pix_offset;		\
		u32			j = 0;																		\
		for (u32 i = 0; i < max_len; i += pix_size)												\
		{																						\
			memcpy(tmp_scan + j, scanline + i, pix_size);										\
			j += freq_pix;																		\
		}																						\
		return 1;																				\
	}

	BLIT_PASS_N(0, 1, 0, 1, 0);	//isnt used, copy immediatly to corresponding scanline in da body
	BLIT_PASS_N(1, 8, 0, 8, 0);
	BLIT_PASS_N(2, 8, 4, 8, 0);
	BLIT_PASS_N(3, 4, 0, 8, 4);
	//BLIT_PASS_N(4, 4, 2, 4, 0);
	//BLIT_PASS_N(5, 2, 0, 4, 2);
	//BLIT_PASS_N(6, 2, 1, 2, 0);
	BLIT_PASS_N(7, 1, 0, 2, 1);	//isnt used, copy immediatly to corresponding scanline in da body

	char		blitPASS4(t_image_recovering *rec, singlet *scanline, const u16 pix_size)
	{
		const	u64	max_len = pix_size * rec->current_pass_width;
		const	u64	pix_offset = 2 * pix_size;
		const	u32	freq_pix = 4 * pix_size;

		singlet		*tmp_scan = offScanPointer(rec, 4, 0) +	pix_offset;
		for (u32 i = 0; i < max_len; i += pix_size)
		{
			memcpy(tmp_scan, scanline + i, pix_size);
			tmp_scan += freq_pix;
		}
		return 1;
	}

	char		blitPASS5(t_image_recovering *rec, singlet *scanline, const u16 pix_size)
	{
		const	u64	max_len = pix_size * rec->current_pass_width;
		const	u32	freq_pix = 2 * pix_size;

		singlet		*tmp_scan = offScanPointer(rec, 4, 2);
		for (u32 i = 0; i < max_len; i += pix_size)
		{
			memcpy(tmp_scan, scanline + i, pix_size);
			tmp_scan += freq_pix;
		}

		return 1;
	}

	char		blitPASS6(t_image_recovering *rec, singlet *scanline, const u16 pix_size)
	{
		const	u64	max_len = pix_size * rec->current_pass_width;
		const	u64	pix_offset = pix_size;
		const	u32	freq_pix = 2 * pix_size;

		singlet		*tmp_scan = offScanPointer(rec, 2, 0) +	pix_offset;
		for (u32 i = 0; i < max_len; i += pix_size)
		{
			memcpy(tmp_scan, scanline + i, pix_size);
			tmp_scan += freq_pix;
		}

		return 1;
	}

}