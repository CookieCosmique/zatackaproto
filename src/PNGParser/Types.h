#pragma once

#ifdef _WIN32
#include <xmmintrin.h>
#endif

#ifdef _POSIX
#include <pthread.h>
#include <sys/types.h>
#endif

#if defined(_WIN32)
#include "WTypes.h"
#endif
#if defined(__GNUC__)
#include <stdint.h>
#endif

#if defined(_MSC_VER) && defined(WIN32)
	typedef signed   __int8     i8;
	typedef signed   __int16    i16;
	typedef signed   __int32    i32;
	typedef signed   __int64    i64;
	typedef unsigned __int8     u8;
	typedef unsigned __int16    u16;
	typedef unsigned __int32    u32;
	typedef unsigned __int64    u64;
	typedef u32                 LongBool;
#elif defined(__GNUC__)
	typedef int8_t              i8;
	typedef int16_t             i16;
	typedef int32_t             i32;
	typedef int64_t             i64;
	typedef uint8_t             u8;
	typedef uint16_t            u16;
	typedef uint32_t            u32;
	typedef uint64_t            u64;

	typedef u32                 LongBool;

#else
#   error "Plateforme non support�e encore"
#endif

	/*	STATIC_ASSERT(sizeof(short) == 2);
		STATIC_ASSERT(sizeof(int) == 4);

		STATIC_ASSERT(sizeof(u8) == 1);
		STATIC_ASSERT(sizeof(u16) == 2);
		STATIC_ASSERT(sizeof(u32) == 4);
		STATIC_ASSERT(sizeof(u64) == 8);

		STATIC_ASSERT(sizeof(i8) == 1);
		STATIC_ASSERT(sizeof(i16) == 2);
		STATIC_ASSERT(sizeof(i32) == 4);
		STATIC_ASSERT(sizeof(i64) == 8);*/
		//#define NO_SIMD

#define MY_DELETE_ARRAY(x) delete[] x
#define MY_DELETE(x) delete x
#define MY_NEW new
