#pragma once

#include "png_type.h"

namespace PNG
{
	typedef enum 	s_chunk_type
	{
		INVALID_CHUNK = 0,
		IHDR_CHUNK = 1,
		IDAT_CHUNK = 2,
		IPLT_CHUNK = 3,
		IEND_CHUNK = 4,
		ELSE_CHUNK = 5
	}				e_chunk_type;

	static const char *chunk_type_str[6] = { "Invalid Chunk",
							  "IHDR Chunk",
							  "IDAT Chunk",
							  "IPLT Chunk",
							  "IEND Chunk",
							  "Other Chunk" };
	typedef enum	s_color_type
	{
		COL_GREYSCALE = 0,
		COL_TRUECOLOR = 2,
		COL_INDEXED = 3,
		COL_GREYSCALE_A = 4,
		COL_TRUECOLOR_A = 6
	}				e_color_type;

	typedef struct	s_partial_chunk
	{
		quadet			length;
		quadet			code;
		e_chunk_type	c_type;
	}				t_partial_chunk;

	typedef struct	s_IHDR_chunk
	{
		const t_partial_chunk	*p_chunk;
		quadet			width;
		quadet			height;

		singlet			bit_depth;
		singlet			colour_type;	//can compare it to e_color_type;
		singlet			compression;
		singlet			filter;
		singlet			interlace;
		quadet			CRC;
	}				t_IHDR_chunk;

	typedef struct	s_IDAT_chunk
	{
		const t_partial_chunk	*p_chunk;
		/*singlet			zlib_comp;	//CMF
		singlet			flag;		//FLG
		quadet			CRC;	//only avalaible at the end

		unsigned char	CM;		//bit 0 to 3 of zlib_comp
		unsigned char	CINFO;	//bit 4 to 7 of zlib_comp; the base 2 log of LZ77 window - 8 (CINFO = 7 -> size is 2^15 = 32k)

		unsigned char	FCHECK; //bits 0 to 4 of flag; check bits	//CMF*256 + FLG should be = k * 31, fcheck is here to verify that
		unsigned char	FDICT;	//bits 5;	preset dictionary	-- there is a 4octets DICTID if this is true - uint
		unsigned char	FLEVEL;	//bits 6 to 7;	compression level 0 - fastest algorithm / 1 - fast algorithm / 2 - default algorithm / 3 - maximum compression, slowest//not needed for decompression
		unsigned int	DICTID;*/	//all these info belong to the idat handler

	}				t_IDAT_chunk;

	static const t_partial_chunk	IHDR_p_chunk = { 0x0000000d, 0x49484452, IHDR_CHUNK };
	static const t_partial_chunk	IEND_p_chunk = { 0x00000000, 0x49454e44, IEND_CHUNK };

	static const quadet				IDAT_code = 0x49444154;

	static const quadet				IHDR_r_code = 0x52444849;
	static const quadet				IDAT_r_code = 0x54414449;
	static const quadet				IPLT_r_code = 0x45544c50;
	static const quadet				IEND_r_code = 0x444e4549;

	static const quadet 			IHDR_r_size = 0x0d000000; //IHDR size is 13 aka 0x0000000d, reversed

	static const quadet				CHUNK_r_code[4] = { IHDR_r_code, IDAT_r_code, IPLT_r_code, IEND_r_code };
}