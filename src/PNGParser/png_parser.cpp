#include "../ProjectDefines.h"

#include "png_parser.h"

#include "../Assert.h"

#include <sstream>

/*
	1 octet = 2hex = 0xFF
	4 octets = 8hex = 0xFFFF FFFF

	Chunk Format :

	Length		: 4 octets - unsigned long int, the size of the DATA
	Chunk Type	: 4 octets - chunk type
	Chunk Data	: Length octets - the data of the chunk
	CRC			: 4 octets -


	CRC : only for the octets from chunk type and data :
		starts at 1,
		can use a table of all 8-bit message
		CRC is reverse, least significant bit is the ^31 (^32 = 1, and only 32bits so ^31 is the last stored, on the least significant bit)
		-> xor du CRC successif des octets un a un, et a la fin obtient un code 32bit
		-> la table est presente pour calculer plus rapidement, utiliser le code de w3

PNG format :
	png code : 8950 4e47 0d0a 1a0a

	IHDR Chunk	- length	: XXXX XXXX / should be 13 - 0000 000d
			  	- code		: 4948 4452
			  	- CRC		: XXXX XXXX

	IDAT Chunk	- length	: XXXX XXXX / can be anything
			  	- code		: 4944 4154
			  	- CRC		: XXXX XXXX

	PLTE Chunk	- length	: XXXX XXXX / can be from 1 to 256 entries of 3byte (so 3 to 768)
			  	- code		: 504c 5445
			  	- CRC		: XXXX XXXX

	IEND Chunk	- length	: XXXX XXXX / should be 0 - 0000 0000
			  	- code		: 4945 4e44
			  	- CRC		: XXXX XXXX / should be then ae42 6082

	Create a system that read the text and buffer it in with a +7octet additional end space the size of a long int, for immediate use of those number without problem,
	only need to sometime update the buffer;


	IDAT - zlib format :
	zlib comp method	- 1 octet
	additional flag		- 1 octet
	data				- n octet
	check value			- 4 octet
*/

namespace PNG
{
	const quadet	png_code_s = 0x89504e47;
	const quadet	png_code_e = 0x0d0a1a0a;

	const quadet	png_code_s_r = 0x474e5089; //reverse bytes order
	const quadet	png_code_e_r = 0x0a1a0a0d; //reverse bytes order

	const octetet	png_code_r = 0x0a1a0a0d474e5089LL;

	char			test_parse(const char *file_name, t_image_data *img, PNG_REVERSE v_reverse)
	{
		FILE	*file;
		char	tmp;

		img->data = nullptr;
		img->width = 0;
		img->height = 0;

#if (defined(_WIN32) || defined(_WIN64)) && !defined(__CYGWIN__)
		errno_t err = fopen_s(&file, file_name, "rb");
#else
		file = fopen(file_name, "rb");
#endif
		AssertErrorMsg(file != nullptr, file_name);

		if (file != NULL)
		{
			//ft_log(LOG_FINE, 1, "Opening file :  %s", file_name);
			//tmp = test_parse_on_file(file, img, v_reverse);
			tmp = cleaner_parse_file(file, img, v_reverse);
			fclose(file);
			//ft_log(LOG_FINE, -1, "Closing file :  %s", file_name);
		}
		else
			return PNG_CANT_OPEN_FILE;	//cant open the file

		//	AssertErrorMsg(true, "cant open file", file_name);

		//return (ft_log(LOG_FINE, 1, "Done/////"));
		return tmp;
	}

	char			test_parse_on_file(FILE *file, t_image_data *img, PNG_REVERSE v_reverse)
	{
		singlet			firstBuffer[0x21];//8 + 4 + 4 + 13 + 4 = 33 //0x21
		t_buffer		buffer;
		//singlet	buffer[BUFFER_SIZE];

		int				done;
		t_IHDR_chunk	ihdr;
		//t_IDAT_chunk	idat;
		t_idat_handler	idat_handl;
		t_unzib_stream	unz_stream;
		t_partial_chunk	tmp_p_chunk;

		bool	dont_get_again;

		//quadet	pngCode;

		//if idat chunk, all other chunk will be idat or no more idat, once i hit idat, treat all idat !
		//ft_log(LOG_FINE, 0, "Starting reading -- / %s", file);

		if (!getFirstBuffer(firstBuffer, file))
			return PNG_BUFFER_CREATION_ERROR;	//couldnt get the first buff completely

		if (!checkBufferStartValidity(firstBuffer))
			return PNG_INVALID_FILE_FORMAT;	//file isnt a valid PNG image

		//return (ft_log(LOG_ERROR, 0, "file isnt a valid PNG image"));
		//else
		//	ft_log(LOG_FINE, 0, "Valid PNG image start");

		getIHDRFromBuffer(&ihdr, firstBuffer);

		/*ft_log(LOG_FINE, 0, "IHDR value : \n \
				width : %u \n \
				height : %u \n \
				bitdepth : %u \n\
				colour type : %u \n\
				compression method: %u \n\
				filter method : %u \n\
				interlace method : %u ///", ihdr.width, ihdr.height, ihdr.bit_depth,
			ihdr.colour_type, ihdr.compression, ihdr.filter,
			ihdr.interlace);*/

		if (!newBuffer(&buffer, file))
			return 0;
		//	return (ft_log(LOG_ERROR, 0, "Couldnt create the buffer"));

		newIdatHandler(&idat_handl, &buffer, &tmp_p_chunk);
		createImageData(img, &ihdr, v_reverse);
		done = 0;
		dont_get_again = false;
		while (!done &&
			((dont_get_again && tmp_p_chunk.c_type != INVALID_CHUNK) ||
			(!dont_get_again && getChunkFromTableBuffer(&tmp_p_chunk, &buffer) != INVALID_CHUNK)))
		{
			dont_get_again = false;

			//ft_log(LOG_FINE, 0, "found chunk type %s// \ncode : %08x \nlength : %d", chunk_type_str[chunk_type], tmp_p_chunk.code, tmp_p_chunk.length);
			if (tmp_p_chunk.c_type == ELSE_CHUNK || tmp_p_chunk.c_type == IEND_CHUNK)
			{
				if (!jumpBuffer(&buffer, tmp_p_chunk.length + 4))
				{
					/*if (chunk_type == IEND_CHUNK)
						ft_log(LOG_FINE, 0, "IEND CHUNK FOUND and EOF atteined//");
					else
						return (ft_log(LOG_ERROR, 0, "EOF atteined, didnt found IENDCHUNK"));*/
					//AssertErrorMsg(true, "EOF atteined, without finding IENDCHUNK");
					if (tmp_p_chunk.c_type != IEND_CHUNK)
						return PNG_INVALID_FILE_FORMAT;	//EOF atteined without finding IENDCHUNK
				}

				if (tmp_p_chunk.c_type == IEND_CHUNK)
					done = 1;
			}
			else if (tmp_p_chunk.c_type == IDAT_CHUNK)	//should get all idat obj
			{
				dont_get_again = true;

				(void)treatIDAT(&idat_handl, &tmp_p_chunk, &buffer);
				newUnzibStream(&unz_stream, &idat_handl);

				uncoverImage(img, &ihdr, &unz_stream);	//uncover image will himself get the next chunk

				/*if (unz_stream.default_tree)
				{
					deleteHuffTree(unz_stream.default_tree);
					MY_DELETE(unz_stream.default_tree);
				}*/

				//AssertError(!treatIDAT(&idat, &tmp_p_chunk, &buffer));
				/*if (!treatIDAT(&idat, &tmp_p_chunk, &buffer))
					ft_log(LOG_ERROR, 0, "error when handling the idat chunk");
				else
					ft_log(LOG_FINE, 0, "IDAT//\n size : %d\nzlib compression method : %d / %d\n flags : %d// dict ? : %d",
						idat.p_chunk->length, idat.CM, idat.CINFO, idat.flag, idat.FDICT == 1);*/

			}
			//ft_log(LOG_FINE, 0, "Found IEND CHUNK, Done");
		}

		/*if (getQuad(&pngCode, file))
			ft_log(LOG_FINE, 0, "Png Code : / %08x", (*pngCode));*/

			/*if (checkExpectedQuad(file, (singlet *) &png_code_s) && checkExpectedQuad(file, (singlet *) &png_code_e))
				ft_log(LOG_FINE, 0, "Png Code correct ///");
			else
				return (ft_log(LOG_ERROR, 0, "ERROR /// the file isnt recognized as PNG///"));

			if (checkExpectedChunk(&IHDR_p_chunk, file))
				ft_log(LOG_FINE, 0, "IHDR chunk found ///");
			else
				return (ft_log(LOG_ERROR, 0, "ERROR /// No IHDR chunk found///"));

			if (getIHDRChunk(&ihdr, &IHDR_p_chunk, file))
				ft_log(LOG_FINE, 0, "IHDR value : \n \
					width : %d \n \
					height : %d \n \
					bitdepth : %d \n\
					colour type : %d \n\
					compression method: %d \n\
					filter method : %d \n\
					interlace method : %d \n///", ihdr.width, ihdr.height, ihdr.bit_depth,
													ihdr.colour_type, ihdr.compression, ihdr.filter,
													ihdr.interlace);
			else
				return (ft_log(LOG_ERROR, 0, "///couldnt get ihdr"));

			done = 0;
			while (!done && (chunk_type = getChunkFromTable(&tmp_p_chunk, file)) != INVALID_CHUNK)
			{
				ft_log(LOG_FINE, 0, "found chunk type %s// \ncode : %08x \nlength : %d", chunk_type_str[chunk_type], tmp_p_chunk.code, tmp_p_chunk.length);
				if (chunk_type == ELSE_CHUNK || chunk_type == IDAT_CHUNK ||chunk_type == IEND_CHUNK)
				{
					ft_log(LOG_FINE, 0, "jumping %d lines", tmp_p_chunk.length);
					if (!jumpChunkData(&tmp_p_chunk.length, file))
						ft_log(LOG_ERROR, 0, "EOF atteined, didnt found IENDCHUNK");
				}

				if (chunk_type == IEND_CHUNK)
				{
					done = 1;
					ft_log(LOG_FINE, 0, "Found IEND CHUNK, Done");
				}
			}*/
			/*while ((tmp = fgetc(file)) != EOF)
			{
				ft_log(LOG_FINE, 0, "%02x", val);
			}*/

		//return (ft_log(LOG_FINE, 0, "Done ///"));
		return PNG_FINE;
	}

	char			cleaner_parse_file(FILE *file, t_image_data *img, PNG_REVERSE v_reverse)
	{
		singlet			firstBuffer[0x21];
		t_buffer		buffer;

		int				done;
		t_IHDR_chunk	ihdr;
		t_idat_handler	idat_handl;
		t_unzib_stream	unz_stream;
		t_partial_chunk	tmp_p_chunk;

		bool	dont_get_again;

		if (!getFirstBuffer(firstBuffer, file))
			return PNG_BUFFER_CREATION_ERROR;	//couldnt get the first buff completely

		if (!checkBufferStartValidity(firstBuffer))
			return PNG_INVALID_FILE_FORMAT;	//file isnt a valid PNG image

											//return (ft_log(LOG_ERROR, 0, "file isnt a valid PNG image"));
											//else
											//	ft_log(LOG_FINE, 0, "Valid PNG image start");

		getIHDRFromBuffer(&ihdr, firstBuffer);

		if (!newBuffer(&buffer, file))
			return 0;

		newIdatHandler(&idat_handl, &buffer, &tmp_p_chunk);
		createImageData(img, &ihdr, v_reverse);
		done = 0;
		dont_get_again = false;
		while (!done &&
			((dont_get_again && tmp_p_chunk.c_type != INVALID_CHUNK) ||
			(!dont_get_again && getChunkFromTableBuffer(&tmp_p_chunk, &buffer) != INVALID_CHUNK)))
		{
			dont_get_again = false;

			if (tmp_p_chunk.c_type == ELSE_CHUNK || tmp_p_chunk.c_type == IEND_CHUNK)
			{
				if (!jumpBuffer(&buffer, tmp_p_chunk.length + 4))
				{
					if (tmp_p_chunk.c_type != IEND_CHUNK)
						return PNG_INVALID_FILE_FORMAT;	//EOF atteined without finding IENDCHUNK
				}

				if (tmp_p_chunk.c_type == IEND_CHUNK)
					done = 1;
			}
			else if (tmp_p_chunk.c_type == IDAT_CHUNK)	//should get all idat obj
			{
				dont_get_again = true;

				(void)treatIDAT(&idat_handl, &tmp_p_chunk, &buffer);
				newUnzibStream(&unz_stream, &idat_handl);

				uncoverImage(img, &ihdr, &unz_stream);	//uncover image will himself get the next chunk
			}
		}

		return PNG_FINE;
	}

	/*char			check_png_code(FILE *file)
	{
		int		i;
		int		correct;
		singlet	val;

		i = 0;
		correct = 1;
		while (correct && i < 4 && getChar(&val, file))
		{
			if (val == png_code_t[i])
				i++;
			else
				correct = 0;
		}

		return (correct);
	}*/

	char			getQuad(quadet *quad, FILE *file)
	{
		//	return (fread(quad, 1, 4, file) == 4);
		quadet	tmp;
		int		val;
		int		i;

		i = 0;
		*quad = 0x00000000;
		while (i < 4 && (val = fgetc(file)) != EOF)
		{
			tmp = val & 0x000000FF;
			*quad += tmp << (8 * (3 - i)); //normal order or reverse order ? (3 - i) * 8
			i++;
		}

		return (i == 4);
	}

	char			getExpectedQuad(quadet *quad, FILE *file, const singlet *exp)
	{
		quadet	tmp;
		int		val;
		int		i;
		int 	correct;

		i = 0;
		correct = 1;
		*quad = 0x00000000;
		while (correct && i < 4 && (val = fgetc(file)) != EOF)
		{
			if (val == exp[i])
			{
				tmp = val & 0x000000FF;
				*quad += tmp << (8 * (3 - i)); //normal order or reverse order ? (3 - i) * 8
				i++;
			}
			else
				correct = 0;
		}

		return (correct && (i == 4));
	}

	char			checkExpectedQuad(FILE *file, const quadet_t exp)
	{
		int		val;
		int		i;
		int 	correct;

		i = 0;
		correct = 1;
		while (correct && i < 4 && (val = fgetc(file)) != EOF)
		{
			if ((val & 0x00ff) == exp[3 - i])
				i++;
			else
				correct = 0;
		}

		return (correct && (i == 4));
	}

	char			getChar(singlet *car, FILE *file)
	{
		int		val;
		if ((val = fgetc(file)) != EOF)
		{
			*car = (char)val;
			return 1;
		}
		else
		{
			*car = 0;
			return 0;
		}
	}

	char			getChunk(t_partial_chunk *p_chunk, FILE *file)
	{
		int	correct;
		correct = 1;

		if (!getQuad(&p_chunk->length, file))
			correct = 0;

		if (!correct || !getQuad(&p_chunk->code, file))
			correct = 0;

		return correct;
	}

	e_chunk_type	getChunkFromTable(t_partial_chunk *p_chunk, FILE *file)
	{
		singlet			val;
		int				i;
		quadet			tmp;
		e_chunk_type	possibleChunk;

		static const char	CHUNK_end[4][2] = { {0x44, 0x52},
											  {0x41, 0x54},
											  {0x54, 0x45},
											  {0x4e, 0x44} };

		if (!getQuad(&p_chunk->length, file))
			return (INVALID_CHUNK);

		if (!getChar(&val, file))
			return (INVALID_CHUNK);

		//ft_log(LOG_FINE, 0, "val : %d", val);
		p_chunk->code = 0x00000000;
		tmp = val & 0x000000FF;
		p_chunk->code += tmp << (8 * 3); //i = 4
		i = 3;
		possibleChunk = ELSE_CHUNK;

		// 0 -IHDR Chunk	- code		: 4948 4452 //not valid here 49 48 44 52
		// 1 - IDAT Chunk	- code		: 4944 4154					 49 44 41 54
		// 2 - PLTE Chunk	- code		: 504c 5445					 50 4c 54 45
		// 3 - IEND Chunk	- code		: 4945 4e44					 49 45 4e 44

		//	52 - ---5 ---2      00
		//		 0101 0010      01
		//	54 - ---5 ---4      10
		//		 0101 0100      11		10
		//	45 - ---4 ---5
		//		 0100 0101    			01
		//	44 - ---4 ---4
		//		 0100 0100	- 			00
		//			x    x
		//ind = [v & 0b00010000 >> 3 + v & 0b00000001;

		if (val == 0x49)
		{
			if (!getChar(&val, file))
				return (INVALID_CHUNK);
			//ft_log(LOG_FINE, 0, "val : %d", val);
			p_chunk->code += (val & 0x000000FF) << (8 * (i - 1)); //normal order or reverse order ? (3 - i) * 8
			i = 2;

			if (val == 0x48)
				possibleChunk = IHDR_CHUNK;
			else if (val == 0x44)
				possibleChunk = IDAT_CHUNK;
			else if (val == 0x45)
				possibleChunk = IEND_CHUNK;
		}
		else if (val == 0x50)
			possibleChunk = IPLT_CHUNK;

		while (possibleChunk != ELSE_CHUNK && i != 0 && getChar(&val, file))
		{
			//ft_log(LOG_FINE, 0, "val : %d", val);
			p_chunk->code += (val & 0x000000FF) << (8 * (i - 1)); //normal order or reverse order ? (3 - i) * 8

			if (val != CHUNK_end[possibleChunk - 1][2 - i])
				possibleChunk = ELSE_CHUNK;
			i--;
		}

		if (i > 0 && possibleChunk != ELSE_CHUNK)
			return (INVALID_CHUNK);

		while (i != 0 && getChar(&val, file))
		{
			//ft_log(LOG_FINE, 0, "val : %d", val);
			p_chunk->code += (val & 0x000000FF) << (8 * (i - 1)); //normal order or reverse order ? (3 - i) * 8
			i--;
		}

		if (i > 0)
			return (INVALID_CHUNK);

		return (possibleChunk);
	}

	e_chunk_type	getChunkFromTableBuffer(t_partial_chunk *p_chunk, t_buffer *buffer)
	{
		quadet			*tmp;

		static const s_chunk_type	reroute[4] = { IEND_CHUNK, IPLT_CHUNK, IDAT_CHUNK, ELSE_CHUNK };

		if (!getRevQuadBuffer(buffer, &p_chunk->length))
			return (INVALID_CHUNK);

		if (!getQuadBuffer(buffer, &tmp))
			return (INVALID_CHUNK);

		p_chunk->c_type = reroute[(((singlet *)tmp)[3] & 0b00000001) + ((((singlet *)tmp)[3] & 0b00010000) >> 3)];

		if (p_chunk->c_type != ELSE_CHUNK && CHUNK_r_code[p_chunk->c_type - 1] != *tmp)
			p_chunk->c_type = ELSE_CHUNK;

		//if (possibleChunk == ELSE_CHUNK)
		p_chunk->code = reverseOctetOrder(tmp);

		return (p_chunk->c_type);
	}

	char			getExpectedCodeChunk(t_partial_chunk *p_chunk, FILE *file)
	{
		if (!getQuad(&p_chunk->length, file))
			return (0);

		if (!checkExpectedQuad(file, (singlet *)&p_chunk->code))
			return (0);

		return (1);
	}

	char			checkExpectedChunk(const t_partial_chunk *p_chunk, FILE *file)
	{
		if (!checkExpectedQuad(file, (singlet *)&p_chunk->length))
			return (0);
		if (!checkExpectedQuad(file, (singlet *)&p_chunk->code))
			return (0);

		return (1);
	}

	char			getIHDRChunk(t_IHDR_chunk *ihdr, t_partial_chunk *p_chunk, FILE *file)
	{
		ihdr->p_chunk = p_chunk;

		if (!getQuad(&ihdr->width, file) ||
			!getQuad(&ihdr->height, file) ||
			!getChar(&ihdr->bit_depth, file) ||
			!getChar(&ihdr->colour_type, file) ||
			!getChar(&ihdr->compression, file) ||
			!getChar(&ihdr->filter, file) ||
			!getChar(&ihdr->interlace, file) ||
			!getQuad(&ihdr->CRC, file))
			return 0;

		return 1;
	}

	void			printQuadet(quadet)
	{
		//std::cout << quad << std::endl;
		//ft_log(LOG_FINE, 0, "/%08x", quad);
	}

	void			printOctetet(octetet)
	{
		//ft_log(LOG_FINE, 0, "/%08x%08x", oct >> 32, oct);
	}

	void			printIntQuadet(quadet)
	{
		//ft_log(LOG_FINE, 0, "/%d", quad);
	}

	char			checkBufferStartValidity(const singlet *buffer)	//33octets buffer
	{
		return ((*((octetet *)(&buffer[0x00])) == png_code_r) &&
			(*((quadet *)(&buffer[0x08])) == IHDR_r_size) &&
			(*((quadet *)(&buffer[0x0c])) == IHDR_r_code));
	}

	char			jumpOctets(const quadet *length, FILE *file)
	{
		quadet tmp;

		tmp = *length;
		while (tmp != 0 && fgetc(file) != EOF)
			tmp--;

		return (tmp == 0);
		//	return (fread(NULL, *length, 1, file) != *length);
	}

	char			jumpChunkData(const quadet *length, FILE *file)
	{
		quadet	length_with_crc = *length + 4;

		return (jumpOctets(&length_with_crc, file));
	}

	char			treatIDAT(t_idat_handler *idat_handl, const t_partial_chunk *p_chunk, t_buffer *buffer)
	{
		//idat->p_chunk = p_chunk;
		idat_handl->idat_rem_data = p_chunk->length;	//pourquoi 6 ?

		if (!getSingleCopBuffer(buffer, &idat_handl->zlib_comp))
			return (0);
		if (!getSingleCopBuffer(buffer, &idat_handl->flag))
			return (0);

		idat_handl->CM = idat_handl->zlib_comp & 0b00001111;			//compression method, should be 8 in png
		idat_handl->CINFO = (idat_handl->zlib_comp & 0b11110000) >> 4;	//compression info, if cm = 8 (should be), its the window size ln2 - 7, window size is
															//2^(CINFO + 8), max is 7, so 2^(7+8) max window size
		idat_handl->idat_rem_data -= 2;
		// [76543210] <- a byte
		//flag :
		//0-4	-> FCHECK (CMF*256 + FLG)-> multiple of 31
		//5		-> FDICT, is fdict present
		//7-8	-> FLEVEL, compression level only usefull for compression ?

		if ((idat_handl->FDICT = idat_handl->flag & 0b00100000))	//if FDICT is set, DICT is right after, bit 5 (from 0 to 7)
		{
			getRevQuadBuffer(buffer, &idat_handl->DICTID);
			idat_handl->idat_rem_data -= 4;
		}

		/*if (!jumpBuffer(buffer, data_length))
			return (0);
		if (!getRevQuadBuffer(buffer, &idat->CRC))	//this is the zlib crc, crc is calculated on DEFLATED (uncompressed) bytes of the zlib, but shouldnt handle it like that nonsense
			return (0);
		if (!jumpBuffer(buffer, 4)) //the usual crc
			return (0);*/

		return (updateIdatBuffer(idat_handl));
	}
}