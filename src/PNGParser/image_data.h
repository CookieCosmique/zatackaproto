#pragma once

#include "png_type.h"
#include "chunk.h"
#include "unzib_stream.h"

//		  Interlacing Adam 7
//	    1  2  3  4  5  6  7  8
//	   /////////////////////////
// 1 // 1  6  4  6  2  6  4  6
// 2 // 7  7  7  7  7  7  7  7
// 3 // 5  6  5  6  5  6  5  6
// 4 // 7  7  7  7  7  7  7  7
// 5 // 3  6  4  6  3  6  4  6
// 6 // 7  7  7  7  7  7  7  7
// 7 // 5  6  5  6  5  6  5  6
// 8 // 7  7  7  7  7  7  7  7

#define		LAST_BLOCK_W_PASS(__pass, __rem_size)											\
		(	(__pass == 1)					? (__rem_size > 0)							: (	\
			(__pass == 2)					? (__rem_size >= 5)							: (	\
			(__pass == 3) 					? ((__rem_size >= 1) + (__rem_size >= 5))	: (	\
			(__pass == 4)					? ((__rem_size >= 3) + (__rem_size >= 7))	: (	\
			(__pass == 5)					? ((__rem_size + 1)/2)						: (	\
			(__pass == 6)					? (__rem_size / 2)							: (	\
			(__pass == 7)					? (__rem_size)								: (	\
		0))))))))

#define		LAST_BLOCK_H_PASS(__pass, __rem_size)		\
		(	(__pass == 1 || __pass == 2)	? (__rem_size > 0)							: (	\
			(__pass == 3) 					? (__rem_size >= 5)							: (	\
			(__pass == 4)					? ((__rem_size >= 1) + (__rem_size >= 5))	: (	\
			(__pass == 5)					? ((__rem_size >= 3) + (__rem_size >= 7))	: (	\
			(__pass == 6)					? ((__rem_size + 1)/2)						: (	\
			(__pass == 7)					? (__rem_size / 2)							: (	\
		0)))))))

#define		PASS_N_HEIGHT(__height, __pass)																					\
	(	(__pass == 0)								? __height															: (	\
		(__pass == 1 || __pass == 2 || __pass == 3)	? (1 * (__height / 8)) + LAST_BLOCK_H_PASS(__pass, __height % 8)	: (	\
		(__pass == 4 || __pass == 5)				? (2 * (__height / 8)) + LAST_BLOCK_H_PASS(__pass, __height % 8)	: (	\
		(__pass == 6 || __pass == 7)				? (4 * (__height / 8)) + LAST_BLOCK_H_PASS(__pass, __height % 8)	: (	\
		-1)))))

#define		PASS_N_WIDTH(__width, __pass)																				\
	(	(__pass == 0)								? __width														: (	\
		(__pass == 1 || __pass == 2)				? (1 * (__width / 8)) + LAST_BLOCK_W_PASS(__pass, __width % 8)	: (	\
		(__pass == 3 || __pass == 4)				? (2 * (__width / 8)) + LAST_BLOCK_W_PASS(__pass, __width % 8)	: (	\
		(__pass == 5 || __pass == 6)				? (4 * (__width / 8)) + LAST_BLOCK_W_PASS(__pass, __width % 8)	: (	\
		(__pass == 7)								? __width														: (	\
		-1))))))

namespace PNG
{
	enum PNG_REVERSE
	{
		NO_REVERSE = 0,
		V_REVERSE = 1
	};
	typedef struct	s_image_data
	{
		u32				width;
		u32				height;

		u8				bit_depth;
		u8				channel;
		u8				pix_size;	//should be like 1 or 2 or 4, not > 256
		u8				colour_type;
		PNG_REVERSE		v_reverse;

		u64				data_size;
		singlet			*data;	//data size is width * height * 4 * (bit_depth / 8)
	}				t_image_data;

	typedef struct s_image_recovering
	{
		t_image_data	*img;

		u32				current_scanline;	//current scanline from the top, define position in data
		u32				current_scanline_width;

		u8				current_pass;		//0 mean no pass, not interlaced
		u32				current_pass_height;
		u32				current_pass_width;

		u8				filter;
	}				t_image_recovering;

	inline singlet	*scanlinePointer(t_image_recovering *rec, u64 scanline_id);
	inline singlet	*offScanPointer(t_image_recovering *rec, u16 freq, u16 off);

	char		createImageData(t_image_data *img, t_IHDR_chunk *ihdr, PNG_REVERSE v_reverse);

	char		uncoverImage(t_image_data *img, t_IHDR_chunk *ihdr, t_unzib_stream *unz);

	char		blitScanlineIMG(t_image_recovering *rec, singlet *scanline, const u16 pix_size);

	char		blitPASS0(t_image_recovering *rec, singlet *scanline, const u16 pix_size);
	char		blitPASS1(t_image_recovering *rec, singlet *scanline, const u16 pix_size);
	char		blitPASS2(t_image_recovering *rec, singlet *scanline, const u16 pix_size);
	char		blitPASS3(t_image_recovering *rec, singlet *scanline, const u16 pix_size);
	char		blitPASS4(t_image_recovering *rec, singlet *scanline, const u16 pix_size);
	char		blitPASS5(t_image_recovering *rec, singlet *scanline, const u16 pix_size);
	char		blitPASS6(t_image_recovering *rec, singlet *scanline, const u16 pix_size);
	char		blitPASS7(t_image_recovering *rec, singlet *scanline, const u16 pix_size);

	extern char(*blitFuncArray[8]) (t_image_recovering *, singlet *, const u16);
}