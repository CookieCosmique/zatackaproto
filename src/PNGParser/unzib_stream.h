#pragma once

#define UNZIB_WINDOW_REAL 0x00008000 //32k
//						  0x00010000 seems a bit faster than the 0x00008000, again 1% ?
#define UNZIB_BUF_SIZE UNZIB_WINDOW_REAL

#include "handle_idat.h"

#include <sstream>

//Unzib will uncompresse the handle_idat raw compresse zlib data, and have access to a 32k window to copy some part of himself, as recquire
//it will also give scanline from its own buffer that it will unfilter itself
//
//will need :
// -> HANDLE_IDAT reference, that will handle him some raw data, like :
//		-> bit by bit movement, singlet giver, quadet giver ?
//		-> need a way to reset, set to next full byte
//		-> a mem copy to copy uncompressed type data
// -> HANDLE_IDAT will give the zlib stream starting from the datablock

#define B_TYPE_NO_COMPRESSION 0b00
#define B_TYPE_FIXED_HUFFMAN 0b01
#define B_TYPE_DYNAMIC_HUFFMAN 0b10
#define B_TYPE_ERROR 0b11
#define B_TYPE_STARTING 0b100	//cant actualy get it from file, just set this at start

#define	FASTER_DONE_MASK		0b1000000000000000	//am i done mask, do if (mask & array[])
#define	FASTER_RES_MASK			0b0000000111111111	//if done, its the symb to get, 0-512 (only need 287) // if not done, is the pos in next array
#define	FASTER_BIT_COUNT_MASK	0b0000111000000000	//if done, will be the bit count i have moved, 0-7 bit counsumed //put only 3 bit, so 8 -> 0 9->1 etc ...
#define	FASTER_BYTE_COUNT_MASK	0b0110000000000000	//if done, will be the byte (octet) moved count, only between 1-2
#define FASTER_INV_DONE_MASK	0b0111111111111111	// at most, started at like 7 -> then +15, means 22bit pos, or 2byte + 6 then
														//actualy, will just give direct bit value and byte offset, since i do it only with 3byte margin,
														//wont have to check anything on idat side

#define FASTER_BIT_COUNT_MOV	9					//do (gna & bitcountmask) >> 9
#define FASTER_BYTE_COUNT_MOV	13					//do (gna & bytecountmask) >> 13
#define FASTER_NEW_BIT_MASK		0b0001111000000000

namespace PNG
{
	typedef struct	s_huffman_tree	//i believe this representation will work but im not sure yet
	{
		//stuff for huffman
		u16	alphabet_size;
		u8	*code_length;	//1 to 1 correspondance, between alphabet (0 - aphabet_size-1) and its length
							//i believe 256 is max code length ? or i hope so i at least
							//code length max is actualy 15
		i16	*symbol_array;	//representation in an array of the tree
							//start on case 0, then if stream = 0-> read pos, if stream = 1-> read pos + 1
							//then if val is < alphabet_size, output val, else, move to (val - alphabet_size) + pos
	}				t_huffman_tree;

	//w for writing, r for reading, since im writing to the buffer when i unzib the zib
	typedef struct	s_unzib_stream
	{
		singlet			buffer[2][UNZIB_BUF_SIZE];
		u16				faster_lit_len[1][0x8100];			//only one accel tree now
		t_idat_handler	*idatHandler;
		u8				w_currentBuffer;	//indicate which buffer is the "new one", and wich is the old one
		u8				r_currentBuffer;	//indicate if the reading have catched up with the writing buffer
		u32				r_index;
		u32				end_pos;		//where the writing to the buffer actually ends

		//decompression section

		u8				b_final;	//current zlib block is the last ?	//first block bit
		u8				b_type;		//current zlib block type			//2 next bit

		t_huffman_tree	*curr_lit_len_tree;
		t_huffman_tree	*curr_dist_tree;
		t_huffman_tree	*default_tree;

		bool			my_tree;	//just check the type of the current block

		bool			ended;
		bool			keep_copy;
		//for 00 type block
		u16				LEN;		//len of compressed data byte
		u16				NLEN;		//nlen, 1complement of len
		u16				curr_LEN;	//start at LEN, then decreased when i use it
	}				t_unzib_stream;

	char	newUnzibStream(t_unzib_stream *unzib, t_idat_handler *idatHandler);

///////////////////////////////////////////////////////
//--Image Recover interface, stuff to copy to image--//
///////////////////////////////////////////////////////------------------------------------------------------------------------------

	void	UnFilterType0(singlet *scan_target, singlet *scan_buffer, const u8 pixel_size, const u32 scan_width, t_unzib_stream *unz);	//singlet *res is an array of good size to handle a whole pixel
	void	UnFilterType1(singlet *scan_target, singlet *scan_buffer, const u8 pixel_size, const u32 scan_width, t_unzib_stream *unz);
	void	UnFilterType2(singlet *scan_target, singlet *scan_buffer, const u8 pixel_size, const u32 scan_width, t_unzib_stream *unz);
	void	UnFilterType3(singlet *scan_target, singlet *scan_buffer, const u8 pixel_size, const u32 scan_width, t_unzib_stream *unz);
	void	UnFilterType4(singlet *scan_target, singlet *scan_buffer, const u8 pixel_size, const u32 scan_width, t_unzib_stream *unz);

	char	getUnzibScanline(singlet *scanline_target, singlet *scanline_before, const u8 pixel_size, const u32 scanline_width, t_unzib_stream *unzib);	//return the unfiltered data to the scanline buffer of size pixel size * scanline_width, unsing the before scaline

	char	getUnzibSinglet(singlet *targ, t_unzib_stream *unzib);
	char	copyUnzibToTarg(singlet *targ, const u32 copSize, t_unzib_stream *unzib);

	extern inline u8	paeth_predicator(const u8 a, const u8 b, const u8 c);

	extern void(*filterType[5]) (singlet *, singlet *, const u8, const u32, t_unzib_stream *);


///////////////////////////////////////////////////////
//-- Decompression // To Fill up the unzib buffers --//
///////////////////////////////////////////////////////-------------------------------------------------------------------------------------

//	Default Huff Tree Function -------------

	void		getNextHuffSymbol(t_huffman_tree * tree, t_unzib_stream *unz, u16 *value);
	//inline void	getNextHuffDebugSymbol(t_huffman_tree * tree, t_unzib_stream *unz, u16 *value, u16 *res_counter, std::stringstream *out_stream);

	char	preTreatUncompressed(t_unzib_stream *unz);
	char	generateHuffmanTree(t_unzib_stream *unz);
	char	generateDefaultTree(t_unzib_stream *unz);

	char	generateLengthArrayFromTreeCode(t_huffman_tree *tree_code, u8 *length_array, u16 array_size, t_unzib_stream *unz);
	char	generateTreeBody(t_huffman_tree *tree);
	char	subGenerateTreeBody(t_huffman_tree *tree, u8 *length_size, u16 *first_len);
	char	deleteHuffTree(t_huffman_tree *tree);	//wont actually delete the tree pointer, only the body of the tree


	extern const u8		table_length_val[29];		//start at 257 -> 285 included	//contain val - 3, so max value is actualy 255, min is 0
	extern const u8		table_length_bitcount[29];

	extern const u16	table_dist_val[30];		//start at 257 -> 285 included	//contain val - 3, so max value is actualy 255, min is 0
	extern const u8		table_dist_bitcount[30];

	extern const u8		order_of_tree_code_length[19];

//  Misc Flow ---------------------------------------------

	char	updateUnzibBuffer(t_unzib_stream *unzib);	//ici que les choses compliqués se font

	extern inline void	applyTreeRule(const u16 tmp, t_unzib_stream *unz);
	void	decodeAndMove(u16 code, t_unzib_stream *unz);						//decode from stream the distance to move, and the length to copy, then call the copy func
	void	copyDistLength(const u16 length, const u16 dist, t_unzib_stream * const unz);

	void	applyRuleUncompressed(t_unzib_stream *unz);

	void	reachEOB(t_unzib_stream *unz);
	char	newBlockUnzib(t_unzib_stream *unz);

	void	copySingletToUnzlib(singlet symb, t_unzib_stream *unz);	//copy symb to unz current write buffer, and eventually swap buffer and all that
	void	copyBlockToUnzlib(singlet *src, u16 size, t_unzib_stream *unz);	//copy block of symb to unz current write buffer, and eventually swap buffer and all that
	void	setBlockToUnzlib(const singlet src, u16 size, t_unzib_stream *unz);	//copy block of symb to unz current write buffer, and eventually swap buffer and all that

//	Accell Tree -------------------------------------------------------------------

	const u8	lookup[16] = {	0x0, 0x8, 0x4, 0xc, 0x2, 0xa, 0x6, 0xe,
								0x1, 0x9, 0x5, 0xd, 0x3, 0xb, 0x7, 0xf};
	inline u8	reverse(u8 n) { return ((lookup[n & 0x0f] << 4) | lookup[n >> 4]); }

	void	getNextAccel_GEN(u16 const fast_sub_tree[0x8100], u16 * const res, t_idat_handler * const idat_handl);

	void	generateAccel(t_unzib_stream *unz, t_huffman_tree *tree);
	void	fillUpAccel_GEN(u16 fast_sub_tree[0x8100], u8 len[16], u16 first_len[16], u16 first_code[16], t_huffman_tree *tree);
}