#include "../ProjectDefines.h"

#include "handle_idat.h"
#include "png_parser.h"

//#include <iostream>
#include <string.h>

namespace PNG {

	u8		mask_array[8] = { 0b00000001, 0b00000010, 0b00000100, 0b00001000,
										0b00010000, 0b00100000, 0b01000000, 0b10000000 };

	char			newIdatHandler(t_idat_handler *idat_handl, t_buffer *buffer, t_partial_chunk *p_chunk)
	{
		idat_handl->file_buffer = buffer;
		idat_handl->p_chunk = p_chunk;

		idat_handl->index_o = 0;
		idat_handl->index_i = 0;
		//idat_handl->current_mask = 1;
		idat_handl->ended = false;

		return 1;
	}

#	define		GET_N_BIT_FUNCTION(__n)										\
	char			get##__n##bitcharIdat(t_idat_handler *idat_handl, u8 *res)	\
	{																		\
		return (getNbitCharIdat(idat_handl, res, __n));						\
	}

	GET_N_BIT_FUNCTION(1);
	GET_N_BIT_FUNCTION(2);
	GET_N_BIT_FUNCTION(3);
	GET_N_BIT_FUNCTION(4);
	GET_N_BIT_FUNCTION(5);
	GET_N_BIT_FUNCTION(6);
	GET_N_BIT_FUNCTION(7);
	GET_N_BIT_FUNCTION(8);

	char			getAByteIdat(t_idat_handler *idat_handl, singlet *res)	//same as up here, or just get a byte directly, if well placed (getc)
	{
		if (idat_handl->index_o == idat_handl->end_pos)	//oho problemo
			return 0;

		if (idat_handl->index_i == 0)
		{
			*res = idat_handl->buffer[idat_handl->index_o];
			idat_handl->index_o++;

			if (idat_handl->index_o < idat_handl->end_pos)
				return 1;
			else
				return updateIdatBuffer(idat_handl);
		}
		else
			return get8bitcharIdat(idat_handl, res);
	}

	char			getNbitCharIdat(t_idat_handler *idat_handl, singlet *res, u8 n)
	{
		u8	tmp;
		u8	dst;
		u8	tmp_n;

		tmp = 1;
		*res = 0;
		tmp_n = n;

		while (tmp_n > 0)
		{
			if (!getBoolFromBit(idat_handl, &dst))
				return 0;
			if (dst)
				*res += tmp;
			tmp = tmp << 1;
			tmp_n--;
		}

		return 1;
	}

	char			getFromStrongToLow5bitCharIdat(t_idat_handler *idat_handl, u8 *res)
	{
		u8	tmp;
		u8	rem;
		u8	dst;

		tmp = 0b00010000;
		*res = 0;
		rem = 5;
		while (rem > 0)
		{
			if (!getBoolFromBit(idat_handl, &dst))
				return 0;
			if (dst)
				*res += tmp;
			tmp = tmp >> 1;
			rem--;
		}

		return 1;
	}


	char			getBoolFromBit(t_idat_handler *idat_handl, u8 *res)	//read from least significance, to highest significance
	{																	//maybe its supposed to be the opposite, i'm not sure sure
		if (idat_handl->index_o == idat_handl->end_pos)	//oho problemo
			return 0;

		//*res = idat_handl->current_mask & idat_handl->buffer[idat_handl->index_o];
		*res = mask_array[idat_handl->index_i] & idat_handl->buffer[idat_handl->index_o];
		idat_handl->index_i++;

		if (idat_handl->index_i == 8)
			return (bytePassage(idat_handl));
		else
		{
			//idat_handl->current_mask = idat_handl->current_mask << 1;
			return 1;
		}
	}


	char			skipToNextByte(t_idat_handler *idat_handl)			//will move to next full byte, if already at the start of a clean byte, do nothing
	{
		if (idat_handl->index_i == 0)
			return 1;
		else
			return bytePassage(idat_handl);
	}

	char			bytePassage(t_idat_handler *idat_handl)	//go to next byte directly with no test and handle the mask reset and all
	{
		idat_handl->index_i = 0;
		idat_handl->index_o++;
		//idat_handl->current_mask = 1;
		if (idat_handl->index_o >= idat_handl->end_pos)
			return updateIdatBuffer(idat_handl);
		else
			return 1;
	}

	//only use if byte is set to an exact position
	char			copyFromIdat(t_idat_handler *idat_handle, singlet *target, u64 size)	//hope it copies it in the good order ? else i'll have to reverse
	{
		u64	rem_size;

		if (idat_handle->index_i != 0)
			return 0;
		else
		{
			rem_size = size;
			while (rem_size > 0)
			{
				if ((idat_handle->index_o + rem_size) < idat_handle->end_pos)
				{
					memcpy(target + (size - rem_size), idat_handle->buffer + idat_handle->index_o, static_cast<size_t>(rem_size));
					idat_handle->index_o += static_cast<u32>(rem_size);
					rem_size = 0;
				}
				else
				{
					memcpy(target + (size - rem_size), idat_handle->buffer + idat_handle->index_o, idat_handle->end_pos - idat_handle->index_o);
					rem_size -= idat_handle->end_pos - idat_handle->index_o;
					idat_handle->index_o = idat_handle->end_pos;
					if (!updateIdatBuffer(idat_handle))	//if arleady ended, will explose
						return 0;
				}
			}

			return 1;
		}
	}

	char			updateIdatBuffer(t_idat_handler *idat_handl)
	{
		u32	to_cop;

		if (idat_handl->ended)
			return 0;

		idat_handl->end_pos = 0;
		idat_handl->index_o = 0;

		to_cop = IDAT_BUFF_SIZE;

		while (to_cop > 0 && !idat_handl->ended)	//i dont fill up the buffer if its actually done
		{
			if (idat_handl->idat_rem_data > to_cop)
			{
				if (!copyFromBuffer(idat_handl->file_buffer, idat_handl->buffer + idat_handl->end_pos, to_cop))
					return 0;
				idat_handl->end_pos += to_cop;
				idat_handl->idat_rem_data -= to_cop;
				to_cop = 0;
			}
			else
			{
				if (!copyFromBuffer(idat_handl->file_buffer, idat_handl->buffer + idat_handl->end_pos, idat_handl->idat_rem_data))
					return 0;
				idat_handl->end_pos += static_cast<u32>(idat_handl->idat_rem_data);
				to_cop -= static_cast<u32>(idat_handl->idat_rem_data);
				idat_handl->idat_rem_data = 0;
				if (!endIdatBlockReached(idat_handl))	//will return 0 if i already set it to ended, and keep going, meaning
					return 0;
			}
		}

		return 1;
	}

	char			endIdatBlockReached(t_idat_handler *idat_handl)
	{
		if (idat_handl->ended)
			return 0;
		else
		{
			if (!jumpBuffer(idat_handl->file_buffer, 4)) //i jump the crc
				return (0);

			if (getChunkFromTableBuffer(idat_handl->p_chunk, idat_handl->file_buffer) != IDAT_CHUNK)
				idat_handl->ended = true;
			else
				idat_handl->idat_rem_data = idat_handl->p_chunk->length;

			return 1;
		}
	}

}