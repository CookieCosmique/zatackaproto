#pragma once

#include "Types.h"

namespace PNG
{
	typedef u64					octetet;	//8 octets
	typedef u32					quadet;		//4 octets
	typedef	u8					singlet;	//1 octet
	typedef singlet 			quadet_t[4];
}