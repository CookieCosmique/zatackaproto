#pragma once

#define IDAT_BUFF_SIZE 0x00008000 //64k + 7 // ??????
#define IDAT_BUFF_SIZE_REAL 0x00010000 //64k

#include "png_type.h"
#include "buffer.h"

namespace PNG
{
	struct s_unzib_stream;

	//idat should tell when done, specified by the IEND terminal from buffer, should give a way to work on the zlib data chunk itself for decompression
	//will have like get 1bit of information, or move by trees, or octets of data at a time, in normal order, or MSB, for the decompression, + have access
	//to an backtrack feature; <- thats in the decompressed handler that i need that

	//fill up buffer when needed, then when at the end of the buffer, just say boobye

	typedef struct	s_idat_handler
	{
		u32				index_o;		//the current index for the buffer	//index of the octet in the buffer
		u8				index_i;											//index inside an octet, bit pos
		singlet			buffer[IDAT_BUFF_SIZE];
		//u8				current_mask;	//is 000000 with a 1 at index_i value	//weird array stuff is faster

		u32				end_pos;		//will be idat_buff_size always, except when we spotted the end, in wich case, ended is set to true,
										//and enpos is then after the last readable char, if index_o == endpos, then we reach eof
		bool			ended;

		singlet			zlib_comp;	//CMF
		singlet			flag;		//FLG
		quadet			CRC;		//only avalaible at the end

		u8				CM;		//bit 0 to 3 of zlib_comp
		u8				CINFO;	//bit 4 to 7 of zlib_comp; the base 2 log of LZ77 window - 8 (CINFO = 7 -> size is 2^15 = 32k)

		u8				FCHECK; //bits 0 to 4 of flag; check bits	//CMF*256 + FLG should be = k * 31, fcheck is here to verify that
		u8				FDICT;	//bits 5;	preset dictionary	-- there is a 4octets DICTID if this is true - uint
		u8				FLEVEL;	//bits 6 to 7;	compression level 0 - fastest algorithm / 1 - fast algorithm / 2 - default algorithm / 3 - maximum compression, slowest//not needed for decompression
		quadet			DICTID;	//weird stuff, 4byte dict id ?

		//buffer update stuff, aka managing the idat block infos
		u64				idat_rem_data;
		t_partial_chunk *p_chunk;
		t_buffer		*file_buffer;
	}				t_idat_handler;

	extern u8		mask_array[8];

	char			newIdatHandler(t_idat_handler *idat_handl, t_buffer *buffer, t_partial_chunk *p_chunk);	//will get the start of the first idat block and process their information

	char			skipToNextByte(t_idat_handler *idat_handl);				//will move to next full byte, if already at the start of a clean byte, do nothing
	char			bytePassage(t_idat_handler *idat_handl);

	char			get1bitcharIdat(t_idat_handler *idat_handl, u8 *res);	//either get a char (8bits), from putting it to 0000000X
	char			get2bitcharIdat(t_idat_handler *idat_handl, u8 *res);	//get 1 bit * 0001 then + 1bit * 00010
	char			get3bitcharIdat(t_idat_handler *idat_handl, u8 *res);	//0001 + 0010 + 0100
	char			get4bitcharIdat(t_idat_handler *idat_handl, u8 *res);	//0001 + 0010 + 0100 + 1000
	char			get5bitcharIdat(t_idat_handler *idat_handl, u8 *res);	//0001 + 0010 + 0100 + 1000 + 10000
	char			get6bitcharIdat(t_idat_handler *idat_handl, u8 *res);	//0001 + 0010 + 0100 + 1000 + 10000 + ...
	char			get7bitcharIdat(t_idat_handler *idat_handl, u8 *res);	//0001 + 0010 + 0100 + 1000 + 10000 + ...
	char			get8bitcharIdat(t_idat_handler *idat_handl, u8 *res);	//0001 + 0010 + 0100 + 1000 + 10000 + ...
	char			getNbitCharIdat(t_idat_handler *idat_handl, u8 *res, u8 n);	//all get ... bit function call this one with n as argument

	char			getFromStrongToLow5bitCharIdat(t_idat_handler *idat_handl, u8 *res);

	char			getAByteIdat(t_idat_handler *idat_handl, singlet *res);	//same as up here, or just get a byte directly, if well placed (from buffer)


	char			getBoolFromBit(t_idat_handler *idat_handl, u8 *res);	//en gros, juste effectue un & avec le mask 0x0..010..0 correspond au bit actuel de lecture,
																			//renvoie soit un 000000, soit un 0001000 qqpart avec le 1 a l'endroit du bit actuel dans l'octet

	inline u8		checkNextBitBool(t_idat_handler *idat_handl)
	{
		//const u8	res = (idat_handl->current_mask & idat_handl->buffer[idat_handl->index_o]);
		const u8	res = (mask_array[idat_handl->index_i] & idat_handl->buffer[idat_handl->index_o]);
		idat_handl->index_i++;

		//if (idat_handl->index_i == 8)
		if (idat_handl->index_i & 0x08)
			bytePassage(idat_handl);
		/*else
			idat_handl->current_mask = idat_handl->current_mask << 1;*/

		return res;
	}

	//only used if byte is set to an exact position
	char			copyFromIdat(t_idat_handler *idat_handl, singlet *target, u64 size);	//hope it copies it in the good order ? else i'll have to reverse
	char			updateIdatBuffer(t_idat_handler *idat_handl);
	char			endIdatBlockReached(t_idat_handler *idat_handl);

	inline bool	is3BytesFree(t_idat_handler *idat_handl)
	{
		if ((idat_handl->index_o) + 3 < idat_handl->end_pos)
			return true;
		else
			return false;
	}

	inline void	manageByteBit(const u8 n_bit, const u8 off_byte, t_idat_handler *handl)
	{
		/*std::cout << "n_bit : " << (int) n_bit << std::endl;
		std::cout << "off_byte : " << (int) off_byte << std::endl;*/

		/*handl->index_i = n_bit;
		handl->index_o += off_byte;	//its fine cuz why ? cuz i checked before if im gud*/
		/*handl->index_i += n_bit;
		handl->index_o += off_byte;
		if (handl->index_i >= 8)
		{
			handl->index_i -= 8;
			handl->index_o ++;
		}*/
		handl->index_i += n_bit;
		handl->index_o += off_byte + (handl->index_i >> 3);
		handl->index_i &= 0x07;
	}

	inline void	manageAddBit(const u8 n_bit, t_idat_handler *idat_handl)
	{
		idat_handl->index_i += n_bit;
		idat_handl->index_o += (idat_handl->index_i >> 3);
		idat_handl->index_i &= 0x07;
	}
	//if idat is already set to ended, and ask for an update, will end it right now

}