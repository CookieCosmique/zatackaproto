#pragma once
//a bit higher like 0004 is a bit faster but not so much (1% ?) isnt worth the cost of memory
# define BUFFER_SIZE 0x00010000 //65k + 7
# define BUFFER_SIZE_REAL 0x0000FFF9 //65k
# define FIRST_BUFFER_SIZE 0x21 //33, size of png code plus IHDR

# include "png_type.h"
# include "chunk.h"
# include <cstdio>

namespace PNG
{
		typedef struct	s_buffer
		{
			singlet			buff[BUFFER_SIZE];
			u32				index;		//the current index for the tab
			u64				move; 		//last operation mouvement
			u32				end_pos;
			FILE			*file;
			//							//i actually didnt used a end condition ???
		}				t_buffer;

		char	getFirstBuffer(singlet *buffer, FILE * file);
		char	getIHDRFromBuffer(t_IHDR_chunk *ihdr, const singlet *buffer);

		char	newBuffer(t_buffer *buffer, FILE *file);
		char	getQuadBuffer(t_buffer *buffer, quadet **res);		//direct pointer to the real value
		char	getRevQuadBuffer(t_buffer *buffer, quadet *res);	//copy a value in here with reverse quad
		char	getSingleBuffer(t_buffer *buffer, singlet **res);
		char	getSingleCopBuffer(t_buffer *buffer, singlet *res);
		char	jumpBuffer(t_buffer *buffer, u64 jump);
		char	copyFromBuffer(t_buffer *buffer, singlet *dst, u64 size);

		quadet			reverseOctetOrder(const quadet *ref);

		void	updateBuffer(t_buffer *buffer);
		void	updateBufferAndSetMove(t_buffer *buffer, u64 move);
		void	advanceBuffer(t_buffer *buffer);

}