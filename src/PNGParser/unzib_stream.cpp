#include "../ProjectDefines.h"

#include "unzib_stream.h"

//#include <iostream>
//#include <sstream>
#include <string.h>

namespace PNG
{
	void(*filterType[5]) (singlet *, singlet *, const u8, const u32, t_unzib_stream *) = { &UnFilterType0, &UnFilterType1, &UnFilterType2, &UnFilterType3, &UnFilterType4 };

	const u8	table_length_val[29] =		{	0, 1, 2, 3, 4, 5, 6, 7,	//0 bit,//+1	//8
												8, 10, 12, 14,			//1 bit //+2	//4
												16, 20, 24, 28,			//2 bit //+4	//4
												32, 40, 48, 56,			//3 bit //+8	//4
												64, 80, 96, 112,		//4 bit	//+16	//4
												128, 160, 192, 224,		//5 bit	//+32	//4
												255 };					//0 bit	//---	//1	//29 value

	const u8	table_length_bitcount[29] = {	0, 0, 0, 0, 0, 0, 0, 0,
												1, 1, 1, 1,
												2, 2, 2, 2,
												3, 3, 3, 3,
												4, 4, 4, 4,
												5, 5, 5, 5,
												0 };

	const u16	table_dist_val[30] =		{	1, 2, 3, 4,
												5, 7,		9, 13,		//1 bit / 2 bit
												17, 25,		33, 49,		//3 bit / 4 bit
												65, 97,		129, 193,	//5 bit / 6 bit
												257, 385,	513, 769,	//7 bit / 8	bit
												1025, 1537, 2049, 3073,	//9 bit	/ 10bit
												4097, 6145, 8193, 12289,//11bit / 12bit
												16385, 24577 };			//13 bit/ ----	//30 value

	const u8	table_dist_bitcount[30] =	{	0, 0, 0, 0,
												1, 1,	2, 2,
												3, 3,	4, 4,
												5, 5,	6, 6,
												7, 7,	8, 8,
												9, 9,	10, 10,
												11, 11,	12, 12,
												13, 13 };

	const u8	order_of_tree_code_length[19] = { 16, 17, 18, 0, 8, 7, 9, 6, 10, 5, 11, 4, 12, 3, 13, 2, 14, 1, 15 };

	char	newUnzibStream(t_unzib_stream *unzib, t_idat_handler *idatHandler)
	{
		unzib->idatHandler = idatHandler;

		unzib->w_currentBuffer = 0;
		unzib->r_currentBuffer = 0;

		unzib->r_index = 0;

		unzib->end_pos = 0;
		unzib->ended = false;

		unzib->my_tree = false;
		unzib->curr_lit_len_tree = nullptr;
		unzib->curr_dist_tree = nullptr;
		unzib->default_tree = nullptr;
		unzib->b_type = B_TYPE_STARTING;

		newBlockUnzib(unzib);
		updateUnzibBuffer(unzib);

		return 0;
	}

	char	getUnzibScanline(singlet *scanline_target, singlet *scanline_before, const u8 pixel_size, const u32 scanline_width, t_unzib_stream *unzib)
	{
		//if the pass is empty, i shouldnt be called
		singlet	filter_type;

		if (!getUnzibSinglet(&filter_type, unzib))		//1byte filter type
			return 0;

		if (!copyUnzibToTarg(scanline_target, pixel_size * scanline_width, unzib))
			return 0;

		/*switch (filter_type)	//on release, same efficience
		{
		case 0 :
			UnFilterType0(scanline_target, scanline_before, pixel_size, scanline_width, unzib);
			break;
		case 1:
			UnFilterType1(scanline_target, scanline_before, pixel_size, scanline_width, unzib);
			break;
		case 2:
			UnFilterType2(scanline_target, scanline_before, pixel_size, scanline_width, unzib);
			break;
		case 3:
			UnFilterType3(scanline_target, scanline_before, pixel_size, scanline_width, unzib);
			break;
		case 4:
			UnFilterType4(scanline_target, scanline_before, pixel_size, scanline_width, unzib);
			break;
		}*/

		filterType[filter_type](scanline_target, scanline_before, pixel_size, scanline_width, unzib);

		return 1;
	}

	char	copyUnzibToTarg(singlet *targ, const u32 copSize, t_unzib_stream *unzib)
	{
		u32	tmp;
		//there was something here
		tmp = copSize;
		while ((unzib->w_currentBuffer == unzib->r_currentBuffer && tmp + unzib->r_index > unzib->end_pos) ||
				(unzib->w_currentBuffer != unzib->r_currentBuffer && tmp + unzib->r_index > UNZIB_BUF_SIZE))
		{
			if (unzib->w_currentBuffer != unzib->r_currentBuffer) //copy all remaining buffer, and if r and write at same place, update buffer
			{
				memcpy(targ + (copSize - tmp), unzib->buffer[unzib->r_currentBuffer] + unzib->r_index, UNZIB_BUF_SIZE - unzib->r_index);
				tmp -= (UNZIB_BUF_SIZE - unzib->r_index);
				unzib->r_index = 0;
				unzib->r_currentBuffer = unzib->w_currentBuffer;
				if (!unzib->ended)
				{
					if (!updateUnzibBuffer(unzib))
						return 0;
				}
			}
			else if (!unzib->ended)			//update the buffer madafacke, and try again
			{
				if (!updateUnzibBuffer(unzib))
					return 0;
			}
			else							//oh well did my best copy all that remain
			{
				memcpy(targ + (copSize - tmp), unzib->buffer[unzib->r_currentBuffer] + unzib->r_index, unzib->end_pos - unzib->r_index);
				//tmp -= (unzib->end_pos - unzib->r_index);
				tmp = 0;//cant copy more stuff anyway
				unzib->r_index = unzib->end_pos;
				return 0;
			}

		}

		if (tmp > 0)
			memcpy(targ + (copSize - tmp), unzib->buffer[unzib->r_currentBuffer] + unzib->r_index, tmp);

		unzib->r_index += tmp;

		return 1;
	}

	char	getUnzibSinglet(singlet *targ, t_unzib_stream *unzib)
	{
		*targ = (unzib->buffer[unzib->r_currentBuffer])[unzib->r_index];

		unzib->r_index++;
		if (((unzib->w_currentBuffer == unzib->r_currentBuffer && unzib->r_index > unzib->end_pos) ||
			(unzib->w_currentBuffer != unzib->r_currentBuffer && unzib->r_index > UNZIB_BUF_SIZE)) && !unzib->ended)
		{
				if (!updateUnzibBuffer(unzib))
					return 0;
		}

		return 1;
		//return (copyUnzibToTarg(targ, 1, unzib));
	}

	inline	u8	paeth_predicator(const u8 a, const u8 b, const u8 c) //a = left, b = above, c = upper left
	{
		const i16	p = i16(a + b) - c;
		i16	pa;
		i16	pb;
		i16	pc;
		i16 mask;


		pa = p - i16(a);
		pb = p - i16(b);
		pc = p - i16(c);

		mask = pa >> 0xf;
		pa = (pa + mask) ^ mask;
		mask = pb >> 0xf;
		pb = (pb + mask) ^ mask;
		mask = pc >> 0xf;
		pc = (pc + mask) ^ mask;

		/*pa = (pa > 0) ? pa : -pa;	//other one seems atas bit faster ?
		pb = (pb > 0) ? pb : -pb;
		pc = (pc > 0) ? pc : -pc;*/
		//tie breaking does so in this order, a, b then c
		if (pa <= pb && pa <= pc)
			return a;
		else if (pb <= pc)
			return b;
		else
			return c;
	}
																							//had to add some, because i and bpp are unsigned, therefore i cant test if they are negative
#	define	__BPP pixel_size																//bpp, aka pixel size, usualy how much we move in filtering
#	define	__X i																			//current byte index in scanline
#	define	__DEC(__x, __bpp) ((__x < __bpp) ? 0 : (scan_target[__x - __bpp]))				//to use only when X design a decoded byte		//to better understand code, x-bpp stuff
#	define	__DEC_SURE(__x, __bpp) (scan_target[__x - __bpp])								//to use only when X design a decoded byte		//to better understand code, x-bpp stuff
#	define	__UND(__x) (scan_target[__x])											//to use only when X design a not decoded byte and X>0	//to better understand the code
#	define	__PRIOR(__x) ((__x < 0) ? 0 : ((scan_buffer == nullptr) ? 0 : (scan_buffer[__x])))							//the up scanline, already decoded, aka unfiltered
#	define	__PRIOR_SURE(__x) (scan_buffer[__x])									//the up scanline, already decoded, aka unfiltered
#	define	__PRIOR_BEF(__x, __bpp) ((__x < __bpp) ? 0 : ((scan_buffer == nullptr) ? 0 : (scan_buffer[__x - __bpp])))	//the up left scanline
#	define	__PRIOR_BEF_SURE(__x, __bpp) (scan_buffer[__x - __bpp])	//the up left scanline
#	define	__PAETH_PRED(__a, __b, __c) paeth_predicator(__a, __b, __c) //a = left, b = above, c = upper left

	void	UnFilterType0(singlet *, singlet *, const u8 , const u32 , t_unzib_stream *)
	{
		//return (copyUnzibToTarg(scan_target, pixel_size * scan_width, unz));
	}

	void	UnFilterType1(singlet *scan_target, singlet *, const u8 pixel_size, const u32 scan_width, t_unzib_stream *)	//the sub filter
	{
		const u32 max_ = pixel_size * scan_width;
		for (u32 i = 0; i < max_; i++)
			scan_target[i] = __UND(__X) + __DEC(__X, __BPP);
	}

	void	UnFilterType2(singlet *scan_target, singlet *scan_buffer, const u8 pixel_size, const u32 scan_width, t_unzib_stream *)	//the up filter
	{
		const u32 max_ = pixel_size * scan_width;
		for (u32 i = 0; i < max_; i++)
			scan_target[i] = __UND(__X) + __PRIOR(__X);
	}

	void	UnFilterType3(singlet *scan_target, singlet *scan_buffer, const u8 pixel_size, const u32 scan_width, t_unzib_stream *)	//the up filter
	{
		const u32 max_ = pixel_size * scan_width;
		for (u32 i = 0; i < max_; i++)
			scan_target[i] = __UND(__X) + ((__DEC(__X, __BPP) + __PRIOR(__X)) / 2);
	}

	void	UnFilterFirstType4(singlet *scan_target, singlet *, const u8 pixel_size, const u32 scan_width, t_unzib_stream *)	//the PAETH filter
	{
		const u32 max_ = pixel_size * scan_width;
		for (u32 i = 0; i < __BPP; i++)
			scan_target[i] = __UND(__X) + __PAETH_PRED(0, 0, 0);
		for (u32 i = __BPP; i < max_; i++)
			scan_target[i] = __UND(__X) + __PAETH_PRED(__DEC_SURE(__X, __BPP), 0, 0);
	}

	void	UnFilterAfterType4(singlet *scan_target, singlet *scan_buffer, const u8 pixel_size, const u32 scan_width, t_unzib_stream *)	//the PAETH filter
	{
		const u32 max_ = pixel_size * scan_width;
		for (u32 i = 0; i < __BPP; i++)
			scan_target[i] = __UND(__X) + __PAETH_PRED(0, __PRIOR_SURE(__X), 0);
		for (u32 i = __BPP; i < max_; i++)
				scan_target[i] = __UND(__X) + __PAETH_PRED(__DEC_SURE(__X, __BPP), __PRIOR_SURE(__X), __PRIOR_BEF_SURE(__X, __BPP));
	}

	void	UnFilterType4(singlet *scan_target, singlet *scan_buffer, const u8 pixel_size, const u32 scan_width, t_unzib_stream *unz)
	{
		if (scan_buffer == nullptr)
			UnFilterFirstType4(scan_target, scan_buffer, pixel_size, scan_width, unz);
		else
			UnFilterAfterType4(scan_target, scan_buffer, pixel_size, scan_width, unz);
	}

	/*void	UnFilterFirstType4(singlet *scan_target, singlet *scan_buffer, const u8 pixel_size, const u32 scan_width, t_unzib_stream *unz)	//the PAETH filter
	{
		const u32 max_ = pixel_size * scan_width;
		debug_filter_4 += max_;
		for (u32 i = 0; i < max_; i++)
			scan_target[i] = __UND(__X) + __PAETH_PRED(__DEC(__X, __BPP), __PRIOR(__X), __PRIOR_BEF(__X, __BPP));
	}*/

//////////////////////////////////////
//--Unzib decompression
//////////////////////////////////////

	char	updateUnzibBuffer(t_unzib_stream *unz)	//ici que les choses compliqués se font
	{
		u16 tmp;
		u8	writing_buffer;

		writing_buffer = unz->w_currentBuffer;
		while (!unz->ended && writing_buffer == unz->w_currentBuffer)	//on s'arrete a la fin du stream de data, ou quand on a swap les buffers
		{
			unz->keep_copy = true;	//on end or buffer swap, will be set to false
			if (unz->b_type == B_TYPE_NO_COMPRESSION)
			{
				while (unz->keep_copy)
					applyRuleUncompressed(unz);
			}
			else
			{
				while (unz->keep_copy)
				{
					if (is3BytesFree(unz->idatHandler))
						getNextAccel_GEN(unz->faster_lit_len[0], &tmp, unz->idatHandler);
					else
						getNextHuffSymbol(unz->curr_lit_len_tree, unz, &tmp);

					applyTreeRule(tmp, unz);
				}
			}
		}

		//will fill a buffer completely, and then start the next one a bit if i go to far (like with a copy dist len stuff)

		return 1;
	}

	char	newBlockUnzib(t_unzib_stream *unz)
	{
		unz->keep_copy = false;

		if (!get1bitcharIdat(unz->idatHandler, &unz->b_final))
			return 0;
		if (!get2bitcharIdat(unz->idatHandler, &unz->b_type))
			return 0;

		if (unz->b_type == B_TYPE_ERROR)
			return 0;
		else if (unz->b_type == B_TYPE_NO_COMPRESSION)
			return preTreatUncompressed(unz);
		else if (unz->b_type == B_TYPE_DYNAMIC_HUFFMAN)
			return generateHuffmanTree(unz);			//generate both huffman trees, will be deleted at EOB
		else										//b_type : static huffman
			return generateDefaultTree(unz);			//if nullptr, create it, else just reuse it, only deleted at end

		//btype : 00 -> no compression, 01 -> fixed huffman code, 10 -> dynamic huffman, 11 -> reserved (error)
	}

	void	reachEOB(t_unzib_stream *unz)
	{
		if (unz->b_type == B_TYPE_DYNAMIC_HUFFMAN)
		{
			MY_DELETE_ARRAY(unz->curr_lit_len_tree->symbol_array);
			MY_DELETE_ARRAY(unz->curr_lit_len_tree->code_length);
			MY_DELETE_ARRAY(unz->curr_dist_tree->symbol_array);
			//curr dist tree code length is the same as the other one code lenght
			MY_DELETE(unz->curr_lit_len_tree);
			MY_DELETE(unz->curr_dist_tree);
		}

		if (unz->b_final)
		{
			//should get the crc or something here i think
			unz->ended = true;
			unz->keep_copy = false;
			if (unz->default_tree)
			{
				deleteHuffTree(unz->default_tree);
				MY_DELETE(unz->default_tree);
				unz->default_tree = nullptr;
			}
		}
		else
			newBlockUnzib(unz);
	}

	void	applyRuleUncompressed(t_unzib_stream *unz)
	{
		if ((UNZIB_BUF_SIZE - unz->end_pos) < unz->curr_LEN)
		{
			copyFromIdat(unz->idatHandler, unz->buffer[unz->w_currentBuffer] + unz->end_pos, UNZIB_BUF_SIZE - unz->end_pos);

			unz->curr_LEN -= (UNZIB_BUF_SIZE - unz->end_pos);
			unz->end_pos = 0;
			unz->w_currentBuffer = 1 - unz->w_currentBuffer;
			unz->keep_copy = false;;
		}
		else
		{
			copyFromIdat(unz->idatHandler, unz->buffer[unz->w_currentBuffer] + unz->end_pos, unz->curr_LEN);

			unz->end_pos += unz->curr_LEN;
			unz->curr_LEN = 0;

			reachEOB(unz);
		}
	}

	inline void	applyTreeRule(const u16 tmp, t_unzib_stream *unz)
	{
		if (tmp < 0x0100)
			copySingletToUnzlib(tmp & 0x00FF, unz);
		else if (tmp == 0x0100)	//reach end of block
			reachEOB(unz);
		else if (tmp <= 0x011D)	//287 - weird copy stuff, decode and mooooove	//removing it doesnt increase perf
			decodeAndMove(tmp, unz);
	}

	char	preTreatUncompressed(t_unzib_stream *unz)
	{
		singlet	tmp_byte;

		if (!skipToNextByte(unz->idatHandler))
			return 0;

		if (!getAByteIdat(unz->idatHandler, &tmp_byte))
			return 0;
		unz->LEN = tmp_byte;
		if (!getAByteIdat(unz->idatHandler, &tmp_byte))
			return 0;
		unz->LEN += tmp_byte << 8;
		if (!getAByteIdat(unz->idatHandler, &tmp_byte))
			return 0;
		unz->NLEN = tmp_byte;
		if (!getAByteIdat(unz->idatHandler, &tmp_byte))
			return 0;
		unz->NLEN += tmp_byte << 8;

		unz->curr_LEN = unz->LEN;

		return 1;
	}

	void	subFonctionMoveLength(u16 &length, const u16 code, t_unzib_stream *unz)
	{
		u8	length_add_bit;
		length = table_length_val[code - 257] + 3;
		length_add_bit = table_length_bitcount[code - 257];

		if (is3BytesFree(unz->idatHandler))	//only max 5bits extra length
		{
			length += (0xFF >> (8 - length_add_bit)) &
				((unz->idatHandler->buffer[unz->idatHandler->index_o] >> unz->idatHandler->index_i) |
					unz->idatHandler->buffer[unz->idatHandler->index_o + 1] << (8 - unz->idatHandler->index_i));
			manageAddBit(length_add_bit, unz->idatHandler);
		}
		else
		{
			u16 tmp_counter = 1;
			while (length_add_bit > 0)
			{
				if (checkNextBitBool(unz->idatHandler))
					length += tmp_counter;
				length_add_bit--;
				tmp_counter = tmp_counter << 1;  //1, 2, 4, ....,
			}
		}
	}

	void	subFonctionMoveDist(u16 &dist, const u16 dist_code, t_unzib_stream *unz)
	{
		dist = table_dist_val[dist_code];
		u8 dist_add_bit = table_dist_bitcount[dist_code];

		if (is3BytesFree(unz->idatHandler))	//only max 5bits extra length
		{
			dist += (0xFFFF >> (16 - dist_add_bit)) &
				((unz->idatHandler->buffer[unz->idatHandler->index_o] >> unz->idatHandler->index_i) |
					unz->idatHandler->buffer[unz->idatHandler->index_o + 1] << (8 - unz->idatHandler->index_i) |
					unz->idatHandler->buffer[unz->idatHandler->index_o + 2] << (16 - unz->idatHandler->index_i));

			manageAddBit(dist_add_bit, unz->idatHandler);
		}
		else
		{
			u16 tmp_counter = 1;

			while (dist_add_bit > 0)
			{
				if (checkNextBitBool(unz->idatHandler))
					dist += tmp_counter;
				dist_add_bit--;
				tmp_counter = tmp_counter << 1;	//1, 2, 4, ....,
			}
		}
	}

	void	decodeAndMove(u16 code, t_unzib_stream *unz)
	{
		u16	length;
		//u8	length_add_bit;
		u16	dist;
		//u8	dist_add_bit;

		u8	dist_code;	//u16 cuz i get it with "next huff symbol"
		u16	tmp_dist_code;

		//u8	tmp_res;

		/*length = table_length_val[code - 257] + 3;
		length_add_bit = table_length_bitcount[code - 257];

		tmp_counter = 1;
		if (is3BytesFree(unz->idatHandler))	//only max 5bits extra length
		{
			length += (0xFF >> (8 - length_add_bit)) &
				((unz->idatHandler->buffer[unz->idatHandler->index_o] >> unz->idatHandler->index_i) |
				unz->idatHandler->buffer[unz->idatHandler->index_o + 1] << (8 - unz->idatHandler->index_i));
			manageAddBit(length_add_bit, unz->idatHandler);
		}
		else
		{
			while (length_add_bit > 0)
			{
				if (checkNextBitBool(unz->idatHandler))
				length += tmp_counter;
				length_add_bit--;
				tmp_counter = tmp_counter << 1;  //1, 2, 4, ....,
			}
		}*/
		subFonctionMoveLength(length, code, unz);

		if (unz->b_type & B_TYPE_DYNAMIC_HUFFMAN)
		{
			getNextHuffSymbol(unz->curr_dist_tree, unz, &tmp_dist_code);
			dist_code = static_cast<u8>(tmp_dist_code);
		}
		else
			getFromStrongToLow5bitCharIdat(unz->idatHandler, &dist_code);

		/*dist = table_dist_val[dist_code];
		dist_add_bit = table_dist_bitcount[dist_code];

		if (is3BytesFree(unz->idatHandler))	//only max 5bits extra length
		{
			dist += (0xFFFF >> (16 - dist_add_bit)) &
				((unz->idatHandler->buffer[unz->idatHandler->index_o] >> unz->idatHandler->index_i) |
					unz->idatHandler->buffer[unz->idatHandler->index_o + 1] << (8 - unz->idatHandler->index_i) |
					unz->idatHandler->buffer[unz->idatHandler->index_o + 2] << (16 - unz->idatHandler->index_i));

			manageAddBit(dist_add_bit, unz->idatHandler);
		}
		else
		{
			u16 tmp_counter = 1;

			while (dist_add_bit > 0)
			{
				if (checkNextBitBool(unz->idatHandler))
					dist += tmp_counter;
				dist_add_bit--;
				tmp_counter = tmp_counter << 1;	//1, 2, 4, ....,
			}
		}*/

		subFonctionMoveDist(dist, dist_code, unz);

		copyDistLength(length, dist, unz);
	}

	/*void	copyDistLength(u16 length, u16 dist, t_unzib_stream *unz)
	{
		u16	tmp_copy_buff;	//u16 is enough, cuz i have a buffer size of 32k
		u16	tmp_copy_pos;
		u16	tmp_dist;
		u16	tmp_length;		//direct copy
		u16	add_length;		//overlap stuff, have to do it step by step since i read and write to same buffer

		tmp_dist = dist;

		if (length > dist)
		{
			tmp_length = dist;
			add_length = length - dist;
		}
		else
		{
			tmp_length = length;
			add_length = 0;
		}

		tmp_copy_buff = unz->w_currentBuffer;
		tmp_copy_pos = unz->end_pos;

		while (tmp_copy_pos < tmp_dist)
		{
			tmp_dist -= (tmp_copy_pos + 1);	//move to actualy unzib buff size -1 directly, so im not stuck on impossible position even temporarely
			tmp_copy_pos = UNZIB_BUF_SIZE - 1;
			tmp_copy_buff = 1 - tmp_copy_buff;
		}

		if (tmp_copy_pos >= tmp_dist)
		{
			tmp_copy_pos -= tmp_dist;
			tmp_dist = 0;
		}

		while (tmp_length > 0)
		{
			if (tmp_copy_pos + tmp_length < UNZIB_BUF_SIZE)
			{
				copyBlockToUnzlib(unz->buffer[tmp_copy_buff] + tmp_copy_pos, tmp_length, unz);
				tmp_copy_pos += tmp_length;
				tmp_length = 0;
			}
			else
			{
				copyBlockToUnzlib(unz->buffer[tmp_copy_buff] + tmp_copy_pos, UNZIB_BUF_SIZE - tmp_copy_pos, unz);
				tmp_length -= (UNZIB_BUF_SIZE - tmp_copy_pos);
				tmp_copy_pos = 0;
				tmp_copy_buff = 1 - tmp_copy_buff;
			}
		}

		while (add_length > 0)
		{
			copySingletToUnzlib(*(unz->buffer[tmp_copy_buff] + tmp_copy_pos), unz);	//copy symb to unz current write buffer, and eventually swap buffer and all that

			tmp_copy_pos++;
			if (tmp_copy_pos >= UNZIB_BUF_SIZE)
			{
				tmp_copy_pos = 0;
				tmp_copy_buff = 1 - tmp_copy_buff;
			}

			add_length--;
		}
	}*/

	void	copyDistLength(const u16 length, const u16 dist, t_unzib_stream * const unz)
	{
		u16	tmp_copy_buff;	//u16 is enough, cuz i have a buffer size of 32k
		u16	tmp_copy_pos;
		u16	tmp_length;		//direct copy
		u16	step;

		if (dist == 1)	//copy same val again and again, so use mem set
		{
			if (unz->end_pos == 0)
				setBlockToUnzlib(unz->buffer[1 - unz->w_currentBuffer][UNZIB_BUF_SIZE - 1], length, unz);
			else
				setBlockToUnzlib(unz->buffer[unz->w_currentBuffer][unz->end_pos - 1], length, unz);
		}
		else
		{
			tmp_length = length;

			tmp_copy_buff = unz->w_currentBuffer;
			tmp_copy_pos = unz->end_pos;

			step = dist;

			if (step > length)
				step = length;

			if (tmp_copy_pos >= dist)
				tmp_copy_pos -= dist;
			else
			{
				tmp_copy_pos += UNZIB_BUF_SIZE - dist;
				tmp_copy_buff = 1 - tmp_copy_buff;
			}

			while (tmp_length > 0)
			{
				if (tmp_length < step)
					step = tmp_length;

				if (tmp_copy_pos + step < UNZIB_BUF_SIZE)
				{
					copyBlockToUnzlib(unz->buffer[tmp_copy_buff] + tmp_copy_pos, step, unz);
					tmp_copy_pos += step;
					tmp_length -= step;
				}
				else
				{
					copyBlockToUnzlib(unz->buffer[tmp_copy_buff] + tmp_copy_pos, UNZIB_BUF_SIZE - tmp_copy_pos, unz);
					tmp_length -= (UNZIB_BUF_SIZE - tmp_copy_pos);
					tmp_copy_pos = 0;
					tmp_copy_buff = 1 - tmp_copy_buff;
				}
			}
		}
	}

	void	copySingletToUnzlib(singlet symb, t_unzib_stream *unz)	//copy symb to unz current write buffer, and eventually swap buffer and all that
	{
		//current end_pos is necessarely a valid index to blit a value, therefore i juste do that
		*(unz->buffer[unz->w_currentBuffer] + unz->end_pos) = symb;

		unz->end_pos++;

		if (unz->end_pos >= UNZIB_BUF_SIZE)
		{
			unz->end_pos = 0;
			unz->w_currentBuffer = 1 - unz->w_currentBuffer;
			unz->keep_copy = false;
		}
	}

	void	copyBlockToUnzlib(singlet *src, u16 size, t_unzib_stream *unz)	//copy block of symb to unz current write buffer, and eventually swap buffer and all that
	{
		u16	tmp_size;
		u16	tmp_src;

		tmp_size = size;
		tmp_src = 0;
		while (tmp_size > 0)
		{
			if ((unz->end_pos + tmp_size) < UNZIB_BUF_SIZE)
			{
				memcpy(unz->buffer[unz->w_currentBuffer] + unz->end_pos, src + tmp_src, tmp_size);
				unz->end_pos += tmp_size;
				tmp_src += tmp_size;
				tmp_size = 0;
			}
			else
			{
				memcpy(unz->buffer[unz->w_currentBuffer] + unz->end_pos, src + tmp_src, UNZIB_BUF_SIZE - unz->end_pos);

				tmp_size -= (UNZIB_BUF_SIZE - unz->end_pos);
				tmp_src += (UNZIB_BUF_SIZE - unz->end_pos);
				unz->end_pos = 0;
				unz->w_currentBuffer = 1 - unz->w_currentBuffer;
				unz->keep_copy = false;
			}
		}
	}

	void	setBlockToUnzlib(const singlet src, u16 size, t_unzib_stream *unz)	//copy block of symb to unz current write buffer, and eventually swap buffer and all that
	{
		u16	tmp_size;

		tmp_size = size;
		while (tmp_size > 0)
		{
			if ((unz->end_pos + tmp_size) < UNZIB_BUF_SIZE)
			{
				memset(unz->buffer[unz->w_currentBuffer] + unz->end_pos, src, tmp_size);
				unz->end_pos += tmp_size;
				tmp_size = 0;
			}
			else
			{
				memset(unz->buffer[unz->w_currentBuffer] + unz->end_pos, src, UNZIB_BUF_SIZE - unz->end_pos);

				tmp_size -= (UNZIB_BUF_SIZE - unz->end_pos);
				unz->end_pos = 0;
				unz->w_currentBuffer = 1 - unz->w_currentBuffer;
				unz->keep_copy = false;
			}
		}
	}

//---------------------------------
//-- Huff Tree
//---------------------------------

	char	generateHuffmanTree(t_unzib_stream *unz)
	{
		t_huffman_tree	tree_code_tree;

		u8	HLIT;	//5bit, lit_len alphabet size - 257 (257 - 286)
		u8	HDIST;	//5bit, dist code size -1			(1 - 32)
		u8	HCLEN;	//4bit,	code length code size - 4	(4 - 19)

		u16	tmp;

		//then (HCLEN + 4) * 3 bit of length for the code length
		//then HLIT + 257 symb to get with code length tree
		//then HDIST + 1 symb to get with code length tree
		//then actually the data

		unz->curr_lit_len_tree = MY_NEW t_huffman_tree();
		unz->curr_dist_tree = MY_NEW t_huffman_tree();

		if (!get5bitcharIdat(unz->idatHandler, &HLIT))
			return 0;
		if (!get5bitcharIdat(unz->idatHandler, &HDIST))
			return 0;
		if (!get4bitcharIdat(unz->idatHandler, &HCLEN))
			return 0;

		unz->curr_lit_len_tree->alphabet_size = HLIT + 257;
		unz->curr_dist_tree->alphabet_size = HDIST + 1;
		tree_code_tree.alphabet_size = 19;
		tree_code_tree.code_length = MY_NEW u8[19];
		for (u8 i = 0; i < 19; i++)
			tree_code_tree.code_length[i] = 0;
		tmp = 0;
		while (tmp < HCLEN + 4)
		{
			if (!get3bitcharIdat(unz->idatHandler, tree_code_tree.code_length + order_of_tree_code_length[tmp]))
				return 0;
			tmp++;
		}

		if (!generateTreeBody(&tree_code_tree))
			return 0;

		//actualy, it generate a biiiiig sequence of code lenght across both of em, so :

		u8	*across_length = MY_NEW u8[HLIT + HDIST + 258];

		if (!generateLengthArrayFromTreeCode(&tree_code_tree, across_length, HLIT + HDIST + 258, unz))
			return 0;

		unz->curr_lit_len_tree->code_length = across_length;
		unz->curr_dist_tree->code_length = across_length + HLIT + 257;	//start at the end of lit len part of 'across buffer'

		if (!generateTreeBody(unz->curr_lit_len_tree))
			return 0;
		if (!generateTreeBody(unz->curr_dist_tree))
			return 0;

		generateAccel(unz, unz->curr_lit_len_tree);

		return (deleteHuffTree(&tree_code_tree));
	}

	char	generateLengthArrayFromTreeCode(t_huffman_tree *tree_code, u8 *length_array, u16 array_size, t_unzib_stream *unz)
	{
		u16 tmp;
		u8	repeat_time;
		u8	repeat_val;
		u8	u8tmp;
		u16	dst;

		tmp = 0;
		while (tmp < array_size)
		{
			getNextHuffSymbol(tree_code, unz, &dst);

			if (dst <= 0x0F)
			{
				length_array[tmp] = static_cast<u8>(dst);
				tmp++;
			}
			else
			{
				if (dst == 0x10)
				{
					repeat_time = 3;
					if (tmp == 0)		//this shouldnt actually happen
						repeat_val = 0;
					else
						repeat_val = length_array[tmp - 1];

					if (!get2bitcharIdat(unz->idatHandler, &u8tmp))
						return 0;
					repeat_time += u8tmp;
				}
				else
				{
					repeat_val = 0;
					if (dst == 0x11)
					{
						repeat_time = 3;
						if (!get3bitcharIdat(unz->idatHandler, &u8tmp))
							return 0;
						repeat_time += u8tmp;
					}
					else if (dst == 0x12)
					{
						repeat_time = 11;
						if (!get7bitcharIdat(unz->idatHandler, &u8tmp))
							return 0;
						repeat_time += u8tmp;
					}
					else
						return 0;
				}

				while (repeat_time > 0)
				{
					length_array[tmp] = repeat_val;
					tmp++;
					repeat_time--;
				}
			}
		}

		return 1;
	}

	char	generateTreeBody(t_huffman_tree *tree)
	{
		u8	length_size[16] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};	//len 0 to 15, len 0 means actualy nothing
		u16	first_len[16] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };		//contain the first char of len [index] + 1 (0 means -1 etc ...)

		for (u16 i = 0; i < tree->alphabet_size; i++)
		{
			length_size[tree->code_length[i]] += 1;
			if (first_len[tree->code_length[i]] == 0)
				first_len[tree->code_length[i]] = i + 1;
		}

		length_size[0] = 0;	//the alphabet of length 0 shouldnt count, they are like not here, but need this 0 value for purposes

		return subGenerateTreeBody(tree, length_size, first_len);
	}

	char	subGenerateTreeBody(t_huffman_tree *tree, u8 *length_size, u16 *first_len)	//length size et first len are array of size 16
	{
		u16	rank_size[16] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

		u16	count;

		u16	tmp;
		i16	curr_char_fill;
		i16	curr_jump_fill;

				count = 1;
		for (u8 tmp = 1; tmp < 16; tmp++)
		{
			count = 2 * count;
			rank_size[tmp] = count + rank_size[tmp - 1];	//count the cumul of rank, wich is the index of the LAST case AFTER this rank
			count = count - length_size[tmp];																			//rank 1 goes from index rank[0] to rank[1] - 1
		}

		tree->symbol_array = MY_NEW i16[rank_size[15]];	//so size of everyithing is rank_size[15]

		for (u8 rank_ = 1; rank_ < 16; rank_++)
		{
			//fill up all rank rank_size[rank - 1] -> rank_size[rank] - 1
			//do all the char
			curr_char_fill = first_len[rank_] - 1 - 1;	//cuz i do ++ at start
			curr_jump_fill = rank_size[rank_];
			tmp = (rank_size[rank_ - 1] + length_size[rank_]);
			for (u16 i = rank_size[rank_ - 1]; i < tmp; i++)
			{
				curr_char_fill++;
				while (tree->code_length[curr_char_fill] != rank_)
					curr_char_fill++;
				//tree->symbol_array[i] = curr_char_fill;
				tree->symbol_array[i] = curr_char_fill - tree->alphabet_size;
			}
			for (u16 i = tmp; i < rank_size[rank_]; i++)
			{
				//tree->symbol_array[i] = curr_jump_fill + tree->alphabet_size;
				tree->symbol_array[i] = curr_jump_fill;
				curr_jump_fill+=2;
			}
		}

		return 1;
	}

#	define	DEF_ALPHABET_SIZE 288
#	define	__(__tmp) (__tmp)
#	define	_r(__tmp) (__tmp - DEF_ALPHABET_SIZE)

	char	generateDefaultTree(t_unzib_stream *unz)
	{
		if (unz->default_tree)
		{
			unz->curr_lit_len_tree = unz->default_tree;
			generateAccel(unz, unz->default_tree);
			return 1;
		}
		else
		{
			//std::cout << "huff tree default created" << std::endl;
			unz->default_tree = MY_NEW t_huffman_tree();
			unz->curr_lit_len_tree = unz->default_tree;
			unz->default_tree->alphabet_size = DEF_ALPHABET_SIZE;
			unz->default_tree->code_length = MY_NEW u8[288];
			//needed to generate accelerated huff, will impl myself a accel huff later maybe directly
			for (u16 i = 0; i <= 143; i++)
				unz->default_tree->code_length[i] = 8;
			for (u16 i = 144; i <= 255; i++)
				unz->default_tree->code_length[i] = 9;
			for (u16 i = 256; i <= 279; i++)
				unz->default_tree->code_length[i] = 7;
			for (u16 i = 280; i <= 287; i++)
				unz->default_tree->code_length[i] = 8;

			unz->default_tree->symbol_array = MY_NEW i16[574]{
/*	0	*/		__(2),                              __(4),																														//		//2		//2		- rank 1 size
/*	2	*/		__(6),              __(8),              __(10),             __(12),																								//		//4		//4		- rank 2 size
/*	6	*/		__(14),     __(16),     __(18),     __(20),     __(22),     __(24),     __(26),     __(28),																		//		//8		//8		- rank 3 size
/*	14	*/		__(30),__(32),__(34),__(36),__(38),__(40),__(42),__(44),__(46),__(48),__(50),__(52),__(54),__(56),__(58),__(60),												//		//16	//16	- rank 4 size
																																												//----------------------------
/*	30	*/		__(62), __(64), __(66), __(68), __(70), __(72), __(74), __(76), __(78), __(80), __(82), __(84), __(86), __(88), __(90), __(92),									//16
/*	46	*/		__(94), __(96), __(98), __(100),__(102),__(104),__(106),__(108),__(110),__(112),__(114),__(116),__(118),__(120),__(122),__(124),								//16	//32	//32	- rank 5 size
																																												//----------------------------
/*	62	*/		__(126),__(128),__(130),__(132),__(134),__(136),__(138),__(140),__(142),__(144),__(146),__(148),__(150),__(152),__(154),__(156),								//16
				__(158),__(160),__(162),__(164),__(166),__(168),__(170),__(172),__(174),__(176),__(178),__(180),__(182),__(184),__(186),__(188),								//16
/*	94	*/		__(190),__(192),__(194),__(196),__(198),__(200),__(202),__(204),__(206),__(208),__(210),__(212),__(214),__(216),__(218),__(220),								//16
				__(222),__(224),__(226),__(228),__(230),__(232),__(234),__(236),__(238),__(240),__(242),__(244),__(246),__(248),__(250),__(252),								//16	//64	//64	- rank 6 size
																																												//----------------------------
/*	126	*/		_r(256),_r(257),_r(258),_r(259),_r(260),_r(261),_r(262),_r(263),_r(264),_r(265),_r(266),_r(267),_r(268),_r(269),_r(270),_r(271),								//16 /r
				_r(272),_r(273),_r(274),_r(275),_r(276),_r(277),_r(278),_r(279),																								//8  /r	//24
/*	150	*/			__(254),__(256),__(258),__(260),__(262),__(264),__(266),__(268),__(270),__(272),__(274),__(276),__(278),__(280),__(282),__(284),							//16
					__(286),__(288),__(290),__(292),__(294),__(296),__(298),__(300),__(302),__(304),__(306),__(308),__(310),__(312),__(314),__(316),							//16
					__(318),__(320),__(322),__(324),__(326),__(328),__(330),__(332),__(334),__(336),__(338),__(340),__(342),__(344),__(346),__(348),							//16
					__(350),__(352),__(354),__(356),__(358),__(360),__(362),__(364),__(366),__(368),__(370),__(372),__(374),__(376),__(378),__(380),							//16
					__(382),__(384),__(386),__(388),__(390),__(392),__(394),__(396),__(398),__(400),__(402),__(404),__(406),__(408),__(410),__(412),							//16
					__(414),__(416),__(418),__(420),__(422),__(424),__(426),__(428),__(430),__(432),__(434),__(436),__(438),__(440),__(442),__(444),							//16
					__(446),__(448),__(450),__(452),__(454),__(456),__(458),__(460),																							//8		//104	//128	- rank 7 size
																																												//----------------------------
/*	254	*/		_r(0),  _r(1),  _r(2),  _r(3),  _r(4),  _r(5),  _r(6),  _r(7),  _r(8),  _r(9),  _r(10), _r(11), _r(12), _r(13), _r(14), _r(15),									//16 /r
				_r(16), _r(17), _r(18), _r(19), _r(20), _r(21), _r(22), _r(23), _r(24), _r(25), _r(26), _r(27), _r(28), _r(29), _r(30), _r(31),									//16 /r
/*	286	*/		_r(32), _r(33), _r(34), _r(35), _r(36), _r(37), _r(38), _r(39), _r(40), _r(41), _r(42), _r(43), _r(44), _r(45), _r(46), _r(47),									//16 /r
				_r(48), _r(49), _r(50), _r(51), _r(52), _r(53), _r(54), _r(55), _r(56), _r(57), _r(58), _r(59), _r(60), _r(61), _r(62), _r(63),									//16 /r
/*	318	*/		_r(64), _r(65), _r(66), _r(67), _r(68), _r(69), _r(70), _r(71), _r(72), _r(73), _r(74), _r(75), _r(76), _r(77), _r(78), _r(79),									//16 /r
				_r(80), _r(81), _r(82), _r(83), _r(84), _r(85), _r(86), _r(87), _r(88), _r(89), _r(90), _r(91), _r(92), _r(93), _r(94), _r(95),									//16 /r
/*	350	*/		_r(96), _r(97), _r(98), _r(99), _r(100),_r(101),_r(102),_r(103),_r(104),_r(105),_r(106),_r(107),_r(108),_r(109),_r(110),_r(111),								//16 /r
				_r(112),_r(113),_r(114),_r(115),_r(116),_r(117),_r(118),_r(119),_r(120),_r(121),_r(122),_r(123),_r(124),_r(125),_r(126),_r(127),								//16 /r
/*	382	*/		_r(128),_r(129),_r(130),_r(131),_r(132),_r(133),_r(134),_r(135),_r(136),_r(137),_r(138),_r(139),_r(140),_r(141),_r(142),_r(143),								//16 /r
				_r(280),_r(281),_r(282),_r(283),_r(284),_r(285),_r(286),_r(287),																								//8  /r	//152
/*	406	*/			__(462),__(464),__(466),__(468),__(470),__(472),__(474),__(476),__(478),__(480),__(482),__(484),__(486),__(488),__(490),__(492),							//16
					__(494),__(496),__(498),__(500),__(502),__(504),__(506),__(508),__(510),__(512),__(514),__(516),__(518),__(520),__(522),__(524),							//16
					__(526),__(528),__(530),__(532),__(534),__(536),__(538),__(540),__(542),__(544),__(546),__(548),__(550),__(552),__(554),__(556),							//16
					__(558),__(560),__(562),__(564),__(566),__(568),__(570),__(572),																							//8		//56	//208	- rank 8 size
																																												//----------------------------
/*	462	*/		_r(144),_r(145),_r(146),_r(147),_r(148),_r(149),_r(150),_r(151),_r(152),_r(153),_r(154),_r(155),_r(156),_r(157),_r(158),_r(159),								//16 /r
				_r(160),_r(161),_r(162),_r(163),_r(164),_r(165),_r(166),_r(167),_r(168),_r(169),_r(170),_r(171),_r(172),_r(173),_r(174),_r(175),								//16 /r
				_r(176),_r(177),_r(178),_r(179),_r(180),_r(181),_r(182),_r(183),_r(184),_r(185),_r(186),_r(187),_r(188),_r(189),_r(190),_r(191),								//16 /r
				_r(192),_r(193),_r(194),_r(195),_r(196),_r(197),_r(198),_r(199),_r(200),_r(201),_r(202),_r(203),_r(204),_r(205),_r(206),_r(207),								//16 /r
				_r(208),_r(209),_r(210),_r(211),_r(212),_r(213),_r(214),_r(215),_r(216),_r(217),_r(218),_r(219),_r(220),_r(221),_r(222),_r(223),								//16 /r
				_r(224),_r(225),_r(226),_r(227),_r(228),_r(229),_r(230),_r(231),_r(232),_r(233),_r(234),_r(235),_r(236),_r(237),_r(238),_r(239),								//16 /r
				_r(240),_r(241),_r(242),_r(243),_r(244),_r(245),_r(246),_r(247),_r(248),_r(249),_r(250),_r(251),_r(252),_r(253),_r(254),_r(255),								//16 /r	//112	//112	- rank 9 size
/*	574 */	};

			generateAccel(unz, unz->default_tree);

			return 1;
		}
	}

#	undef	__
#	undef	_r

	char	deleteHuffTree(t_huffman_tree *tree)	//wont actually delete the tree pointer, only the body of the tree
	{
		if (tree)
		{
			if (tree->symbol_array)
				MY_DELETE_ARRAY(tree->symbol_array);
			if (tree->code_length)
				MY_DELETE_ARRAY(tree->code_length);
		}

		return 1;
	}

	void	getNextHuffSymbol(t_huffman_tree * tree, t_unzib_stream *unz, u16 *value)
	{
		i16		tree_pos;

		tree_pos = 0;
		while (1)
		{

			if (checkNextBitBool(unz->idatHandler))
				tree_pos += 1;

			if (tree->symbol_array[tree_pos] < 0)	//0 - alphabet size -> means the end // instead use negative, maybe faster ?
			{
				//*value = tree->symbol_array[tree_pos];
				*value = tree->symbol_array[tree_pos] + tree->alphabet_size;
				break;
			}
			else									// > alphabet size -> arr = target + alphabet_size //instead is direct stuff
				tree_pos = tree->symbol_array[tree_pos];
			//	tree_pos = (tree->symbol_array[tree_pos] - (checkNextBitBool(unz->idatHandler) ? tree->alphabet_size_minus_one : tree->alphabet_size));
		}
	}

	/*void	getNextHuffSymbol(t_huffman_tree * tree, t_unzib_stream *unz, u16 *value)	//slower, have tested
	{
		i16		tree_pos;

		tree_pos = 0;
		while (1)
		{

			if (checkNextBitBool(unz->idatHandler))
				tree_pos = tree->symbol_array[tree_pos + 1];
			else
				tree_pos = tree->symbol_array[tree_pos];

			if (tree_pos < 0)
			{
				*value = tree_pos + tree->alphabet_size;
				break;
			}
		}
	}*/

	/*void	getNextHuffSymbol8bit(t_huffman_tree * tree, t_unzib_stream *unz, u8 *value)
	{
		i16		tree_pos;

		tree_pos = 0;
		while (1)
		{

			if (checkNextBitBool(unz->idatHandler))
				tree_pos += 1;

			if (tree->symbol_array[tree_pos] < 0)
			{
				*value = tree->symbol_array[tree_pos] + tree->alphabet_size;
				break;
			}
			else
				tree_pos = tree->symbol_array[tree_pos];
		}
	}*/

	/*inline void	getNextHuffDebugSymbol(t_huffman_tree * tree, t_unzib_stream *unz, u16 *value, u16 *res_counter, std::stringstream *out_stream)
	{
		i16		tree_pos;
		u16		counter;

		tree_pos = 0;
		counter = 0;

		//std::cout << "calling debug huffman" << std::endl;
		//out_stream->clear();
		//(*out_stream) << "huffman//";
		while (1)
		{
			counter++;
			if (checkNextBitBool(unz->idatHandler))
			{
				(*out_stream) << "1";
				tree_pos += 1;
			}
			else
				(*out_stream) << "0";

			if (tree->symbol_array[tree_pos] < 0)	//0 - alphabet size -> means the end // instead use negative, maybe faster ?
			{
			//	*value = tree->symbol_array[tree_pos];
				*value = tree->symbol_array[tree_pos] + tree->alphabet_size;
				break;
			}
			else									// > alphabet size -> arr = target + alphabet_size //instead is direct stuff
				tree_pos = tree->symbol_array[tree_pos];
			//	tree_pos = (tree->symbol_array[tree_pos] - (checkNextBitBool(unz->idatHandler) ? tree->alphabet_size_minus_one : tree->alphabet_size));
		}
		*res_counter = counter;
		//(*out_stream) << " //" << (int)counter << " bits";
		//std::cout << out_stream->str() << std::endl;
	}*/

//---------------------------------
//-- Accel Tree
//---------------------------------

#define CALL_MANAGE_BIT_BYTE(__val, __handl) \
			manageByteBit((__val & FASTER_BIT_COUNT_MASK) >> FASTER_BIT_COUNT_MOV, (__val & FASTER_BYTE_COUNT_MASK) >> FASTER_BYTE_COUNT_MOV, __handl);

	/*void	getNextAccel_GEN(u16 const fast_sub_tree[0x8100], u16 * const res, t_idat_handler * const idat_handl)	//256 - 128
	{
		const u16	conc_3bit = ((idat_handl->buffer[idat_handl->index_o] >> idat_handl->index_i) |
			idat_handl->buffer[idat_handl->index_o + 1] << (8 - idat_handl->index_i) |
			idat_handl->buffer[idat_handl->index_o + 2] << (16 - idat_handl->index_i));

		*res = fast_sub_tree[0xFF & conc_3bit];
		if (!((*res) & FASTER_DONE_MASK))
			*res = fast_sub_tree[(*res) << 7 |
			((conc_3bit >> 8) & 0b01111111)]; //val is necessarly done now	//next part dont interrest me

		CALL_MANAGE_BIT_BYTE(*res, idat_handl);
		*res &= FASTER_RES_MASK;
	}*/	// a bit worse, or the same ....

	void	getNextAccel_GEN(u16 const fast_sub_tree[0x8100], u16 * const res, t_idat_handler * const idat_handl)	//256 - 128
	{
		*res = fast_sub_tree[idat_handl->buffer[idat_handl->index_o] >> idat_handl->index_i | (0xFF & (idat_handl->buffer[idat_handl->index_o + 1] << (8 - idat_handl->index_i)))];
		if (!((*res) & FASTER_DONE_MASK))
			*res = fast_sub_tree[(*res) << 7 |
			((idat_handl->buffer[idat_handl->index_o + 1] >> idat_handl->index_i | (0xFF & (idat_handl->buffer[idat_handl->index_o + 2] << (8 - idat_handl->index_i)))) & 0b01111111)]; //val is necessarly done now	//next part dont interrest me

		CALL_MANAGE_BIT_BYTE(*res, idat_handl);
		*res &= FASTER_RES_MASK;
	}

#	define COPY_16_ARR(__dst, __src)				\
	__dst[0] = __src[0]; __dst[1] = __src[1];		\
	__dst[2] = __src[2]; __dst[3] = __src[3];		\
	__dst[4] = __src[4]; __dst[5] = __src[5];		\
	__dst[6] = __src[6]; __dst[7] = __src[7];		\
	__dst[8] = __src[8]; __dst[9] = __src[9];		\
	__dst[10] = __src[10]; __dst[11] = __src[11];	\
	__dst[12] = __src[12]; __dst[13] = __src[13];	\
	__dst[14] = __src[14]; __dst[15] = __src[15];

	void	generateAccel(t_unzib_stream *unz, t_huffman_tree *tree)
	{
	//should move that its done twice actualy
		/*if (unz->b_type == B_TYPE_FIXED_HUFFMAN)
			std::cout << "generating an accel default tree" << std::endl;*/
		u8	length_size[16] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };	//len 0 to 15, len 0 means actualy nothing
		u16	first_len[16] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };		//contain the first char of len [index] + 1 (0 means -1 etc ...)
		u16	next_case_fill[16];
		u16	code;

		for (u16 i = 0; i < tree->alphabet_size; i++)
		{
			length_size[tree->code_length[i]] += 1;
			if (first_len[tree->code_length[i]] == 0)
				first_len[tree->code_length[i]] = i + 1;
		}
		length_size[0] = 0;	//the alphabet of length 0 shouldnt count, they are like not here, but need this 0 value for purposes

		code = 0;
		for (u8 i = 1; i < 16; i++)
		{
			code = (code + length_size[i-1]) << 1;
			next_case_fill[i] = code;
			first_len[i] -= 1;
		}


		fillUpAccel_GEN(unz->faster_lit_len[0], length_size, first_len, next_case_fill, tree);
	}

#	define	FILL_UP_DECLARATION					\
	u16	next_code_of_len[16];					\
	u16	next_char_of_len[16];					\
	u16 next_block_ref;							\
	u16 tmp_curr_block;							\
												\
	u8	tmp_k_max;								\
	u16	tmp_cop;								\
	u16	*tmp_block_pos;							\
												\
	COPY_16_ARR(next_code_of_len, first_code);	\
	COPY_16_ARR(next_char_of_len, first_len);

	//	will fill a 'block' at pos __offset, for target value
	//  q need to be a u16, cuz he will need to actually hit 256 to stop ....
#	define	FILL_SUB_TREE(__offset, __target)						\
	tmp_curr_block = __offset;										\
	for (u16 q = 0; q < __target; q++)								\
		fast_sub_tree[tmp_curr_block | q] &= FASTER_INV_DONE_MASK;

#	define	SET_TMP_COP(__bit_start)																\
	tmp_cop =	(FASTER_DONE_MASK)																|	\
				(FASTER_RES_MASK		& next_char_of_len[i])									|	\
				(FASTER_BIT_COUNT_MASK	& ((i + __bit_start) << FASTER_BIT_COUNT_MOV))			|	\
				(FASTER_BYTE_COUNT_MASK & (((i + __bit_start) / 8) << FASTER_BYTE_COUNT_MOV));

	void	fillUpAccel_GEN(u16 fast_sub_tree[0x8100], u8 len[16], u16 first_len[16], u16 first_code[16], t_huffman_tree *tree)
	{
		FILL_UP_DECLARATION;

		FILL_SUB_TREE(0, 0x100);

		for (u8 i = 1; i <= 8; i++)
		{
			for (u16 j = 0; j < len[i]; j++)
			{
				while (tree->code_length[next_char_of_len[i]] != i)
					next_char_of_len[i]++;
				SET_TMP_COP(0);

				tmp_k_max = 1 << (8 - i);
				for (u16 q = 0; q < tmp_k_max; q++)
					fast_sub_tree[q << i | reverse(next_code_of_len[i] << (8 - i))] = tmp_cop;

				next_code_of_len[i]++;
				next_char_of_len[i]++;
			}
		}
		next_block_ref = 2;
		for (u16 i = 0; i < 0x100; i++)	//for each 'case' of first block, meaning, the first array [i] and the associated block [val ?]
		{
			if (!(fast_sub_tree[i] & FASTER_DONE_MASK))	//else already treated earlier
			{
				fast_sub_tree[i] = FASTER_INV_DONE_MASK & next_block_ref;	//set first block val, where to jump

				FILL_SUB_TREE(next_block_ref << 7, 0x080);
				next_block_ref++;
			}
		}

		for (u8 i = 9; i <= 15; i++)
		{
			for (u16 j = 0; j < len[i]; j++)
			{
				while (tree->code_length[next_char_of_len[i]] != i)
					next_char_of_len[i]++;
				SET_TMP_COP(0);

				tmp_block_pos = &fast_sub_tree[(fast_sub_tree[reverse(next_code_of_len[i] >> (i - 8))]) << 7];
				tmp_k_max = 1 << (15 - i);
				for (u16 q = 0; q < tmp_k_max; q++)
					tmp_block_pos[q << (i - 8) | reverse((0x00FF & next_code_of_len[i]) << (16 - i))] = tmp_cop;	//squish til only 1, then reverso

				next_char_of_len[i]++;
				next_code_of_len[i]++;
			}
		}
	}

}