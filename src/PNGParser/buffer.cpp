#include "../ProjectDefines.h"

#include "buffer.h"

#include <string.h>

namespace PNG
{
	char	getFirstBuffer(singlet *buffer, FILE * file)
	{
		return (fread(buffer, 33, 1, file) == 1);
	}

	char	getIHDRFromBuffer(t_IHDR_chunk *ihdr, const singlet *buffer)
	{
		ihdr->width = reverseOctetOrder((quadet *)(&(buffer[0x10])));
		ihdr->height = reverseOctetOrder((quadet *)(&(buffer[0x14])));
		ihdr->bit_depth = buffer[0x18];
		ihdr->colour_type = buffer[0x19];
		ihdr->compression = buffer[0x1a];
		ihdr->filter = buffer[0x1b];
		ihdr->interlace = buffer[0x1c];
		ihdr->CRC = reverseOctetOrder((quadet *)(&buffer[0x1d]));

		return 1;
	}

	quadet	reverseOctetOrder(const quadet *ref)
	{
		return ((((*ref) & 0x000000ff) << 24) +
			(((*ref) & 0x0000ff00) << 8) +
			(((*ref) & 0x00ff0000) >> 8) +
			(((*ref) & 0xff000000) >> 24));
	}

	char	newBuffer(t_buffer *buffer, FILE *file)
	{
		buffer->file = file;
		buffer->index = 0;
		buffer->move = 0;
		buffer->end_pos = static_cast<u32>(fread(buffer->buff, 1, BUFFER_SIZE, file));

		//ft_log(LOG_FINE, 0, "end pos : %d", buffer->end_pos);

		return (buffer->end_pos > 0);
	}

	char	getQuadBuffer(t_buffer *buffer, quadet **res)		//direct pointer to the real value
	{
		updateBufferAndSetMove(buffer, 4);

		if (buffer->index + 4 >= buffer->end_pos)
			return (0);

		(*res) = (quadet *) &(buffer->buff[buffer->index]);
		return (1);
	}

	char	getRevQuadBuffer(t_buffer *buffer, quadet *res)	//copy a value in here with reverse quad
	{
		quadet *tmp;

		if (!getQuadBuffer(buffer, &tmp))
			return (0);
		else
			*res = reverseOctetOrder(tmp);

		return (1);
	}

	char	getSingleBuffer(t_buffer *buffer, singlet **res)
	{
		updateBufferAndSetMove(buffer, 1);

		if (buffer->index + 1 >= buffer->end_pos)
			return (0);

		(*res) = buffer->buff + buffer->index;
		return (1);
	}

	char	getSingleCopBuffer(t_buffer *buffer, singlet *res)
	{
		updateBufferAndSetMove(buffer, 1);

		if (buffer->index + 1 >= buffer->end_pos)
			return (0);

		(*res) = *(buffer->buff + buffer->index);
		return (1);
	}

	char	jumpBuffer(t_buffer *buffer, u64 jump)
	{
		updateBufferAndSetMove(buffer, jump);
		updateBufferAndSetMove(buffer, 0);			//do this to update immediatly and check is
		return (!(buffer->index >= buffer->end_pos));	//i still have files left
	}

	char	copyFromBuffer(t_buffer *buffer, singlet *dst, u64 size)
	{
		u64	rem_size;
		//u64	copied_alrdy;

		updateBufferAndSetMove(buffer, 0);

		rem_size = size;
		while (buffer->index < buffer->end_pos && rem_size > 0)	//i think it work that way ?, if i can go around, end_pos is always higher than index (cuz + 7 ??)
		{
			if ((buffer->end_pos - buffer->index) > rem_size)
			{
				memcpy(dst + (size - rem_size), buffer->buff + buffer->index, static_cast<std::size_t>(rem_size));
				buffer->index += static_cast<u32>(rem_size);
				rem_size = 0;
			}
			else
			{
				memcpy(dst + (size - rem_size), buffer->buff + buffer->index, buffer->end_pos - buffer->index);
				rem_size -= (buffer->end_pos - buffer->index);
				buffer->index = buffer->end_pos;

				advanceBuffer(buffer);
			}
		}

		return (rem_size == 0);
	}


	void	updateBufferAndSetMove(t_buffer *buffer, u64 move)
	{
		if (buffer->move != 0)
		{
			buffer->index += static_cast<u32>(buffer->move);
			if (buffer->index >= BUFFER_SIZE_REAL)
				advanceBuffer(buffer);
		}

		buffer->move = move;
	}

	void	updateBuffer(t_buffer *buffer)
	{
		updateBufferAndSetMove(buffer, 0);
	}

	void	advanceBuffer(t_buffer *buffer)
	{
		unsigned int	i;
		char			is_end;

		while (buffer->index >= BUFFER_SIZE_REAL)
		{
			is_end = 0;

			if (buffer->end_pos < BUFFER_SIZE)
			{
				if (buffer->end_pos >= BUFFER_SIZE_REAL)
					buffer->end_pos -= BUFFER_SIZE_REAL;
				else
					buffer->end_pos = 0;
				is_end = 1;
			}

			i = 0;
			while (i < 7)
			{
				buffer->buff[i] = buffer->buff[BUFFER_SIZE_REAL + i];
				i++;
			}

			buffer->index -= BUFFER_SIZE_REAL;

			if (!is_end)
				buffer->end_pos = static_cast<u32>(fread(buffer->buff + 7, 1, BUFFER_SIZE_REAL, buffer->file) + 7);
		}
	}
}