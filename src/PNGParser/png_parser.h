#pragma once

#include <stdlib.h>
#include <stdio.h>

#include "chunk.h"
#include "png_type.h"
#include "buffer.h"
#include "unzib_stream.h"
#include "image_data.h"

namespace PNG
{
	enum E_PNG_PARSING_ERROR : char
	{
		PNG_FINE,
		PNG_INVALID_FILE_FORMAT,
		PNG_BUFFER_CREATION_ERROR,
		PNG_CANT_OPEN_FILE
	};

	extern const quadet		png_code_s;
	extern const quadet		png_code_e;

	extern const quadet		png_code_s_r; //reverse bytes order
	extern const quadet		png_code_e_r; //reverse bytes order

	extern const octetet	png_code_r;
	//static const singlet	png_code_t[4] = {0x89, 0x50, 0x4e, 0x47};
	//static const quadet_t	png_code_t = {0x89, 0x50, 0x4e, 0x47};


	char			test_parse(const char *file_name, t_image_data *img, PNG_REVERSE v_reverse);
	char			test_parse_on_file(FILE *file, t_image_data *img, PNG_REVERSE v_reverse);
	char			cleaner_parse_file(FILE *file, t_image_data *img, PNG_REVERSE v_reverse);

	char			getQuad(quadet *quad, FILE *file);
	char			getChar(singlet *car, FILE *file);

	char			getExpectedQuad(quadet *quad, FILE *file, const singlet *exp);
	char			checkExpectedQuad(FILE *file, const singlet *exp);

	//char			check_png_code(FILE *file);

	char			getChunk(t_partial_chunk *p_chunk, FILE *file);							//
	e_chunk_type	getChunkFromTable(t_partial_chunk *p_chunk, FILE *file);
	char			getExpectedCodeChunk(t_partial_chunk *p_chunk, FILE *file); //will stop when an octet isnt coinciding, use the code in the t_partial_chunk, and return in it too
	char			checkExpectedChunk(const t_partial_chunk *p_chunk, FILE *file); //will stop when an octet isnt coinciding

	char			getIHDRChunk(t_IHDR_chunk *ihdr, const t_partial_chunk *p_chunk, FILE *file);

	char			jumpOctets(const quadet *length, FILE *file);
	char			jumpChunkData(const quadet *length, FILE *file);

	//new system
	char			checkBufferStartValidity(const singlet *buffer);	//33octets buffer

	e_chunk_type	getChunkFromTableBuffer(t_partial_chunk *p_chunk, t_buffer *buffer);

	char			treatIDAT(t_idat_handler *idat_handl, const t_partial_chunk *p_chunk, t_buffer *buffer);

	void			printQuadet(quadet quad);
	void			printOctetet(octetet oct);
	void			printIntQuadet(quadet quad);
}