#include "ProjectDefines.h"

#include "Loader.h"
#include "OpenGL.h"
#include "ImageParser.h"
#include "Assert.h"

#include <regex>

RawModel Loader::loadToVAO(const std::vector<GLfloat>& positions, const std::vector<GLuint>& indices, const std::vector<GLfloat>& uvs, const std::vector<GLfloat>& normals)
{
	return loadToVAO(positions.data(), positions.size(), indices.data(), indices.size(), uvs.data(), uvs.size(), normals.data(), normals.size());
}

RawModel Loader::loadToVAO(const GLfloat* positions, unsigned int size, const GLuint* indices, unsigned int sizeIndices, const GLfloat* textureCoords, unsigned int sizeUV, const GLfloat* normals, unsigned int sizeNormals)
{
	const GLuint vaoID = createVAO();
	bindIndicesBuffer(indices, sizeIndices * sizeof(GLuint));
	storeDataInAttributeList(0, 3, positions, size * sizeof(GLfloat));
	storeDataInAttributeList(1, 2, textureCoords, sizeUV * sizeof(GLfloat));
	storeDataInAttributeList(2, 3, normals, sizeNormals * sizeof(GLfloat));
	unbindVAO();
	return { vaoID, sizeIndices };
}

RawModel Loader::loadToVAO(const GLfloat* positions, unsigned int size, const GLuint* indices, unsigned int sizeIndices)
{
	const GLuint vaoID = createVAO();
	bindIndicesBuffer(indices, sizeIndices * sizeof(GLuint));
	storeDataInAttributeList(0, 3, positions, size * sizeof(GLfloat));
	unbindVAO();
	return { vaoID, sizeIndices };
}

RawModel Loader::loadToVAO(const GLfloat* positions, unsigned int size, unsigned int dimensions)
{
	const GLuint vaoID = createVAO();
	storeDataInAttributeList(0, dimensions, positions, size * sizeof(GLfloat));
	unbindVAO();
	return { vaoID, size / dimensions };
}

GLuint Loader::loadTexture(const std::string& path)
{
	GLuint textureID = parseTextureFile(path);

	if (textureID != 0)
		textures.push_back(textureID);

	return textureID;
}

GLuint Loader::parseTextureFile(const std::string& path) const
{
	BufferedImage infos;

	if (std::regex_match(path, std::regex("^.*\\.bmp$")))
	{
		infos = ParseBMPFile(path.c_str());
	}
	else if (std::regex_match(path, std::regex("^.*\\.png$")))
	{
		infos = ParsePNGFile(path.c_str());
	}
	else
	{
		AssertErrorMsg(false, path, ": format not supported.");
		return 0;
	}

	if (infos.datas == nullptr) {
		AssertError(false);
		return 0;
	}

	GLuint id;
	glGenTextures(1, &id);
	glBindTexture(GL_TEXTURE_2D, id);
	glTexImage2D(GL_TEXTURE_2D, 0, infos.internalFormat, infos.width, infos.height, 0, infos.format, GL_UNSIGNED_BYTE, infos.datas);

	glGenerateMipmap(GL_TEXTURE_2D);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_LOD_BIAS, -0.4f);

	delete[] infos.datas;

	return id;
}

void Loader::cleanUp()
{
	foreachitem(vao, vaos)
		glDeleteVertexArrays(1, &vao);

	foreachitem(vbo, vbos)
		glDeleteBuffers(1, &vbo);

	foreachitem(texture, textures)
		glDeleteTextures(1, &texture);
}

GLuint Loader::createVAO()
{
	GLuint vaoID;
	glGenVertexArrays(1, &vaoID);
	vaos.push_back(vaoID);
	glBindVertexArray(vaoID);
	return vaoID;
}

void Loader::storeDataInAttributeList(int attributeNumber, GLint coordinateSize, const float* data, unsigned int size)
{
	GLuint vboID;
	glGenBuffers(1, &vboID);
	vbos.push_back(vboID);
	glBindBuffer(GL_ARRAY_BUFFER, vboID);
	glVertexAttribPointer(attributeNumber, coordinateSize, GL_FLOAT, GL_FALSE, 0, nullptr);
	glBufferData(GL_ARRAY_BUFFER, size, data, GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void Loader::unbindVAO() const
{
	glBindVertexArray(0);
}

void Loader::bindIndicesBuffer(const GLuint * indices, unsigned int size)
{
	GLuint vboID;
	glGenBuffers(1, &vboID);
	vbos.push_back(vboID);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vboID);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, size, indices, GL_STATIC_DRAW);
}
