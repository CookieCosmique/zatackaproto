#include "ProjectDefines.h"

#include "Assert.h"
#include "Breakpoint.h"
#include "IsInDebugger.h"

#include <iostream>

#ifdef DEBUG

namespace Assert
{
	void MyAssert(const char* parType, const char* parCondition, const char* parFile, const unsigned int parLine, const std::string parMessage)
	{
		std::stringstream ss;

		ss << "/!\\ Assertion failed /!\\ (" << parType << ")\n";
		ss << "\t" << parFile << ": line " << parLine << " (" << parCondition << ")";

		if (!parMessage.empty())
			ss << "\n\t - " << parMessage;

		std::cerr << ss.str() << std::endl;

		if (IsInDebugger())
			breakpoint();
	}
} // !namespace Assert

#endif
