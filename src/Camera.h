#pragma once

#include "glm/glm.hpp"

struct Camera
{
	glm::vec3 position;
	float pitch;
	float yaw;
	float roll;
};
