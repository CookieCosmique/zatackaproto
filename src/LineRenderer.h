#pragma once

#include "Camera.h"
#include "LineShader.h"
#include "Light.h"
#include "RawModel.h"

#include "glm/glm.hpp"

#include <map>
#include <vector>

class LineRenderer
{
public:
	LineRenderer(const glm::mat4& projectionMatrix);

	void render(const Camera& camera, const Light& light, GLuint vao, GLuint size) const;

private:
	LineShader shader;
};
