#include "ProjectDefines.h"

#include "LineShader.h"

#include "Maths.h"

#include <string>

#ifdef __EMSCRIPTEN__
LineShader::LineShader() :
	ShaderProgram("Shaders/lineVertexShader.glsl", "Shaders/lineFragmentShader.glsl")
#else
LineShader::LineShader() :
	ShaderProgram("Shaders/lineVertexShader.glsl", "Shaders/lineGeometryShader.glsl", "Shaders/lineFragmentShader.glsl")
#endif
{
	init();
}

LineShader::~LineShader()
{

}

void LineShader::loadProjectionMatrix(const glm::mat4& matrix) const
{
	loadMatrix(location_projectionMatrix, matrix);
}

void LineShader::loadViewMatrix(const Camera& camera) const
{
	loadMatrix(location_viewMatrix, Maths::createViewMatrix(camera));
}

void LineShader::loadLight(const Light& light) const
{
	loadVector(location_lightPosition, light.position);
	loadVector(location_lightColour, light.colour);
}

void LineShader::bindAttributes() const
{
	bindAttribute(0, "position");
	bindAttribute(1, "normal");
}

void LineShader::getAllUniformLocations()
{
	location_projectionMatrix = getUniformLocation("projectionMatrix");
	location_viewMatrix = getUniformLocation("viewMatrix");

	location_lightPosition = getUniformLocation("lightPosition");
	location_lightColour = getUniformLocation("lightColour");
}
