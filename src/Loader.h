#pragma once

#include "RawModel.h"

#include "OpenGL.h"

#include <vector>
#include <string>

class Loader
{
public:
	RawModel loadToVAO(const std::vector<GLfloat>& positions, const std::vector<GLuint>& indices, const std::vector<GLfloat>& uvs, const std::vector<GLfloat>& normals);
	RawModel loadToVAO(const GLfloat* positions, unsigned int size, const GLuint* indices, unsigned int sizeIndices, const GLfloat* textureCoords, unsigned int sizeUV, const GLfloat* normals, unsigned int sizeNormals);
	RawModel loadToVAO(const GLfloat* positions, unsigned int size, const GLuint* indices, unsigned int sizeIndices);
	RawModel loadToVAO(const GLfloat* positions, unsigned int size, unsigned int dimensions);

	GLuint loadTexture(const std::string& path);

	void cleanUp();

private:
	GLuint createVAO();
	void storeDataInAttributeList(int attributeNumber, GLint coordinateSize, const float* data, unsigned int size);
	void unbindVAO() const;
	void bindIndicesBuffer(const GLuint* indices, unsigned int size);
	GLuint parseTextureFile(const std::string& path) const;

private:
	std::vector<GLuint> vaos;
	std::vector<GLuint> vbos;
	std::vector<GLuint> textures;
};
