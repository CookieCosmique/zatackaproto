#include "ProjectDefines.h"

#include "Assert.h"

#include "ShaderProgram.h"

#include <fstream>

ShaderProgram::ShaderProgram(const std::string& vertexFile, const std::string& fragmentFile)
{
	vertexShaderID = loadShader(vertexFile, GL_VERTEX_SHADER);
	fragmentShaderID = loadShader(fragmentFile, GL_FRAGMENT_SHADER);
	geometryShaderID = 0;
}

#ifndef __EMSCRIPTEN__
ShaderProgram::ShaderProgram(const std::string& vertexFile, const std::string& geometryFile, const std::string& fragmentFile)
{
	vertexShaderID = loadShader(vertexFile, GL_VERTEX_SHADER);
	geometryShaderID = loadShader(geometryFile, GL_GEOMETRY_SHADER);
	fragmentShaderID = loadShader(fragmentFile, GL_FRAGMENT_SHADER);
}
#endif

void ShaderProgram::init()
{
	programID = glCreateProgram();
	AssertError(programID != 0);

	glAttachShader(programID, vertexShaderID);
	if (geometryShaderID != 0)
		glAttachShader(programID, geometryShaderID);
	glAttachShader(programID, fragmentShaderID);
	bindAttributes();
#ifndef __EMSCRIPTEN__
	glBindFragDataLocation(programID, 0, "out_Color");
#endif
	glLinkProgram(programID);
	glValidateProgram(programID);
	getAllUniformLocations();
}

void ShaderProgram::start() const
{
	glUseProgram(programID);
}

void ShaderProgram::stop() const
{
	glUseProgram(0);
}

void ShaderProgram::cleanUp() const
{
	stop();
	glDetachShader(programID, vertexShaderID);
	if (geometryShaderID != 0)
		glDetachShader(programID, geometryShaderID);
	glDetachShader(programID, fragmentShaderID);
	glDeleteShader(vertexShaderID);
	if (geometryShaderID != 0)
		glDeleteShader(geometryShaderID);
	glDeleteShader(fragmentShaderID);
	glDeleteProgram(programID);
}

GLuint ShaderProgram::GetAttribLocation(const char* attribute) const {
	return glGetAttribLocation(programID, attribute);
}

void ShaderProgram::bindAttribute(GLuint attribute, const std::string& name) const
{
	glBindAttribLocation(programID, attribute, name.c_str());
}

GLuint ShaderProgram::getUniformLocation(const char* uniformName) const
{
	return glGetUniformLocation(programID, uniformName);
}

void ShaderProgram::loadInt(GLuint location, GLint value) const
{
	glUniform1i(location, value);
}

void ShaderProgram::loadFloat(GLuint location, GLfloat value) const
{
	glUniform1f(location, value);
}

void ShaderProgram::loadVector(GLuint location, const glm::vec3& vector) const
{
	glUniform3f(location, vector.x, vector.y, vector.z);
}

void ShaderProgram::loadVector(GLuint location, const glm::vec2& vector) const
{
	glUniform2f(location, vector.x, vector.y);
}

void ShaderProgram::loadBool(GLuint location, bool value) const
{
	glUniform1i(location, value ? 1 : 0);
}

void ShaderProgram::loadMatrix(GLuint location, const glm::mat4& matrix) const
{
	glUniformMatrix4fv(location, 1, GL_FALSE, &matrix[0][0]);
}

GLuint ShaderProgram::loadShader(const std::string& path, GLenum type)
{
	printf("Loading shader: %s\n", path.c_str());
	std::ifstream file(path.c_str(), std::ifstream::in);
	if (!file.is_open())
	{
		AssertErrorMsg(false, "Cannot open file: ", path);
		printf("Cannot open file: %s\n", path.c_str());
		return 0;
	}
	std::string line;
	std::stringstream ss;
	while (std::getline(file, line))
		ss << line << "\n";
	GLchar* source = new char[ss.str().length() + 1];
#if (defined(_WIN32) || defined(_WIN64)) && !defined(__CYGWIN__)
	strcpy_s(source, ss.str().length() + 1, ss.str().c_str());
#else
	strcpy(source, ss.str().c_str());
#endif
	file.close();

	GLuint shaderID = glCreateShader(type);
	AssertError(shaderID != 0);
	glShaderSource(shaderID, 1, &source, nullptr);
	glCompileShader(shaderID);

	GLint status;
	glGetShaderiv(shaderID, GL_COMPILE_STATUS, &status);
	if (status == GL_FALSE)
	{
		char buffer[512];
		glGetShaderInfoLog(shaderID, 512, NULL, buffer);
		AssertErrorMsg(false, "Shader error (", path, "): ", buffer);
		printf("Shader error (%s): %s\n", path.c_str(), buffer);
	} else {
		printf("Shader loaded: %s\n", path.c_str());
	}

	delete[] source;

	return shaderID;
}
