#include "ProjectDefines.h"

#include "Maths.h"
#include "Camera.h"

#include "glm/gtc/matrix_transform.hpp"

glm::mat4 Maths::createTransformationMatrix(const glm::vec3& translation, const glm::vec3& rotation, const glm::vec3& scale)
{
	glm::mat4 matrix(1.f);
	matrix = glm::translate(matrix, translation);
	matrix = glm::rotate(matrix, toRadian(rotation.y), glm::vec3(0, 1, 0));
	matrix = glm::rotate(matrix, toRadian(rotation.x), glm::vec3(1, 0, 0));
	matrix = glm::rotate(matrix, toRadian(rotation.z), glm::vec3(0, 0, 1));
	matrix = glm::scale(matrix, scale);
	return matrix;
}

glm::mat4 Maths::createTransformationMatrix(const Entity& entity)
{
	return createTransformationMatrix(entity.position, entity.rotation, entity.scale);
}

glm::mat4 Maths::createProjectionMatrix(float fov, float width, float height, float near, float far)
{
	return glm::perspectiveFov(toRadian(fov), width, height, near, far);
//	return glm::ortho(-width / 50, width / 50, -height / 50, height / 50, 1.f, 1000.f);
}

glm::mat4 Maths::createViewMatrix(const Camera& camera)
{
	glm::mat4 matrix(1.f);
	matrix = glm::translate(matrix, camera.position);
	matrix = glm::rotate(matrix, toRadian(camera.yaw), glm::vec3(0.f, 1.f, 0.f));
	matrix = glm::rotate(matrix, toRadian(camera.pitch), glm::vec3(1.f, 0.f, 0.f));
	matrix = glm::rotate(matrix, toRadian(camera.roll), glm::vec3(0.f, 0.f, 1.f));
	matrix = glm::inverse(matrix);
	return matrix;
//	return glm::lookAt(camera.position, camera.position + glm::vec3(camera.pitch, camera.yaw, camera.roll), glm::vec3(0.f, 1.f, 0.f));
}
