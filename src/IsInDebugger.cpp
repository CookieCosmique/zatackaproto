#include "ProjectDefines.h"

#if defined(_WIN32) || defined(_WIN64) || defined(__CYGWIN__)

#include <windows.h>
#include <debugapi.h>

bool IsInDebugger(void)
{
	return IsDebuggerPresent() != 0;
}

#elif defined (__APPLE__)

#include <stdbool.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/sysctl.h>

bool IsInDebugger(void)
{
	int                 junk;
	int                 mib[4];
	struct kinfo_proc   info;
	size_t              size;

	// Initialize the flags so that, if sysctl fails for some bizarre
	// reason, we get a predictable result.

	info.kp_proc.p_flag = 0;

	// Initialize mib, which tells sysctl the info we want, in this case
	// we're looking for information about a specific process ID.

	mib[0] = CTL_KERN;
	mib[1] = KERN_PROC;
	mib[2] = KERN_PROC_PID;
	mib[3] = getpid();

	// Call sysctl.

	size = sizeof(info);
	junk = sysctl(mib, sizeof(mib) / sizeof(*mib), &info, &size, NULL, 0);

	// We're being debugged if the P_TRACED flag is set.

	return ((info.kp_proc.p_flag & P_TRACED) != 0);
}

#elif defined (__linux__)
#include <sys/stat.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>

bool IsInDebugger(void)
{
	char buf[1024];
	int debugger_present = 0;

	int status_fd = open("/proc/self/status", O_RDONLY);
	if (status_fd == -1)
		return 0;

	ssize_t num_read = read(status_fd, buf, sizeof(buf));

	if (num_read > 0)
	{
		static const char TracerPid[] = "TracerPid:";
		char *tracer_pid;

		buf[num_read] = 0;
		tracer_pid = strstr(buf, TracerPid);
		if (tracer_pid)
			debugger_present = !!atoi(tracer_pid + sizeof(TracerPid) - 1);
	}

	return debugger_present != 0;
}

#elif defined(__EMSCRIPTEN__)
bool IsInDebugger(void)
{
	return false;
}
#else
# error "platform not supported"
#endif
