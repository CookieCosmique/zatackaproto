#pragma once

#include "ShaderProgram.h"

#include <vector>

#include "glm/glm.hpp"

#include "Camera.h"
#include "Light.h"

class LineShader : public ShaderProgram
{
public:
	LineShader();
	~LineShader();

	void loadProjectionMatrix(const glm::mat4& matrix) const;
	void loadViewMatrix(const Camera& camera) const;

	void loadLight(const Light& light) const;

protected:
	virtual void bindAttributes() const override;
	virtual void getAllUniformLocations() override;

private:
	GLuint location_projectionMatrix;
	GLuint location_viewMatrix;

	GLuint location_lightPosition;
	GLuint location_lightColour;
};
