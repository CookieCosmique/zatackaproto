#pragma once

#include "Camera.h"
#include "GLineShader.h"
#include "Light.h"
#include "RawModel.h"

#include "glm/glm.hpp"

#include <map>
#include <vector>
#include "Options.h"

struct RenderLineOptions
{
	glm::vec3 color;
};

struct RenderLine
{
	GLuint vao;
	GLuint size;
	RenderLineOptions options;
};

struct RenderLines
{
	static constexpr size_t MAX_RENDER_LINE_COUNT = 2;
	RenderLine renderLines[MAX_RENDER_LINE_COUNT];
	size_t count;
};

class GLineRenderer
{
public:
	GLineRenderer(const glm::mat4& projectionMatrix);

	void render(const Camera& camera, const Light& light, const RenderLines& lines, const Options& options) const;

private:
#ifdef __EMSCRIPTEN__
	GLineShader shader = GLineShader("Shaders/glineVertexShader.glsl", "Shaders/glineFragmentShader.glsl");
	GLineShader shaderDebug = GLineShader("Shaders/glineVertexShader.glsl", "Shaders/glineFragmentShader.glsl");
#else
	GLineShader shader = GLineShader("Shaders/glineVertexShader.glsl", "Shaders/glineGeometryShader.glsl", "Shaders/glineFragmentShader.glsl");
	GLineShader shaderDebug = GLineShader("Shaders/glineVertexShader.glsl", "Shaders/glineGeometryShaderDebug.glsl", "Shaders/glineFragmentShader.glsl");
#endif
};
