#pragma once

#include "OpenGL.h"

class DisplayManager
{
public:
	DisplayManager();

	void createDisplay();
	void updateDisplay();
	void closeDisplay();

	void requestClose();
	bool isCloseRequested();

	GLFWwindow* getWindow() { return window; }

	float getFrameTimeSeconds() { return delta; }

private:
	long getCurrentTime();

private:
	GLFWwindow* window;

	static const int WIDTH;
	static const int HEIGHT;

	long lastFrameTime;
	float delta;
};
