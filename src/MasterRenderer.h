#pragma once

#include "Camera.h"
#include "Light.h"

#include "TexturedModel.h"
#include "Entity.h"
#include "Options.h"

class EntityRenderer;
class TerrainRenderer;
class LineRenderer;
class GLineRenderer;
class Loader;
struct RenderLines;

class MasterRenderer
{
public:
	MasterRenderer(GLuint defaultTexture, Loader* loader);
	~MasterRenderer();

	void render(const Camera& camera, const Light& light, const RenderLines& lines, const Options& options) const;

	void cleanUp();

	void addEntity(const TexturedModel* model, const Entity* entity);

private:
	void prepare() const;

private:
	glm::mat4 projectionMatrix;
	EntityRenderer* entityRenderer;
	TerrainRenderer* terrainRenderer;
	LineRenderer* lineRenderer;
	GLineRenderer* glineRenderer;
};
