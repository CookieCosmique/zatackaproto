#include "ProjectDefines.h"

#include "ImageParser.h"

#include "Assert.h"

#include "PNGParser/png_parser.h"

ColorRGBA BufferedImage::getColor(unsigned int x, unsigned int y) const
{
	AssertFatal(x >= 0 && x < width && y >= 0 && y < height);

	unsigned int pos = (x + y * width) * channels * bitDepth / 8;

	ColorRGBA color;

	switch (channels)
	{
	case 1: // greyscale
		color.r = ((float)datas[pos] / ((float)(1 << bitDepth) - 1));
		color.g = ((float)datas[pos] / ((float)(1 << bitDepth) - 1));
		color.b = ((float)datas[pos] / ((float)(1 << bitDepth) - 1));
		color.a = 1.f;
		break;
	case 2: // greyscale + alpha
		color.r = ((float)datas[pos] / ((float)(1 << bitDepth) - 1));
		color.g = ((float)datas[pos] / ((float)(1 << bitDepth) - 1));
		color.b = ((float)datas[pos] / ((float)(1 << bitDepth) - 1));
		color.a = ((float)datas[pos + 1 * bitDepth / 8] / ((float)(1 << bitDepth) - 1));
		break;
	case 3: // rgb
		color.r = ((float)datas[pos] / ((float)(1 << bitDepth) - 1));
		color.g = ((float)datas[pos + 1 * bitDepth / 8] / ((float)(1 << bitDepth) - 1));
		color.b = ((float)datas[pos + 2 * bitDepth / 8] / ((float)(1 << bitDepth) - 1));
		color.a = 1.f;
		break;
	case 4: //rgba
		color.r = ((float)datas[pos] / ((float)(1 << bitDepth) - 1));
		color.g = ((float)datas[pos + 1 * bitDepth / 8] / ((float)(1 << bitDepth) - 1));
		color.b = ((float)datas[pos + 2 * bitDepth / 8] / ((float)(1 << bitDepth) - 1));
		color.a = ((float)datas[pos + 3 * bitDepth / 8] / ((float)(1 << bitDepth) - 1));
		break;
	default:
		AssertNotImplemented();
	}

	return color;
}

BufferedImage ParseBMPFile(const char* path)
{
	BufferedImage infos = {};

	FILE *file;
#if (defined(_WIN32) || defined(_WIN64)) && !defined(__CYGWIN__)
	fopen_s(&file, path, "rb");
#else
	file = fopen(path, "rb");
#endif
	if (file == nullptr) {
		AssertErrorMsg(false, path);
		printf("Failed to open file: %s\n", path);
		return infos;
	}

	unsigned char hd[54];
	size_t ret = fread(hd, 1, 54, file);
	(void)ret;
	AssertError(ret == 54);
	AssertError(hd[0] == 'B');
	AssertError(hd[1] == 'M');

	infos.width = *(int*)&(hd[0x12]);
	infos.height = *(int*)&(hd[0x16]);
	//unsigned int bitPerPixel = *(int*)&(hd[28]);
	unsigned int size = *(int*)&(hd[0x22]);

	if (size == 0)
		size = infos.width * infos.height * 3;

	unsigned char* outDatas = new unsigned char[size];
	fread(outDatas, 1, size, file);

	fclose(file);

	infos.datas = outDatas;

//	if (bitPerPixel / 8 == 3)
//	{
		infos.internalFormat = GL_RGB;
		infos.format = GL_BGR;
/*	}
	else if (bitPerPixel / 8 == 4)
	{
		infos.internalFormat = GL_RGBA;
		infos.format = GL_BGRA;
	}
	else
	{
		AssertNotImplemented();
	}*/

	infos.bitDepth = 8;
	infos.channels = 3;

	return infos;
}

BufferedImage ParsePNGFile(const char* path)
{
	BufferedImage infos;

	PNG::t_image_data img;

	PNG::E_PNG_PARSING_ERROR test_parse = PNG::E_PNG_PARSING_ERROR(PNG::test_parse(path, &img, PNG::V_REVERSE));
	AssertErrorMsg(test_parse == PNG::PNG_FINE, "PNG parsing error: ", test_parse);
	(void)test_parse;

	unsigned char* datas = img.data;
	infos.width = static_cast<u32>(img.width);
	infos.height = static_cast<u32>(img.height);

	if (img.colour_type == 2)
	{
		if (img.bit_depth == 16)
			infos.internalFormat = GL_RGB16;
		else
			infos.internalFormat = GL_RGB;
	}
	else
	{
		if (img.bit_depth == 16)
			infos.internalFormat = GL_RGBA16UI;
		else
			infos.internalFormat = GL_RGBA;
	}

	infos.format = infos.internalFormat;

	infos.datas = datas;
	infos.bitDepth = img.bit_depth;
	infos.channels = img.channel;

	return infos;
}
