#pragma once

#include "Camera.h"
#include "Entity.h"
#include "EntityShader.h"
#include "Light.h"
#include "TexturedModel.h"

#include "glm/glm.hpp"

#include <map>
#include <vector>
#include "Options.h"

class EntityRenderer
{
public:
	EntityRenderer(const glm::mat4& projectionMatrix, GLuint defaultTexture);

	void render(const Camera& camera, const Light& light, const Options& options) const;

	void addEntity(const TexturedModel* model, const Entity* entity);
	void cleanUp();

private:
	void prepareTexturedModel(const TexturedModel* model) const;
	void unbindTexturedModel() const;

private:
#ifdef __EMSCRIPTEN__
	EntityShader shader = EntityShader("Shaders/arrowVertexShader.glsl", "Shaders/arrowFragmentShader.glsl");
	EntityShader shaderDebug = EntityShader("Shaders/arrowVertexShader.glsl", "Shaders/arrowFragmentShader.glsl");
#else
	EntityShader shader = EntityShader("Shaders/arrowVertexShader.glsl", "Shaders/arrowGeometryShader.glsl", "Shaders/arrowFragmentShader.glsl");
	EntityShader shaderDebug = EntityShader("Shaders/arrowVertexShader.glsl", "Shaders/arrowGeometryShaderDebug.glsl", "Shaders/arrowFragmentShader.glsl");
#endif
	std::map<const TexturedModel*, std::vector<const Entity*>> entities;
	GLuint defaultTexture;
};
