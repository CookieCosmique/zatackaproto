#pragma once

#define _USE_MATH_DEFINES
#include <cmath>

#include "glm/glm.hpp"

#define toRadian(x) ((float) (x * (M_PI / 180.f)))

#include "Camera.h"
#include "Entity.h"

class Maths
{
public:
	static glm::mat4 createTransformationMatrix(const glm::vec3& translation, const glm::vec3& rotation, const glm::vec3& scale);
	static glm::mat4 createTransformationMatrix(const Entity& entity);
	static glm::mat4 createProjectionMatrix(float fov, float width, float height, float near, float far);
	static glm::mat4 createViewMatrix(const Camera& camera);
};
