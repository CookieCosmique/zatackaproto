#pragma once

#include "RawModel.h"

struct TexturedModel
{
	RawModel model;
	GLuint texture;
};
