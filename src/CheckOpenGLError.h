#pragma once

#include <stdio.h>

#include "OpenGL.h"

#include "Breakpoint.h"

#include <sstream>
#include <iostream>

#ifdef DEBUG
#define CGLE() CheckOpenGLError(__FILE__, __LINE__)
#else
#define CGLE() DONOTHING
#endif

static inline void CheckOpenGLError(const char* const file, const unsigned int line)
{
	std::stringstream ss;

	GLenum curError;

	do
	{
		curError = glGetError();

		//if (curError != GL_NO_ERROR && curError != GL_INVALID_OPERATION)
		//	ss << "'" << glewGetErrorString(curError) << "' ";
	}
	while (curError != GL_NO_ERROR && curError != GL_INVALID_OPERATION);

	if (!ss.str().empty())
	{
		std::cout << file << ": line " << line << "\n" << ss.str() << std::endl;
		breakpoint();
	}
}
