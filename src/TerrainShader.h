#pragma once

#include "ShaderProgram.h"

#include <vector>

#include "glm/glm.hpp"

#include "Camera.h"

class TerrainShader : public ShaderProgram
{
public:
	TerrainShader();
	~TerrainShader();

	void loadProjectionMatrix(const glm::mat4& matrix) const;
	void loadViewMatrix(const Camera& camera) const;

protected:
	virtual void bindAttributes() const override;
	virtual void getAllUniformLocations() override;

private:
	GLuint location_projectionMatrix;
	GLuint location_viewMatrix;
};
