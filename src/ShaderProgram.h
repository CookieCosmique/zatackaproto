#pragma once

#include "OpenGL.h"

#include "glm/glm.hpp"

#include <string>

class ShaderProgram
{
public:
	ShaderProgram(const std::string& vertexFile, const std::string& fragmentFile);
#ifndef __EMSCRIPTEN__
	ShaderProgram(const std::string& vertexFile, const std::string& geometryFile, const std::string& fragmentFile);
#endif

	void start() const;
	void stop() const;
	void cleanUp() const;

	GLuint GetAttribLocation(const char* attribute) const;

protected:
	void init();

	virtual void bindAttributes() const = 0;
	virtual void getAllUniformLocations() = 0;

	void bindAttribute(GLuint attribute, const std::string& name) const;
	GLuint getUniformLocation(const char* uniformName) const;

	void loadInt(GLuint location, GLint value) const;
	void loadFloat(GLuint location, GLfloat value) const;
	void loadVector(GLuint location, const glm::vec3& vector) const;
	void loadVector(GLuint location, const glm::vec2& vector) const;
	void loadBool(GLuint location, bool value) const;
	void loadMatrix(GLuint location, const glm::mat4& matrix) const;

private:
	static GLuint loadShader(const std::string& file, GLenum type);

private:
	GLuint programID;
	GLuint vertexShaderID;
	GLuint geometryShaderID;
	GLuint fragmentShaderID;
};
