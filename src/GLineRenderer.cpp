#include "ProjectDefines.h"

#include "GLineRenderer.h"

#include "Maths.h"
#include "CheckOpenGLError.h"

GLineRenderer::GLineRenderer(const glm::mat4& projectionMatrix)
{
	shader.start();
	shader.loadProjectionMatrix(projectionMatrix);
	shader.stop();

	shaderDebug.start();
	shaderDebug.loadProjectionMatrix(projectionMatrix);
	shaderDebug.stop();
}

void GLineRenderer::render(const Camera& camera, const Light& light, const RenderLines& lines, const Options& options) const
{
	glDepthFunc(GL_ALWAYS);

	const GLineShader& currentShader = options.debug_vertices ? shaderDebug : shader;

	currentShader.start();
	currentShader.loadViewMatrix(camera);
	currentShader.loadLight(light);
	currentShader.loadLighting(options.diffuse, options.specular);

	for (size_t iLine = 0; iLine < lines.count; ++iLine)
	{
		const RenderLine& line = lines.renderLines[iLine];
		currentShader.loadColor(line.options.color);

		glBindVertexArray(line.vao); CGLE();
		//const GLint positionLocation = currentShader.GetAttribLocation("position");
		//printf("position location: %d\n", positionLocation);
		glEnableVertexAttribArray(0); CGLE();
		glEnableVertexAttribArray(1); CGLE();
		glEnableVertexAttribArray(2); CGLE();

		glDrawElements(GL_LINES, line.size, GL_UNSIGNED_INT, nullptr); CGLE();

		glDisableVertexAttribArray(0); CGLE();
		glDisableVertexAttribArray(1); CGLE();
		glDisableVertexAttribArray(2); CGLE();
		glBindVertexArray(0); CGLE();
	}

	currentShader.stop();

	glDepthFunc(GL_LESS);
}
