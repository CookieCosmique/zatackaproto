#include "ProjectDefines.h"

#include "EntityRenderer.h"

#include "Maths.h"

EntityRenderer::EntityRenderer(const glm::mat4& projectionMatrix, GLuint texture) :
	defaultTexture(texture)
{
	shader.start();
	shader.loadProjectionMatrix(projectionMatrix);
	shader.stop();

	shaderDebug.start();
	shaderDebug.loadProjectionMatrix(projectionMatrix);
	shaderDebug.stop();
}

void EntityRenderer::render(const Camera& camera, const Light& light, const Options& options) const
{
	const EntityShader& currentShader = options.debug_vertices ? shaderDebug : shader;

	currentShader.start();

	currentShader.loadViewMatrix(camera);
	currentShader.loadLight(light);
	currentShader.loadLighting(options.diffuse, options.specular);

	if (options.colored_arrow)
		currentShader.loadColor(options.color);
	else
		currentShader.loadColor(glm::vec3(1.f, 1.f, 1.f));

	foreachitemconst(pair, entities)
	{
		prepareTexturedModel(pair.first);

		foreachitemconst(entity, pair.second)
		{
			currentShader.loadTransformationMatrix(Maths::createTransformationMatrix(*entity));
			glDrawElements(GL_TRIANGLES, pair.first->model.size, GL_UNSIGNED_INT, nullptr);
		}

		unbindTexturedModel();
	}

	currentShader.stop();
}

void EntityRenderer::addEntity(const TexturedModel* model, const Entity* entity)
{
	entities[model].push_back(entity);
}

void EntityRenderer::cleanUp()
{
	entities.clear();
	shader.cleanUp();
}

void EntityRenderer::prepareTexturedModel(const TexturedModel* model) const
{
	glBindVertexArray(model->model.vao);

	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	glEnableVertexAttribArray(2);

	glActiveTexture(GL_TEXTURE0);

	if (model->texture != 0)
		glBindTexture(GL_TEXTURE_2D, model->texture);
	else
		glBindTexture(GL_TEXTURE_2D, defaultTexture);
}

void EntityRenderer::unbindTexturedModel() const
{
	glDisableVertexAttribArray(2);
	glDisableVertexAttribArray(1);
	glDisableVertexAttribArray(0);

	glBindVertexArray(0);
}
