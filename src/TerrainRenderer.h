#pragma once

#include "Camera.h"
#include "TerrainShader.h"
#include "RawModel.h"

#include "glm/glm.hpp"

#include <map>
#include <vector>

class Loader;

class TerrainRenderer
{
public:
	TerrainRenderer(const glm::mat4& projectionMatrix, Loader* loader);

	void render(const Camera& camera) const;

private:
	TerrainShader shader;
	RawModel terrainModel;
};
