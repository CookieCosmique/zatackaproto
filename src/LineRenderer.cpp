#include "ProjectDefines.h"

#include "LineRenderer.h"

#include "Maths.h"
#include "CheckOpenGLError.h"

LineRenderer::LineRenderer(const glm::mat4& projectionMatrix)
{
	shader.start();
	shader.loadProjectionMatrix(projectionMatrix);
	shader.stop();
}

void LineRenderer::render(const Camera& camera, const Light& light, GLuint vao, GLuint size) const
{
	shader.start();
	shader.loadViewMatrix(camera);
	shader.loadLight(light);

	glBindVertexArray(vao); CGLE();
	glEnableVertexAttribArray(0); CGLE();
	glEnableVertexAttribArray(1); CGLE();

	glDrawElements(GL_TRIANGLES, size, GL_UNSIGNED_INT, nullptr); CGLE();

	glDisableVertexAttribArray(0); CGLE();
	glDisableVertexAttribArray(1); CGLE();
	glBindVertexArray(0); CGLE();

	shader.stop();
}
