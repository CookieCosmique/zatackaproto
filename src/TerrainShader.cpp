#include "ProjectDefines.h"

#include "TerrainShader.h"

#include "Maths.h"

#include <string>

TerrainShader::TerrainShader() :
	ShaderProgram("Shaders/terrainVertexShader.glsl", "Shaders/terrainFragmentShader.glsl")
{
	init();
}

TerrainShader::~TerrainShader()
{

}

void TerrainShader::loadProjectionMatrix(const glm::mat4& matrix) const
{
	loadMatrix(location_projectionMatrix, matrix);
}

void TerrainShader::loadViewMatrix(const Camera& camera) const
{
	loadMatrix(location_viewMatrix, Maths::createViewMatrix(camera));
}

void TerrainShader::bindAttributes() const
{
	bindAttribute(0, "position");
}

void TerrainShader::getAllUniformLocations()
{
	location_projectionMatrix = getUniformLocation("projectionMatrix");
	location_viewMatrix = getUniformLocation("viewMatrix");
}
