#include "ProjectDefines.h"

#include "TerrainRenderer.h"

#include "Maths.h"

#include "Loader.h"

namespace
{
	RawModel loadTerrain(Loader* loader)
	{
		std::vector<GLfloat> positions;
		std::vector<GLuint> indices;

		const int size = 100;
		const int cellSize = 5;
		const float y = -5.f;

		for (int i = -size / 2; i <= size / 2; i++)
		{
			positions.push_back((float)(i * cellSize));
			positions.push_back(y);
			positions.push_back((-size / 2) * cellSize);

			indices.push_back((GLuint)(positions.size() / 3 - 1));

			positions.push_back((float)(i * cellSize));
			positions.push_back(y);
			positions.push_back((size / 2) * cellSize);

			indices.push_back((GLuint)(positions.size() / 3 - 1));
		}

		for (int i = -size / 2; i <= size / 2; i++)
		{
			positions.push_back((-size / 2) * cellSize);
			positions.push_back(y);
			positions.push_back((float)(i * cellSize));

			indices.push_back((GLuint)(positions.size() / 3 - 1));

			positions.push_back((size / 2) * cellSize);
			positions.push_back(y);
			positions.push_back((float)(i * cellSize));

			indices.push_back((GLuint)(positions.size() / 3 - 1));
		}

		for (float i = y; i <= 0.f; ++i) {
			positions.push_back((-size / 2) * cellSize);
			positions.push_back(i);
			positions.push_back((-size / 2) * cellSize);

			indices.push_back((GLuint)(positions.size() / 3 - 1));

			positions.push_back((-size / 2) * cellSize);
			positions.push_back(i);
			positions.push_back((size / 2) * cellSize);

			indices.push_back((GLuint)(positions.size() / 3 - 1));

			positions.push_back((-size / 2) * cellSize);
			positions.push_back(i);
			positions.push_back((size / 2) * cellSize);

			indices.push_back((GLuint)(positions.size() / 3 - 1));

			positions.push_back((size / 2) * cellSize);
			positions.push_back(i);
			positions.push_back((size / 2) * cellSize);

			indices.push_back((GLuint)(positions.size() / 3 - 1));

			positions.push_back((size / 2) * cellSize);
			positions.push_back(i);
			positions.push_back((size / 2) * cellSize);

			indices.push_back((GLuint)(positions.size() / 3 - 1));

			positions.push_back((size / 2) * cellSize);
			positions.push_back(i);
			positions.push_back((-size / 2) * cellSize);

			indices.push_back((GLuint)(positions.size() / 3 - 1));

			positions.push_back((size / 2) * cellSize);
			positions.push_back(i);
			positions.push_back((-size / 2) * cellSize);

			indices.push_back((GLuint)(positions.size() / 3 - 1));

			positions.push_back((-size / 2) * cellSize);
			positions.push_back(i);
			positions.push_back((-size / 2) * cellSize);

			indices.push_back((GLuint)(positions.size() / 3 - 1));
		}

		return loader->loadToVAO(positions.data(), positions.size(), indices.data(), indices.size());
	}
}

TerrainRenderer::TerrainRenderer(const glm::mat4& projectionMatrix, Loader* loader)
{
	shader.start();
	shader.loadProjectionMatrix(projectionMatrix);
	shader.stop();

	terrainModel = loadTerrain(loader);
}

void TerrainRenderer::render(const Camera& camera) const
{
	shader.start();
	shader.loadViewMatrix(camera);

	glBindVertexArray(terrainModel.vao);
	glEnableVertexAttribArray(0);

	glDrawElements(GL_LINES, terrainModel.size, GL_UNSIGNED_INT, nullptr);

	glDisableVertexAttribArray(0);
	glBindVertexArray(0);

	shader.stop();
}
