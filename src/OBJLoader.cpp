#include "ProjectDefines.h"

#include "OBJLoader.h"
#include "OpenGL.h"

#include "Assert.h"

#include "glm/glm.hpp"

#include <fstream>
#include <vector>
#include "Loader.h"

namespace
{
	void strSplit(const std::string& str, char c, std::vector<std::string>& out)
	{
		std::istringstream iss(str);
		std::string line;

		while (std::getline(iss, line, c))
			out.push_back(line);
	}

	void strSplit(const std::string& str, char c, std::vector<unsigned int>& out)
	{
		std::istringstream iss(str);
		std::string line;

		while (std::getline(iss, line, c))
		{
			if (line.empty())
				out.push_back(1);
			else
				out.push_back(std::stoi(line));
		}
	}
}

RawModel OBJLoader::loadObjModel(const std::string& path, Loader* loader)
{
	std::ifstream file(path.c_str(), std::ifstream::in);

	if (!file.is_open())
	{
		AssertErrorMsg(false, "Cannot open file: ", path);
		printf("Cannot open file: %s\n", path.c_str());
		return {0, 0};
	}

	std::vector<glm::vec3> vertices;
	std::vector<glm::vec2> textures;
	std::vector<glm::vec3> normals;

	std::vector<GLuint> indices;
	std::vector<GLfloat> textureArray;
	std::vector<GLfloat> normalsArray;

	std::string line;

	while (std::getline(file, line))
	{
		std::vector<std::string> split;
		strSplit(line, ' ', split);

		AssertFatal(!split.empty());

		if (!split.at(0).compare("v"))
		{
			AssertFatal(split.size() == 4);
			vertices.push_back(glm::vec3(std::stof(split.at(1)), std::stof(split.at(2)), std::stof(split.at(3))));
		}
		else if (!split.at(0).compare("vt"))
		{
			AssertFatal(split.size() == 3);
			textures.push_back(glm::vec2(std::stof(split.at(1)), std::stof(split.at(2))));
		}
		else if (!split.at(0).compare("vn"))
		{
			AssertFatal(split.size() == 4);
			normals.push_back(glm::vec3(std::stof(split.at(1)), std::stof(split.at(2)), std::stof(split.at(3))));
		}
		else if (!split.at(0).compare("f"))
		{
			textureArray.resize(vertices.size() * 2);
			normalsArray.resize(vertices.size() * 3);
			break;
		}
	}

	do
	{
		std::vector<std::string> split;
		strSplit(line, ' ', split);

		AssertFatal(!split.empty());

		if (!split.at(0).compare("f"))
		{
			std::vector<unsigned int> vertex1;
			strSplit(split.at(1), '/', vertex1);
			processVertex(vertex1, indices, textures, normals, textureArray, normalsArray);

			std::vector<unsigned int> vertex2;
			strSplit(split.at(2), '/', vertex2);
			processVertex(vertex2, indices, textures, normals, textureArray, normalsArray);

			std::vector<unsigned int> vertex3;
			strSplit(split.at(3), '/', vertex3);
			processVertex(vertex3, indices, textures, normals, textureArray, normalsArray);

			for (unsigned int i = 4; i < split.size(); i++)
			{
				vertex2 = vertex3;

				vertex3.clear();
				strSplit(split.at(i), '/', vertex3);

				processVertex(vertex1, indices, textures, normals, textureArray, normalsArray);
				processVertex(vertex2, indices, textures, normals, textureArray, normalsArray);
				processVertex(vertex3, indices, textures, normals, textureArray, normalsArray);
			}
		}
	} while (std::getline(file, line));

	file.close();

	std::vector<GLfloat> verticesArray;
	verticesArray.reserve(vertices.size() * 3);

	foreachitem(vec, vertices)
	{
		verticesArray.push_back(vec.x);
		verticesArray.push_back(vec.y);
		verticesArray.push_back(vec.z);
	}

	return loader->loadToVAO(verticesArray.data(), (unsigned int)verticesArray.size(), indices.data(), (unsigned int)indices.size(), textureArray.data(), (unsigned int)textureArray.size(), normalsArray.data(), (unsigned int)normalsArray.size());
}

void OBJLoader::processVertex(const std::vector<unsigned int>& vertexData, std::vector<GLuint>& indices, const std::vector<glm::vec2>& textures, const std::vector<glm::vec3>& normals, std::vector<GLfloat>& textureArray, std::vector<GLfloat>& normalsArray)
{
	AssertFatal(vertexData.size() == 3);

	unsigned int currentVertexPointer = vertexData.at(0) - 1;
	indices.push_back(currentVertexPointer);

	const glm::vec2& currentTex = textures.at(vertexData.at(1) - 1);
	textureArray[currentVertexPointer * 2] = currentTex.x;
	textureArray[currentVertexPointer * 2 + 1] = currentTex.y;

	const glm::vec3& currentNorm = normals.at(vertexData.at(2) - 1);
	normalsArray[currentVertexPointer * 3] = currentNorm.x;
	normalsArray[currentVertexPointer * 3 + 1] = currentNorm.y;
	normalsArray[currentVertexPointer * 3 + 2] = currentNorm.z;
}
