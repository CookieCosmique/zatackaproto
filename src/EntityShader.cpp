#include "ProjectDefines.h"

#include "EntityShader.h"
#include "Maths.h"

#include <string>

#ifdef __EMSCRIPTEN__
EntityShader::EntityShader(const char* vertexFile, const char* fragmentFile) :
	ShaderProgram(vertexFile, fragmentFile)
#else
EntityShader::EntityShader(const char* vertexFile, const char* geometryFile, const char* fragmentFile) :
	ShaderProgram(vertexFile, geometryFile, fragmentFile)
#endif
{
	init();
}

EntityShader::~EntityShader()
{

}

void EntityShader::loadTransformationMatrix(const glm::mat4& matrix) const
{
	loadMatrix(location_transformationMatrix, matrix);
}

void EntityShader::loadProjectionMatrix(const glm::mat4& matrix) const
{
	loadMatrix(location_projectionMatrix, matrix);
}

void EntityShader::loadViewMatrix(const Camera& camera) const
{
	const glm::mat4 ViewMatrix = Maths::createViewMatrix(camera);
	loadMatrix(location_viewMatrix, ViewMatrix);
	loadMatrix(location_viewMatrix_inverse, glm::inverse(ViewMatrix));
}

void EntityShader::loadLight(const Light& light) const
{
	loadVector(location_lightPosition, light.position);
	loadVector(location_lightColour, light.colour);
}

void EntityShader::loadLighting(bool diffuse, bool specular) const
{
	loadBool(location_lightDiffuse, diffuse);
	loadBool(location_lightSpecular, specular);
}

void EntityShader::loadColor(const glm::vec3& color) const
{
	loadVector(location_color, color);
}

void EntityShader::bindAttributes() const
{
	bindAttribute(0, "position");
	bindAttribute(1, "textureCoords");
	bindAttribute(2, "normal");
}

void EntityShader::getAllUniformLocations()
{
	location_transformationMatrix = getUniformLocation("transformationMatrix");
	location_projectionMatrix = getUniformLocation("projectionMatrix");
	location_viewMatrix = getUniformLocation("viewMatrix");
	location_viewMatrix_inverse = getUniformLocation("viewMatrix_inverse");

	location_lightPosition = getUniformLocation("lightPosition");
	location_lightColour = getUniformLocation("lightColour");
	location_lightDiffuse = getUniformLocation("lightDiffuse");
	location_lightSpecular = getUniformLocation("lightSpecular");

	location_color = getUniformLocation("color");
}
