#pragma once

#include <string>
#include <sstream>

#ifdef DEBUG

#define AssertWarningMsg(CONDITION, ...) if (!(CONDITION)) Assert::MyAssert("Warning", #CONDITION, __FILE__, __LINE__, Assert::AssertMsgToString(__VA_ARGS__));
#define AssertErrorMsg(CONDITION, ...) if (!(CONDITION)) Assert::MyAssert("Error", #CONDITION, __FILE__, __LINE__, Assert::AssertMsgToString(__VA_ARGS__));
#define AssertFatalMsg(CONDITION, ...) if (!(CONDITION)) Assert::MyAssert("Fatal", #CONDITION, __FILE__, __LINE__, Assert::AssertMsgToString(__VA_ARGS__));

// TODO:
// nouvelle syntaxe: AssertErrorMsg(CONDITION, MSG)
// AssertErrorMsg(truc, m1 << m2 << m3);
// std::stringstream ss;
// ss << MSG;

#define AssertWarning(CONDITION) if (!(CONDITION)) Assert::MyAssert("Warning", #CONDITION, __FILE__, __LINE__);
#define AssertError(CONDITION) if (!(CONDITION)) Assert::MyAssert("Error", #CONDITION, __FILE__, __LINE__);
#define AssertFatal(CONDITION) if (!(CONDITION)) Assert::MyAssert("Fatal", #CONDITION, __FILE__, __LINE__);

namespace Assert
{
	void MyAssert(const char* parType, const char* parCondition, const char* parFile, const unsigned int parLine, const std::string parMessage = "");

	template<typename T>
	std::string AssertMsgToString(const T& parMsg)
	{
		std::stringstream ss;

		ss << parMsg;
		return (ss.str());
	}
	template<typename T1, typename T2>
	std::string AssertMsgToString(const T1& parMsg, const T2& parMsg2)
	{
		std::stringstream ss;

		ss << parMsg << parMsg2;
		return (ss.str());
	}
	template<typename T1, typename T2, typename T3>
	std::string AssertMsgToString(const T1& parMsg, const T2& parMsg2, const T3& parMsg3)
	{
		std::stringstream ss;

		ss << parMsg << parMsg2 << parMsg3;
		return (ss.str());
	}
	template<typename T1, typename T2, typename T3, typename T4>
	std::string AssertMsgToString(const T1& parMsg, const T2& parMsg2, const T3& parMsg3, const T4& parMsg4)
	{
		std::stringstream ss;

		ss << parMsg << parMsg2 << parMsg3 << parMsg4;
		return (ss.str());
	}
	template<typename T1, typename T2, typename T3, typename T4, typename T5>
	std::string AssertMsgToString(const T1& parMsg, const T2& parMsg2, const T3& parMsg3, const T4& parMsg4, const T5& parMsg5)
	{
		std::stringstream ss;

		ss << parMsg << parMsg2 << parMsg3 << parMsg4 << parMsg5;
		return (ss.str());
	}
	template<typename T1, typename T2, typename T3, typename T4, typename T5, typename T6>
	std::string AssertMsgToString(const T1& parMsg, const T2& parMsg2, const T3& parMsg3, const T4& parMsg4, const T5& parMsg5, const T6& parMsg6)
	{
		std::stringstream ss;

		ss << parMsg << parMsg2 << parMsg3 << parMsg4 << parMsg5 << parMsg6;
		return (ss.str());
	}
}

#else

#define AssertWarningMsg(CONDITION, ...) DONOTHING
#define AssertErrorMsg(CONDITION, ...) DONOTHING
#define AssertFatalMsg(CONDITION, ...) DONOTHING

#define AssertWarning(CONDITION) DONOTHING
#define AssertError(CONDITION) DONOTHING
#define AssertFatal(CONDITION) DONOTHING

#endif

#define AssertNotReached() AssertError(false)
#define AssertNotImplemented() AssertErrorMsg(false, "Not Implemented.")
