#pragma once

#include "glm/glm.hpp"

struct Options
{
	bool debug_vertices;
	bool diffuse;
	bool specular;
	glm::vec3 color;
	bool colored_arrow;
};
