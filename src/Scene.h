#pragma once

#include "OpenGL.h"

#include "glm/glm.hpp"

#include <vector>
#include <unordered_map>

struct Material
{
	GLuint texture;
	float specular;
	float reflectivity;
};

struct Mesh
{
	GLuint vao;
	GLuint size;
	uint32_t materialID;
};

struct Transform
{
	glm::vec3 position;
	glm::vec3 rotation;
	float scale;
};

struct Instance
{
	uint32_t meshID;
	uint32_t transformID;
};

struct Camera
{
	glm::vec3 position;
	float pitch;
	float yaw;
	float roll;
};

struct Light
{
	glm::vec3 position;
	glm::vec3 colour;
};

class Scene
{
public:
	Camera camera;
	Light light;

	std::vector<Material> materials;
	std::vector<Mesh> meshes;
	std::vector<Transform> transforms;
	std::vector<Instance> instances;

	std::unordered_map<uint32_t, uint32_t> instanceMap; // < MeshID, InstanceID >


};
