#include "ProjectDefines.h"

#include "MasterRenderer.h"

#include "Maths.h"

#include "EntityRenderer.h"
#include "TerrainRenderer.h"
#include "LineRenderer.h"
#include "GLineRenderer.h"

MasterRenderer::MasterRenderer(GLuint defaultTexture, Loader* loader)
{
	projectionMatrix = Maths::createProjectionMatrix(60.f, 1280.f, 720.f, .1f, 1000.f);
//	projectionMatrix = glm::ortho(0.f, 1280.f, 0.f, 720.f, 0.f, 100.f);
	entityRenderer = new EntityRenderer(projectionMatrix, defaultTexture);
	terrainRenderer = new TerrainRenderer(projectionMatrix, loader);
	lineRenderer = new LineRenderer(projectionMatrix);
	glineRenderer = new GLineRenderer(projectionMatrix);
}

MasterRenderer::~MasterRenderer()
{
	delete glineRenderer;
	delete lineRenderer;
	delete terrainRenderer;
	delete entityRenderer;
}

void MasterRenderer::render(const Camera& camera, const Light& light, const RenderLines& lines, const Options& options) const
{
	prepare();
	terrainRenderer->render(camera);
	glineRenderer->render(camera, light, lines, options);
	entityRenderer->render(camera, light, options);

/*	glEnable(GL_SCISSOR_TEST);
	glScissor(1280 - 200, 0, 200, 200);

	Camera cam = camera;
	Options opt = options;

	opt.diffuse = false;
	opt.specular = false;

	terrainRenderer->render(cam);
	glineRenderer->render(cam, light, vao, size, opt);
	entityRenderer->render(cam, light, opt);

	glDisable(GL_SCISSOR_TEST);*/
}

void MasterRenderer::prepare() const
{
	glEnable(GL_DEPTH_TEST);
	//glClearColor(.6f, .4f, .2f, 1.f);
	glClearColor(0.f, 0.f, 0.f, 1.f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void MasterRenderer::cleanUp()
{
	entityRenderer->cleanUp();
}

void MasterRenderer::addEntity(const TexturedModel* model, const Entity* entity)
{
	entityRenderer->addEntity(model, entity);
}
