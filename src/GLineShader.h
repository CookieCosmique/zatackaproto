#pragma once

#include "ShaderProgram.h"

#include <vector>
#include <string>

#include "glm/glm.hpp"

#include "Camera.h"
#include "Light.h"

class GLineShader : public ShaderProgram
{
public:
#ifdef __EMSCRIPTEN__
	GLineShader(const char* vertexFile, const char* fragmentFile);
#else
	GLineShader(const char* vertexFile, const char* geometryFile, const char* fragmentFile);
#endif
	~GLineShader();

	void loadProjectionMatrix(const glm::mat4& matrix) const;
	void loadViewMatrix(const Camera& camera) const;

	void loadLight(const Light& light) const;
	void loadLighting(bool diffuse, bool specular) const;

	void loadColor(const glm::vec3& color) const;

protected:
	virtual void bindAttributes() const override;
	virtual void getAllUniformLocations() override;

private:
	GLuint location_projectionMatrix;
	GLuint location_viewMatrix;
	GLuint location_viewMatrix_inverse;

	GLuint location_lightPosition;
	GLuint location_lightColour;
	GLuint location_lightDiffuse;
	GLuint location_lightSpecular;

	GLuint location_color;
};
