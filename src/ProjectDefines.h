// Pas de #pragma once : le fichier doit etre au debut de tous les .cpp

#define NOMINMAX

#define DONOTHING (void)0;

#ifdef _VERSION
# define VERSION _VERSION
#endif

#ifdef _DEBUG
# define DEBUG
#endif

#define foreachitem(itemName, collection) for (auto& itemName : collection)
#define foreachitemconst(itemName, collection) for (const auto& itemName : collection)
