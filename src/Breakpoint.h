#pragma once

#ifndef breakpoint
#if defined(_MSC_VER)
# define breakpoint __debugbreak
#elif defined(__EMSCRIPTEN__)
# define breakpoint()
#elif defined(__GNUC__)
# define breakpoint() asm("int3")
#endif
#endif
