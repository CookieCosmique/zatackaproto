#pragma once

#include "OpenGL.h"

#include "glm/glm.hpp"

#include <string>
#include <vector>

#include "RawModel.h"

class Loader;

class OBJLoader
{
public:
	static RawModel loadObjModel(const std::string& path, Loader* loader);

private:
	static void processVertex(const std::vector<unsigned int>& vertexData, std::vector<GLuint>& indices, const std::vector<glm::vec2>& textures, const std::vector<glm::vec3>& normals, std::vector<GLfloat>& textureArray, std::vector<GLfloat>& normalsArray);
};
