#pragma once

#include "OpenGL.h"

struct ColorRGBA
{
	float r;
	float g;
	float b;
	float a;
};

struct BufferedImage
{
	unsigned char* datas;
	unsigned int width;
	unsigned int height;
	GLint internalFormat;
	GLenum format;

	unsigned int channels;
	unsigned int bitDepth;

	ColorRGBA getColor(unsigned int x, unsigned int y) const;
};

BufferedImage ParsePNGFile(const char* path);
BufferedImage ParseBMPFile(const char* path);
