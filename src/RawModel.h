#pragma once

#include "OpenGL.h"

struct RawModel
{
	GLuint vao;
	GLuint size;
};
